# coding=utf-8

import codecs
import re
import os
import sys
from PySide import QtGui
from PySide import QtCore

import Global
import MediaToolsLogger
from Liedanzeige import LiedanzeigeDisplayWidget
from Liedanzeige import LiedanzeigeDisplayModel
from TextEditor import TextEditorWidget
from Dateinamen import DateinamenEditorWidget
from Dateinamen import DateinamenSettingsWidget
from Dateinamen import CreateDirsWidget
from Dateinamen import DateinamenModel
from Dateinamen import PlaylistWidget
from Converter import ConverterWidget
from GeneralSettings import GeneralSettingsModel
from GeneralSettings import GeneralSettingsWidget
from MediaDockInfoFiles import RecordInfoWidget
from MediaDockInfoFiles import CollectionInfoWidget
from MediaDockInfoFiles import CategoryInfoWidget
import MtWidgets
from HtmlViewer import HtmlViewer
from Id3Tags import Mp3tagWidget
import DiagnosticModule
import OnBoardDiagnostics


class MainWindow(QtGui.QMainWindow):
    """GUI der Anwendung."""

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application
        self.setWindowIcon(QtGui.QIcon(QtGui.QPixmap(Global.MAIN_WINDOW.ICON_PATH)))

        widget = QtGui.QWidget()
        grid = QtGui.QGridLayout()
        grid.setSpacing(0)
        grid.setContentsMargins(0, 0, 0, 0)
        widget.setLayout(grid)

        self.tab_widget = QtGui.QTabWidget()
        self.tab_widget.setTabsClosable(True)
        self.tab_widget.setMovable(True)
        # noinspection PyUnresolvedReferences
        self.tab_widget.tabCloseRequested.connect(self.close_tab)
        # noinspection PyUnresolvedReferences
        self.tab_widget.currentChanged.connect(self.current_changed)

        self.liedanzeige_model = LiedanzeigeDisplayModel(self.application)
        self.dateinamen_model = DateinamenModel(self.application)
        self.general_settings_model = GeneralSettingsModel(self.application)

        grid.addWidget(self.tab_widget, 0, 0, 1, 1)

        status_bar_widget = QtGui.QWidget()
        grid.addWidget(status_bar_widget)
        status_bar_layout = QtGui.QHBoxLayout()
        status_bar_layout.setContentsMargins(0, 0, 0, 0)
        status_bar_widget.setLayout(status_bar_layout)

        self.status_bar = QtGui.QLabel(u'')
        self.status_bar.setTextFormat(QtCore.Qt.PlainText)
        self.status_bar.setContentsMargins(5, 5, 5, 5)
        status_bar_layout.addWidget(self.status_bar, 1)

        self.always_in_front_button = QtGui.QCheckBox(u'Fenster im Vordergrund')
        self.always_in_front_button.setCheckable(True)
        self.always_in_front_button.setContentsMargins(5, 5, 5, 5)
        # noinspection PyUnresolvedReferences
        self.always_in_front_button.clicked.connect(self.show_in_front_action)
        status_bar_layout.addWidget(self.always_in_front_button)
        self.always_in_front_button.setChecked(self.general_settings_model.show_window_on_top)
        self.set_window_flag_on_top_hint(self.general_settings_model.show_window_on_top)

        self.setCentralWidget(widget)

        # menu
        menu_bar = self.menuBar()

        start_page_action = QtGui.QAction(u'', self)
        self.start_page_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-home-6-240.png'))
        start_page_action.setIcon(self.start_page_icon)
        # noinspection PyUnresolvedReferences
        start_page_action.triggered.connect(self.show_startseite)
        menu_bar.addAction(start_page_action)

        # options_menu = menu_bar.addMenu(u'Optionen')
        view_menu = menu_bar.addMenu(u'Ansicht')
        file_menu = menu_bar.addMenu(u'Dateien')
        help_menu = menu_bar.addMenu(u'Hilfe')

        exit_action = QtGui.QAction(u'Beenden', self)
        exit_action.setShortcut('Ctrl+Q')
        # noinspection PyUnresolvedReferences
        exit_action.triggered.connect(self.close)
        # options_menu.addAction(exit_action)

        churches_file_action = QtGui.QAction(u'Gemeinden', self)
        # settings_action.setShortcut('Ctrl+Q')
        self.file_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-note-14-240.png'))
        churches_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        churches_file_action.triggered.connect(self.open_churches_file)
        file_menu.addAction(churches_file_action)

        events_file_action = QtGui.QAction(u'Ereignisse', self)
        # settings_action.setShortcut('Ctrl+Q')
        events_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        events_file_action.triggered.connect(self.open_events_file)
        file_menu.addAction(events_file_action)

        collection_file_action = QtGui.QAction(u'Sammlungen', self)
        # settings_action.setShortcut('Ctrl+Q')
        collection_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        collection_file_action.triggered.connect(self.open_collections_file)
        file_menu.addAction(collection_file_action)

        categories_file_action = QtGui.QAction(u'Kategorien', self)
        # settings_action.setShortcut('Ctrl+Q')
        categories_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        categories_file_action.triggered.connect(self.open_categories_file)
        file_menu.addAction(categories_file_action)

        file_menu.addSeparator()

        names_file_action = QtGui.QAction(u'Dateinamen - Namen', self)
        # settings_action.setShortcut('Ctrl+Q')
        names_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        names_file_action.triggered.connect(self.open_names_file)
        file_menu.addAction(names_file_action)

        titel_file_action = QtGui.QAction(u'Dateinamen - Titel', self)
        # settings_action.setShortcut('Ctrl+Q')
        titel_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        titel_file_action.triggered.connect(self.open_titel_file)
        file_menu.addAction(titel_file_action)

        sonst_file_action = QtGui.QAction(u'Dateinamen - Sonst', self)
        # settings_action.setShortcut('Ctrl+Q')
        sonst_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        sonst_file_action.triggered.connect(self.open_sonst_file)
        file_menu.addAction(sonst_file_action)

        sonst_file_action = QtGui.QAction(u'Wiedergabelisten - Medientypen', self)
        # settings_action.setShortcut('Ctrl+Q')
        sonst_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        sonst_file_action.triggered.connect(self.open_playlists_mediatypes)
        file_menu.addAction(sonst_file_action)

        file_menu.addSeparator()

        log_file_action = QtGui.QAction(u'Log-Datei', self)
        # settings_action.setShortcut('Ctrl+Q')
        log_file_action.setIcon(self.file_icon)
        # noinspection PyUnresolvedReferences
        log_file_action.triggered.connect(self.open_log_file)
        file_menu.addAction(log_file_action)

        dateinamen_action = QtGui.QAction(u'Dateinamen', self)
        # dateinamen_action.setShortcut('Ctrl+Q')
        self.dateinamen_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-file-29-240.png'))
        dateinamen_action.setIcon(self.dateinamen_icon)
        # noinspection PyUnresolvedReferences
        dateinamen_action.triggered.connect(self.show_dateinamen)
        view_menu.addAction(dateinamen_action)

        playlist_dateinamen_action = QtGui.QAction(u'Wiedergabelisten', self)
        # dateinamen_action.setShortcut('Ctrl+Q')
        playlist_dateinamen_action.setIcon(self.dateinamen_icon)
        # noinspection PyUnresolvedReferences
        playlist_dateinamen_action.triggered.connect(self.show_playlist_dateinamen)
        view_menu.addAction(playlist_dateinamen_action)

        create_dirs_action = QtGui.QAction(u'Ordner erstellen', self)
        # create_dirs_action.setShortcut('Ctrl+Q')
        self.create_dirs_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-folder-4-240.png'))
        create_dirs_action.setIcon(self.create_dirs_icon)
        # noinspection PyUnresolvedReferences
        create_dirs_action.triggered.connect(self.show_create_dirs)
        view_menu.addAction(create_dirs_action)

        dateinamen_settings_action = QtGui.QAction(u'Dateinamen - Einstellungen', self)
        # dateinamen_settings_action.setShortcut('Ctrl+Q')
        self.settings_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-gear-10-240.png'))
        dateinamen_settings_action.setIcon(self.settings_icon)
        # noinspection PyUnresolvedReferences
        dateinamen_settings_action.triggered.connect(self.show_dateinamen_settings)
        view_menu.addAction(dateinamen_settings_action)

        view_menu.addSeparator()

        record_info_action = QtGui.QAction(u'Aufnahmen für MediaDock', self)
        # record_info_action.setShortcut('Ctrl+Q')
        self.mediadock_files_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-note-29-240.png'))
        record_info_action.setIcon(self.mediadock_files_icon)
        # noinspection PyUnresolvedReferences
        record_info_action.triggered.connect(self.show_record_info)
        view_menu.addAction(record_info_action)

        collection_info_action = QtGui.QAction(u'Sammlungen für MediaDock', self)
        # collection_info_action.setShortcut('Ctrl+Q')
        collection_info_action.setIcon(self.mediadock_files_icon)
        # noinspection PyUnresolvedReferences
        collection_info_action.triggered.connect(self.show_collection_info)
        view_menu.addAction(collection_info_action)

        category_info_action = QtGui.QAction(u'Kategorien für MediaDock', self)
        # category_info_action.setShortcut('Ctrl+Q')
        category_info_action.setIcon(self.mediadock_files_icon)
        # noinspection PyUnresolvedReferences
        category_info_action.triggered.connect(self.show_category_info)
        view_menu.addAction(category_info_action)

        view_menu.addSeparator()

        id3_tags_action = QtGui.QAction(u'ID3-Tags', self)
        # id3_tags_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.IMAGES_DIR, u'id3v2.png'))
        id3_tags_action.setIcon(icon)
        # id3_tags_action.triggered.connect(self.show_id3_tags)
        # view_menu.addAction(id3_tags_action)

        mp3tag_action = QtGui.QAction(u'Mp3tag', self)
        # mp3tag_action.setShortcut('Ctrl+Q')
        self.mp3tag_icon = QtGui.QPixmap(os.path.join(Global.IMAGES_DIR, u'mp3tag.png'))  # todo other image (license)
        mp3tag_action.setIcon(self.mp3tag_icon)
        # noinspection PyUnresolvedReferences
        mp3tag_action.triggered.connect(self.show_mp3tag)
        view_menu.addAction(mp3tag_action)

        view_menu.addSeparator()

        liedanzeige_action = QtGui.QAction(u'Liedanzeige', self)
        # liedanzeige_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.IMAGES_DIR, u'Liedanzeige.png'))
        liedanzeige_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        liedanzeige_action.triggered.connect(self.show_liedanzeige)
        # view_menu.addAction(liedanzeige_action)

        # view_menu.addSeparator()

        media_dock_action = QtGui.QAction(u'MediaDock', self)
        # media_dock_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.IMAGES_DIR, u'MediaDock.png'))
        media_dock_action.setIcon(icon)
        # media_dock_action.triggered.connect(self.show_media_dock)
        # view_menu.addAction(media_dock_action)

        cd_orders_action = QtGui.QAction(u'CD-Bestellungen', self)
        # cd_orders_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-disc-5-240.png'))
        cd_orders_action.setIcon(icon)
        # cd_orders_action.triggered.connect(self.show_cd_orders)
        # view_menu.addAction(cd_orders_action)

        # view_menu.addSeparator()

        converter_action = QtGui.QAction(u'CopyCenter nach MediaDock konvertieren', self)
        # converter_action.setShortcut('Ctrl+Q')
        # icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-disc-5-240.png'))
        # converter_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        converter_action.triggered.connect(self.show_converter)
        # view_menu.addAction(converter_action)

        # view_menu.addSeparator()

        settings_action = QtGui.QAction(u'Einstellungen', self)
        # settings_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-gear-10-240.png'))
        settings_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        settings_action.triggered.connect(self.show_settings)
        view_menu.addAction(settings_action)

        help_action = QtGui.QAction(u'Hilfe', self)
        # settings_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-lifebuoy-1-240.png'))
        help_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        help_action.triggered.connect(self.open_help)
        help_menu.addAction(help_action)

        help_menu.addSeparator()

        diagnostic_file_action = QtGui.QAction(u'Diagnosedatei erstellen', self)
        # diagnostic_file_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-medical-5-240'))
        diagnostic_file_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        diagnostic_file_action.triggered.connect(self.create_diagnostic_file)
        help_menu.addAction(diagnostic_file_action)

        help_menu.addSeparator()

        license_action = QtGui.QAction(u'Lizenz', self)
        # license_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-copyright-1-240.png'))
        license_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        license_action.triggered.connect(self.open_license)
        help_menu.addAction(license_action)

        about_action = QtGui.QAction(u'Über', self)
        # settings_action.setShortcut('Ctrl+Q')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-info-9-240.png'))
        about_action.setIcon(icon)
        # noinspection PyUnresolvedReferences
        about_action.triggered.connect(self.show_about)
        help_menu.addAction(about_action)

        self.html_view_icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-text-20-240.png'))

        self.setGeometry(300, 300, 700, 500)
        self.setWindowTitle(u'MediaTools')

        self.show()

    def show_in_front_action(self):
        self.general_settings_model.show_window_on_top = self.always_in_front_button.isChecked()
        self.set_window_flag_on_top_hint(self.general_settings_model.show_window_on_top)
        self.show()

    def set_window_flag_on_top_hint(self, stay_on_top):
        if stay_on_top:
            self.setWindowFlags(self.windowFlags() | QtCore.Qt.WindowStaysOnTopHint)
        else:
            self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowStaysOnTopHint)

    def show_about(self):
        QtGui.QMessageBox.about(self.application.main_window,
                                u'Über MediaTools',
                                u'MediaTools ist eine Sammlung von Tools für den Tontechniker.\n\n'
                                u'Version: +++ replace version +++\n'
                                u'Build: +++ replace build +++\n'
                                u'Commit: +++ replace commit +++\n\n'
                                u'Copyright \u00a9 +++ replace year +++ Peter Klassen (peter@mediadock.org)\n\n'
                                u'Diese Software steht unter der Apache Version 2.0 Lizenz. '
                                u'Weitere Lizenzbestimmungen unter Hilfe\u2192Lizenz.')

    def close_tab(self, current_index):
        widget = self.tab_widget.widget(current_index)
        if isinstance(widget, DateinamenEditorWidget):
            widget.save_dateiname()
        if isinstance(widget, TextEditorWidget):
            if not widget.ask_save_file_on_close():
                return
        self.tab_widget.removeTab(current_index)

    def current_changed(self, index):
        widget = self.tab_widget.widget(index)
        if isinstance(widget, TextEditorWidget):
            widget.check_reload()

    def show_startseite(self):
        self.show_html_page(u'index.html')

    def show_html_page(self, page):
        page_path = os.path.join(Global.HTML_VIEWER.DOCUMENT_ROOT, page)
        if not os.path.isfile(page_path):
            return
        try:
            page_file = codecs.open(page_path, 'r', 'utf-8')
        except IOError as error:
            self.application.logger.log(u'Fehler beim Laden der Seite: %s' % unicode(error), __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            content = page_file.read()
            match = re.search(r'<title>(.+?)</title>', content)
            tab_title = match.groups()[0].strip()
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, HtmlViewer) and self.tab_widget.tabText(i) == tab_title:
                self.tab_widget.setCurrentIndex(i)
                return
        tab = HtmlViewer(self.application, page)
        index = self.tab_widget.addTab(tab, self.html_view_icon, tab_title)
        self.tab_widget.setCurrentIndex(index)

    def show_dateinamen(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, DateinamenEditorWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        dateinamen_tab = DateinamenEditorWidget(self.application)
        index = self.tab_widget.addTab(dateinamen_tab, self.dateinamen_icon, Global.DATEINAMEN.EDITOR.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_playlist_dateinamen(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, PlaylistWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        dateinamen_tab = PlaylistWidget(self.application)
        index = self.tab_widget.addTab(dateinamen_tab, self.dateinamen_icon, Global.DATEINAMEN.PLAYLIST.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_dateinamen_settings(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, DateinamenSettingsWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        dateinamen_settings_tab = DateinamenSettingsWidget(self.application)
        index = self.tab_widget.addTab(dateinamen_settings_tab, self.settings_icon, Global.DATEINAMEN.SETTINGS.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_create_dirs(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, CreateDirsWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        create_dirs_tab = CreateDirsWidget(self.application)
        index = self.tab_widget.addTab(create_dirs_tab, self.create_dirs_icon, Global.DATEINAMEN.CREATE_DIRS.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_record_info(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, RecordInfoWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        create_dirs_tab = RecordInfoWidget(self.application)
        index = self.tab_widget.addTab(create_dirs_tab, self.mediadock_files_icon,
                                       Global.MEDIADOCKINFOFILES.RECORD_INFO.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_collection_info(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, CollectionInfoWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        create_dirs_tab = CollectionInfoWidget(self.application)
        index = self.tab_widget.addTab(create_dirs_tab, self.mediadock_files_icon,
                                       Global.MEDIADOCKINFOFILES.COLLECTION_INFO.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_category_info(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, CategoryInfoWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        create_dirs_tab = CategoryInfoWidget(self.application)
        index = self.tab_widget.addTab(create_dirs_tab, self.mediadock_files_icon,
                                       Global.MEDIADOCKINFOFILES.CATEGORY_INFO.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_liedanzeige(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, LiedanzeigeDisplayWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        liedanzeige_tab = LiedanzeigeDisplayWidget(self.application, self.liedanzeige_model)
        index = self.tab_widget.addTab(liedanzeige_tab, Global.LIEDANZEIGE.DISPLAY.TAB_TEXT)  # todo add icon
        self.tab_widget.setCurrentIndex(index)

    def show_converter(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, ConverterWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        tab = ConverterWidget(self.application)
        index = self.tab_widget.addTab(tab, Global.CONVERTER.TAB_TEXT)  # todo add icon
        self.tab_widget.setCurrentIndex(index)

    def show_settings(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, GeneralSettingsWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        tab = GeneralSettingsWidget(self.application)
        index = self.tab_widget.addTab(tab, self.settings_icon, Global.GENERALSETTINGS.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def show_mp3tag(self):
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            if isinstance(widget, Mp3tagWidget):
                self.tab_widget.setCurrentIndex(i)
                return
        tab = Mp3tagWidget(self.application)
        index = self.tab_widget.addTab(tab, self.mp3tag_icon, Global.ID3TAGS.MP3TAG.TAB_TEXT)
        self.tab_widget.setCurrentIndex(index)

    def open_log_file(self):
        tab = TextEditorWidget(self.application, self.application.logger.log_file_path, immutable=True)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Log-Datei')
        self.tab_widget.setCurrentIndex(index)

    def open_churches_file(self):
        tab = TextEditorWidget(self.application, Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Gemeinden')
        self.tab_widget.setCurrentIndex(index)

    def open_events_file(self):
        tab = TextEditorWidget(self.application, Global.MEDIADOCKINFOFILES.FILES.EVENTS)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Ereignisse')
        self.tab_widget.setCurrentIndex(index)

    def open_collections_file(self):
        tab = TextEditorWidget(self.application, Global.MEDIADOCKINFOFILES.FILES.COLLECTIONS)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Sammlungen')
        self.tab_widget.setCurrentIndex(index)

    def open_categories_file(self):
        tab = TextEditorWidget(self.application, Global.MEDIADOCKINFOFILES.FILES.CATEGORIES)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Kategorien')
        self.tab_widget.setCurrentIndex(index)

    def open_names_file(self):
        tab = TextEditorWidget(self.application, Global.DATEINAMEN.FILES.PATH_NAMES, sort_enabled=True)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Dateinamen - Namen')
        self.tab_widget.setCurrentIndex(index)

    def open_titel_file(self):
        tab = TextEditorWidget(self.application, Global.DATEINAMEN.FILES.PATH_TITEL, sort_enabled=True)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Dateinamen - Titel')
        self.tab_widget.setCurrentIndex(index)

    def open_sonst_file(self):
        tab = TextEditorWidget(self.application, Global.DATEINAMEN.FILES.PATH_SONST, sort_enabled=True)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Dateinamen - Sonst')
        self.tab_widget.setCurrentIndex(index)

    def open_playlists_mediatypes(self):
        tab = TextEditorWidget(self.application, Global.DATEINAMEN.PLAYLIST.TYPE_COMPLETER_PATH)
        index = self.tab_widget.addTab(tab, self.file_icon, u'Wiedergabelisten - Medientypen')
        self.tab_widget.setCurrentIndex(index)

    def open_help(self):
        self.show_html_page('help.html')

    def open_license(self):
        self.show_html_page('license.html')

    def create_diagnostic_file(self):
        dialog = QtGui.QFileDialog()
        file_name = OnBoardDiagnostics.OnBoardDiagnostics.create_filename()
        answer = dialog.getSaveFileName(parent=self, dir=os.path.join(os.path.expanduser('~'), file_name),
                                        caption=u"Diagnosedatei speichern",
                                        filter=u"MediaTools Diagnose Datei (*.mt_obd)")
        if answer[0]:
            self.application.logger.log(u'Erstelle Diagnosedatei...', __name__, MediaToolsLogger.SEVERITY_INFO)
            try:
                obd = OnBoardDiagnostics.OnBoardDiagnostics(self.application)
                obd.write_diagnostic_file(answer[0])
            except Exception as e:
                self.application.logger.log(u'Fehler beim Erstellen der Diagnosedatei: {}'.format(e.message), __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
            else:
                self.application.logger.log(u'Diagnosedatei erfolgreich erstellt', __name__,
                                            MediaToolsLogger.SEVERITY_INFO, status_bar=True)

    def closeEvent(self, event):

        if not self.general_settings_model.ask_quit:
            event.accept()
            return

        dialog = MtWidgets.QuitDialog(self.application.main_window)
        dialog.exec_()

        self.general_settings_model.ask_quit = dialog.ask_quit

        if dialog.ok:
            self.save_tabs()
            for i in range(self.tab_widget.count())[::-1]:
                self.close_tab(i)
            event.accept()
        else:
            event.ignore()

    def save_tabs(self):
        widget_names = list()
        for i in range(self.tab_widget.count()):
            widget = self.tab_widget.widget(i)
            widget_names.append(widget.__class__.__name__)
        try:
            tabs_file = open(Global.MAIN_WINDOW.TABS_SAVE_PATH, 'w')
        except IOError:
            if os.path.isfile(Global.MAIN_WINDOW.TABS_SAVE_PATH):
                try:
                    os.remove(Global.MAIN_WINDOW.TABS_SAVE_PATH)
                except OSError:
                    pass
        else:
            tabs_file.write('\n'.join(widget_names))
            tabs_file.close()

    def load_tabs(self):
        self.show_html_page(u'index.html')
        widget_names = list()
        try:
            tabs_file = open(Global.MAIN_WINDOW.TABS_SAVE_PATH, 'r')
        except IOError:
            pass
        else:
            widget_names = tabs_file.readlines()
            tabs_file.close()
        for name in widget_names:
            striped_name = name.strip()
            if striped_name == 'DateinamenEditorWidget':
                self.show_dateinamen()
            elif striped_name == 'PlaylistWidget':
                self.show_playlist_dateinamen()
            elif striped_name == 'CreateDirsWidget':
                self.show_create_dirs()
            elif striped_name == 'RecordInfoWidget':
                self.show_record_info()
            elif striped_name == 'CollectionInfoWidget':
                self.show_collection_info()
            elif striped_name == 'CategoryInfoWidget':
                self.show_category_info()
            elif striped_name == 'Mp3tagWidget':
                self.show_mp3tag()
        self.tab_widget.setCurrentIndex(0)


class MediaToolsApp(QtGui.QApplication):
    """MediaTools Anwendung."""

    def __init__(self, *args, **kwargs):
        super(self.__class__, self).__init__(*args, **kwargs)
        self.init_dirs()
        self.logger = MediaToolsLogger.MediaToolsLogger(self, Global.LOG_DIR)
        DiagnosticModule.DiagnosticModule(Global.LOG_DIR)
        self.init_files()
        self.main_window = MainWindow(self)
        # noinspection PyUnresolvedReferences
        self.aboutToQuit.connect(self.on_stop)
        self.import_legacy_files()

    def on_stop(self):
        pass

    @staticmethod
    def init_dirs():
        """Erstellt notwendige Ordner."""
        if not os.path.exists(Global.SETTINGS_DIR):
            os.makedirs(Global.SETTINGS_DIR)
        if not os.path.exists(os.path.dirname(Global.DATEINAMEN.FILES.PATH_NAMES)):
            os.makedirs(os.path.dirname(Global.DATEINAMEN.FILES.PATH_NAMES))

    def create_file_if_not_exists(self, path, content=u""):
        if os.path.isfile(path):
            return
        else:
            self.logger.log(u'Erstelle Datei: {0}'.format(path), __name__, severity=MediaToolsLogger.SEVERITY_DEBUG)
            try:
                file_ = codecs.open(path, 'a', 'utf-8')
                file_.write(content)
            except IOError:
                pass
            else:
                file_.close()

    def import_legacy_file(self, legacy_file_path, new_file_path):
        if not os.path.isfile(legacy_file_path):
            return 0
        try:
            legacy_file = codecs.open(legacy_file_path, 'r', 'utf-8')
        except IOError:
            self.logger.log(u"Failed to open file: {0}".format(legacy_file_path), __name__,
                            severity=MediaToolsLogger.SEVERITY_ERROR)
            return 1
        legacy_file_lines = legacy_file.readlines()
        try:
            new_read_file = codecs.open(new_file_path, 'r', 'utf-8')
        except IOError:
            self.logger.log(u"Failed to open file: {0}".format(new_file_path), __name__,
                            severity=MediaToolsLogger.SEVERITY_ERROR)
            return 1
        new_file_lines = new_read_file.readlines()
        new_read_file.close()
        new_file_striped_lines = [i.strip() for i in new_file_lines]
        try:
            new_write_file = codecs.open(new_file_path, 'a', 'utf-8')
        except IOError:
            self.logger.log(u"Failed to open file: {0}".format(new_file_path), __name__,
                            severity=MediaToolsLogger.SEVERITY_ERROR)
            return 1
        for legacy_file_line in legacy_file_lines:
            if not legacy_file_line.strip() in new_file_striped_lines:
                new_write_file.write(legacy_file_line.strip() + os.linesep)
        new_write_file.close()
        return 0

    def import_legacy_files(self):
        if os.path.exists(Global.LEGACY_FILES_IMPORTED_PATH):
            return
        else:
            if os.path.exists(Global.XMLMAKER_PATH) or os.path.exists(Global.MEDIA_NAMES_DATA_DIR):
                message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                                u"Daten holen",
                                                u"Es wurden Daten vorheriger MediaNames und/oder XMLMaker "
                                                u"Installationen gefunden. Sollen diese Daten übernommen werden?",
                                                QtGui.QMessageBox.NoButton,
                                                self.main_window)
                message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
                message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)
                message_box.addButton(u"Jetzt nicht", QtGui.QMessageBox.RejectRole)

                answer = message_box.exec_()
                if answer == 0:  # yes
                    error_sum = 0
                    error_sum += self.import_legacy_file(os.path.join(Global.MEDIA_NAMES_DATA_DIR, u'Namen.txt'),
                                                         Global.DATEINAMEN.FILES.PATH_NAMES)
                    error_sum += self.import_legacy_file(os.path.join(Global.MEDIA_NAMES_DATA_DIR, u'Sonst.txt'),
                                                         Global.DATEINAMEN.FILES.PATH_SONST)
                    error_sum += self.import_legacy_file(os.path.join(Global.MEDIA_NAMES_DATA_DIR, u'Titel.txt'),
                                                         Global.DATEINAMEN.FILES.PATH_TITEL)
                    error_sum += self.import_legacy_file(os.path.join(Global.XMLMAKER_PATH, u'churches.txt'),
                                                         Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
                    error_sum += self.import_legacy_file(os.path.join(Global.XMLMAKER_PATH, u'events.txt'),
                                                         Global.MEDIADOCKINFOFILES.FILES.EVENTS)
                    if error_sum == 0:
                        self.write_legacy_imported_file()
                    else:
                        self.logger.log(u"Es gab Probleme mit einigen Dateien. Für Details siehe Log-Datei.", __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
                elif answer == 1:  # no
                    self.write_legacy_imported_file()

    @staticmethod
    def write_legacy_imported_file():
        try:
            file_ = open(Global.LEGACY_FILES_IMPORTED_PATH, 'a')
        except IOError:
            pass
        else:
            file_.close()

    def add_all_songs(self):
        if os.path.exists(Global.ALL_SONGS_ADDED_IMPORTED_PATH):
            return
        else:
            if not os.path.isfile(Global.DATEINAMEN.FILES.PATH_TITEL):
                return
            try:
                new_read_file = codecs.open(Global.DATEINAMEN.FILES.PATH_TITEL, 'r', 'utf-8')
            except IOError:
                self.logger.log(u"Failed to open file: {0}".format(Global.DATEINAMEN.FILES.PATH_TITEL), __name__,
                                severity=MediaToolsLogger.SEVERITY_ERROR)
                return
            new_file_lines = new_read_file.readlines()
            new_read_file.close()
            new_file_striped_lines = [i.strip() for i in new_file_lines]
            try:
                new_write_file = codecs.open(Global.DATEINAMEN.FILES.PATH_TITEL, 'a', 'utf-8')
            except IOError:
                self.logger.log(u"Failed to open file: {0}".format(Global.DATEINAMEN.FILES.PATH_TITEL), __name__,
                                severity=MediaToolsLogger.SEVERITY_ERROR)
                return
            for song in Global.DATEINAMEN.MODEL.ALL_SONGS:
                if song not in new_file_striped_lines:
                    new_write_file.write(os.linesep + song)
            new_write_file.close()
            self.write_all_songs_imported_file()

    @staticmethod
    def write_all_songs_imported_file():
        """Schreib eine Datei, die angibt dass alle Lieder der Liederdatei hinzugefügt wurden"""
        try:
            file_ = open(Global.ALL_SONGS_ADDED_IMPORTED_PATH, 'a')
        except IOError:
            pass
        else:
            file_.close()

    def init_files(self):
        """Erstellt notwendige Dateien."""
        self.create_file_if_not_exists(Global.MEDIADOCKINFOFILES.FILES.CHURCHES)  # Gemeinden.txt
        self.create_file_if_not_exists(Global.MEDIADOCKINFOFILES.FILES.CATEGORIES)  # Kategorien.txt
        self.create_file_if_not_exists(Global.DATEINAMEN.FILES.PATH_NAMES)  # Namen.txt
        self.create_file_if_not_exists(Global.MEDIADOCKINFOFILES.FILES.COLLECTIONS)  # Sammlungen.txt
        self.create_file_if_not_exists(Global.DATEINAMEN.FILES.PATH_SONST)  # Sonst.txt
        self.create_file_if_not_exists(Global.DATEINAMEN.FILES.PATH_TITEL)  # Titel.txt
        self.create_file_if_not_exists(Global.MEDIADOCKINFOFILES.FILES.EVENTS,
                                       u'\n'.join(Global.MEDIADOCKINFOFILES.FILES.DEFAULT_EVENTS))  # Ereignisse.txt
        self.create_file_if_not_exists(Global.DATEINAMEN.PLAYLIST.TYPE_COMPLETER_PATH,
                                       u'\n'.join(Global.DATEINAMEN.PLAYLIST.DEFAULT_MEDIA_TYPES))  # Medientypen.txt
        self.add_all_songs()

    @DiagnosticModule.execute_with_exception_handling
    def run(self):
        """Startet die Applikation.

        :return: Rückgabewert der Applikation.
        """
        self.logger.log(u'MediaTools gestartet', __name__, severity=MediaToolsLogger.SEVERITY_INFO, status_bar=True)
        self.logger.log(u'Version: +++ replace version +++, Build: +++ replace build +++, '
                        u'Commit: +++ replace commit +++', __name__, MediaToolsLogger.SEVERITY_INFO)
        self.logger.cleanup()
        self.main_window.load_tabs()
        exit_code = self.exec_()
        self.logger.log(u'MediaTools beendet', __name__, severity=MediaToolsLogger.SEVERITY_INFO)
        return exit_code


if __name__ == '__main__':
    App = MediaToolsApp(sys.argv)
    sys.exit(App.run())
