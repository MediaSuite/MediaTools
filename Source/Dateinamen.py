# coding=utf-8

import calendar
import codecs
import copy
import datetime
import linecache
import shutil
import os
import re
import json
from PySide import QtGui
from PySide import QtCore

import Global
import MediaToolsLogger
import MtWidgets


class DateinamenEditorWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()

        self.application = application
        self.model = application.main_window.dateinamen_model

        layout = QtGui.QHBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        self.splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        layout.addWidget(self.splitter)

        self.left_layout_widget = QtGui.QWidget()
        self.left_layout = QtGui.QVBoxLayout()
        self.left_layout.setContentsMargins(0, 0, 0, 0)
        self.left_layout_widget.setLayout(self.left_layout)
        self.splitter.addWidget(self.left_layout_widget)

        self.right_layout_widget = QtGui.QWidget()
        right_layout = QtGui.QVBoxLayout()
        right_layout.setContentsMargins(0, 0, 0, 0)
        self.right_layout_widget.setLayout(right_layout)
        self.splitter.addWidget(self.right_layout_widget)

        # left side
        widget_l_1 = QtGui.QWidget()
        self.left_layout.addWidget(widget_l_1)
        layout_l_1 = QtGui.QHBoxLayout()
        layout_l_1.setContentsMargins(0, 0, 0, 0)
        widget_l_1.setLayout(layout_l_1)

        layout_l_1.addWidget(QtGui.QLabel(u'Zähler:'))

        self.counter_spin_box = QtGui.QSpinBox()
        self.counter_spin_box.setMaximum(10 ** Global.DATEINAMEN.MODEL.NUMBER_LENGTH_MAX - 1)
        self.counter_spin_box.setMinimum(0)
        self.counter_spin_box.setValue(1)
        layout_l_1.addWidget(self.counter_spin_box)

        button_reset_counter = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-refresh-8-240.png'))
        button_reset_counter.setIcon(icon)
        button_reset_counter.setToolTip(u'Zähler zurücksetzen')
        # noinspection PyUnresolvedReferences
        button_reset_counter.clicked.connect(self.reset_counter)
        layout_l_1.addWidget(button_reset_counter)

        button_reset_dateinamen = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-12-240.png'))
        button_reset_dateinamen.setIcon(icon)
        button_reset_dateinamen.setToolTip(u'Eingaben löschen')
        # noinspection PyUnresolvedReferences
        button_reset_dateinamen.clicked.connect(self.clear_view)
        layout_l_1.addWidget(button_reset_dateinamen)

        label = QtGui.QLabel()
        layout_l_1.addWidget(label, 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        self.left_layout.addWidget(line)

        self.type_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.type_radio_button_group.buttonReleased[int].connect(self.type_radio_button_released)
        self.type_radio_buttons = list()

        widget_l_2 = QtGui.QWidget()
        layout_l_2 = QtGui.QHBoxLayout()
        layout_l_2.setContentsMargins(0, 0, 0, 0)
        widget_l_2.setLayout(layout_l_2)
        self.left_layout.addWidget(widget_l_2)
        type_rb_eroeffnung = QtGui.QRadioButton(u'Eröffnung')
        # noinspection PyUnresolvedReferences
        type_rb_eroeffnung.toggled.connect(self.calculate_focus_policies)
        type_rb_eroeffnung.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_eroeffnung, Global.DATEINAMEN.RB_TYPE.EROEFFNUNG)
        self.type_radio_buttons.append(type_rb_eroeffnung)
        layout_l_2.addWidget(type_rb_eroeffnung)

        type_rb_gebet = QtGui.QRadioButton(u'Gebet')
        # noinspection PyUnresolvedReferences
        type_rb_gebet.toggled.connect(self.calculate_focus_policies)
        type_rb_gebet.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_gebet, Global.DATEINAMEN.RB_TYPE.GEBET)
        self.type_radio_buttons.append(type_rb_gebet)
        layout_l_2.addWidget(type_rb_gebet)

        type_rb_ansprache = QtGui.QRadioButton(u'Ansprache')
        # noinspection PyUnresolvedReferences
        type_rb_ansprache.toggled.connect(self.calculate_focus_policies)
        type_rb_ansprache.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_ansprache, Global.DATEINAMEN.RB_TYPE.ANSPRACHE)
        self.type_radio_buttons.append(type_rb_ansprache)
        layout_l_2.addWidget(type_rb_ansprache)

        type_rb_moderation = QtGui.QRadioButton(u'Moderation')
        # noinspection PyUnresolvedReferences
        type_rb_moderation.toggled.connect(self.calculate_focus_policies)
        type_rb_moderation.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_moderation, Global.DATEINAMEN.RB_TYPE.MODERATION)
        self.type_radio_buttons.append(type_rb_moderation)
        layout_l_2.addWidget(type_rb_moderation)

        type_rb_abschluss = QtGui.QRadioButton(u'Abschluss')
        # noinspection PyUnresolvedReferences
        type_rb_abschluss.toggled.connect(self.calculate_focus_policies)
        type_rb_abschluss.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_abschluss, Global.DATEINAMEN.RB_TYPE.ABSCHLUSS)
        self.type_radio_buttons.append(type_rb_abschluss)
        layout_l_2.addWidget(type_rb_abschluss)

        layout_l_2.addWidget(QtGui.QLabel(), 1)

        widget_l_3 = QtGui.QWidget()
        layout_l_3 = QtGui.QHBoxLayout()
        layout_l_3.setContentsMargins(0, 0, 0, 0)
        widget_l_3.setLayout(layout_l_3)
        self.left_layout.addWidget(widget_l_3)

        type_rb_chor = QtGui.QRadioButton(u'Chor')
        # noinspection PyUnresolvedReferences
        type_rb_chor.toggled.connect(self.calculate_focus_policies)
        type_rb_chor.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_chor, Global.DATEINAMEN.RB_TYPE.CHOR)
        self.type_radio_buttons.append(type_rb_chor)
        layout_l_3.addWidget(type_rb_chor)

        type_rb_allg_lied = QtGui.QRadioButton(u'Allg.Lied:')
        # noinspection PyUnresolvedReferences
        type_rb_allg_lied.toggled.connect(self.calculate_focus_policies)
        type_rb_allg_lied.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_allg_lied, Global.DATEINAMEN.RB_TYPE.ALLG_LIED)
        self.type_radio_buttons.append(type_rb_allg_lied)
        layout_l_3.addWidget(type_rb_allg_lied)

        self.line_edit_allg_lied = QtGui.QLineEdit()
        self.line_edit_allg_lied.setFixedWidth(40)
        # noinspection PyUnresolvedReferences
        self.line_edit_allg_lied.textChanged.connect(self.line_edit_allg_lied_text_changed)
        # noinspection PyUnresolvedReferences
        self.line_edit_allg_lied.returnPressed.connect(self.fifo_insert)
        layout_l_3.addWidget(self.line_edit_allg_lied)

        type_rb_musik = QtGui.QRadioButton(u'Musik')
        # noinspection PyUnresolvedReferences
        type_rb_musik.toggled.connect(self.calculate_focus_policies)
        type_rb_musik.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_musik, Global.DATEINAMEN.RB_TYPE.MUSIK)
        self.type_radio_buttons.append(type_rb_musik)
        layout_l_3.addWidget(type_rb_musik)

        type_rb_orchester = QtGui.QRadioButton(u'Orchester')
        # noinspection PyUnresolvedReferences
        type_rb_orchester.toggled.connect(self.calculate_focus_policies)
        type_rb_orchester.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_orchester, Global.DATEINAMEN.RB_TYPE.ORCHESTER)
        self.type_radio_buttons.append(type_rb_orchester)
        layout_l_3.addWidget(type_rb_orchester)

        layout_l_3.addWidget(QtGui.QLabel(), 1)

        widget_l_4 = QtGui.QWidget()
        layout_l_4 = QtGui.QHBoxLayout()
        layout_l_4.setContentsMargins(0, 0, 0, 0)
        widget_l_4.setLayout(layout_l_4)
        self.left_layout.addWidget(widget_l_4)

        type_rb_gruppenlied = QtGui.QRadioButton(u'Gruppenlied')
        # noinspection PyUnresolvedReferences
        type_rb_gruppenlied.toggled.connect(self.calculate_focus_policies)
        type_rb_gruppenlied.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_gruppenlied, Global.DATEINAMEN.RB_TYPE.GRUPPENLIED)
        self.type_radio_buttons.append(type_rb_gruppenlied)
        layout_l_4.addWidget(type_rb_gruppenlied)

        type_rb_jugendchor = QtGui.QRadioButton(u'Jugendchor')
        # noinspection PyUnresolvedReferences
        type_rb_jugendchor.toggled.connect(self.calculate_focus_policies)
        type_rb_jugendchor.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_jugendchor, Global.DATEINAMEN.RB_TYPE.JUGENDCHOR)
        self.type_radio_buttons.append(type_rb_jugendchor)
        layout_l_4.addWidget(type_rb_jugendchor)

        type_rb_kinderchor = QtGui.QRadioButton(u'Kinderchor')
        # noinspection PyUnresolvedReferences
        type_rb_kinderchor.toggled.connect(self.calculate_focus_policies)
        type_rb_kinderchor.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_kinderchor, Global.DATEINAMEN.RB_TYPE.KINDERCHOR)
        self.type_radio_buttons.append(type_rb_kinderchor)
        layout_l_4.addWidget(type_rb_kinderchor)

        layout_l_4.addStretch()

        widget_l_5 = QtGui.QWidget()
        layout_l_5 = QtGui.QHBoxLayout()
        layout_l_5.setContentsMargins(0, 0, 0, 0)
        widget_l_5.setLayout(layout_l_5)
        self.left_layout.addWidget(widget_l_5)

        type_rb_anspiel = QtGui.QRadioButton(u'Anspiel')
        # noinspection PyUnresolvedReferences
        type_rb_anspiel.toggled.connect(self.calculate_focus_policies)
        type_rb_anspiel.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_anspiel, Global.DATEINAMEN.RB_TYPE.ANSPIEL)
        self.type_radio_buttons.append(type_rb_anspiel)
        layout_l_5.addWidget(type_rb_anspiel)

        type_rb_zeugnis = QtGui.QRadioButton(u'Zeugnis')
        # noinspection PyUnresolvedReferences
        type_rb_zeugnis.toggled.connect(self.calculate_focus_policies)
        type_rb_zeugnis.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_zeugnis, Global.DATEINAMEN.RB_TYPE.ZEUGNIS)
        self.type_radio_buttons.append(type_rb_zeugnis)
        layout_l_5.addWidget(type_rb_zeugnis)

        type_rb_text = QtGui.QRadioButton(u'Text')
        # noinspection PyUnresolvedReferences
        type_rb_text.toggled.connect(self.calculate_focus_policies)
        type_rb_text.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_text, Global.DATEINAMEN.RB_TYPE.TEXT)
        self.type_radio_buttons.append(type_rb_text)
        layout_l_5.addWidget(type_rb_text)

        type_rb_gedicht = QtGui.QRadioButton(u'Gedicht')
        # noinspection PyUnresolvedReferences
        type_rb_gedicht.toggled.connect(self.calculate_focus_policies)
        type_rb_gedicht.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_gedicht, Global.DATEINAMEN.RB_TYPE.GEDICHT)
        self.type_radio_buttons.append(type_rb_gedicht)
        layout_l_5.addWidget(type_rb_gedicht)

        type_rb_prosa = QtGui.QRadioButton(u'Prosa')
        # noinspection PyUnresolvedReferences
        type_rb_prosa.toggled.connect(self.calculate_focus_policies)
        type_rb_prosa.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_prosa, Global.DATEINAMEN.RB_TYPE.PROSA)
        self.type_radio_buttons.append(type_rb_prosa)
        layout_l_5.addWidget(type_rb_prosa)

        type_rb_beitrag = QtGui.QRadioButton(u'Beitrag')
        # noinspection PyUnresolvedReferences
        type_rb_beitrag.toggled.connect(self.calculate_focus_policies)
        type_rb_beitrag.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_beitrag, Global.DATEINAMEN.RB_TYPE.BEITRAG)
        self.type_radio_buttons.append(type_rb_beitrag)
        layout_l_5.addWidget(type_rb_beitrag)

        layout_l_5.addStretch()

        widget_l_6 = QtGui.QWidget()
        layout_l_6 = QtGui.QHBoxLayout()
        layout_l_6.setContentsMargins(0, 0, 0, 0)
        widget_l_6.setLayout(layout_l_6)
        self.left_layout.addWidget(widget_l_6)

        type_rb_bibelstunde = QtGui.QRadioButton(u'Bibelstunde')
        # noinspection PyUnresolvedReferences
        type_rb_bibelstunde.toggled.connect(self.calculate_focus_policies)
        type_rb_bibelstunde.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_bibelstunde, Global.DATEINAMEN.RB_TYPE.BIBELSTUNDE)
        self.type_radio_buttons.append(type_rb_bibelstunde)
        layout_l_6.addWidget(type_rb_bibelstunde)

        type_rb_thema = QtGui.QRadioButton(u'Thema')
        # noinspection PyUnresolvedReferences
        type_rb_thema.toggled.connect(self.calculate_focus_policies)
        type_rb_thema.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_thema, Global.DATEINAMEN.RB_TYPE.THEMA)
        self.type_radio_buttons.append(type_rb_thema)
        layout_l_6.addWidget(type_rb_thema)

        layout_l_6.addWidget(QtGui.QLabel(), 1)

        widget_l_7o = QtGui.QWidget()
        layout_l_7o = QtGui.QHBoxLayout()
        layout_l_7o.setContentsMargins(0, 0, 0, 0)
        widget_l_7o.setLayout(layout_l_7o)
        self.left_layout.addWidget(widget_l_7o)

        type_rb_definition = QtGui.QRadioButton()
        # noinspection PyUnresolvedReferences
        type_rb_definition.toggled.connect(self.calculate_focus_policies)
        type_rb_definition.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_definition, Global.DATEINAMEN.RB_TYPE.DEFINITION)
        self.type_radio_buttons.append(type_rb_definition)
        layout_l_7o.addWidget(type_rb_definition)

        self.type_combo_box = QtGui.QComboBox()
        # noinspection PyUnresolvedReferences
        self.type_combo_box.currentIndexChanged.connect(self.calculate_focus_policies)
        # noinspection PyUnresolvedReferences
        self.type_combo_box.currentIndexChanged.connect(self.type_combo_box_index_changed)
        # noinspection PyUnresolvedReferences
        self.type_combo_box.textChanged.connect(self.type_combo_validate_selection)
        self.type_combo_box.setEditable(True)
        completer = self.type_combo_box.completer()
        completer.setCompletionMode(QtGui.QCompleter.PopupCompletion)
        self.type_combo_box.setCompleter(completer)
        layout_l_7o.addWidget(self.type_combo_box, 1)

        widget_l_7 = QtGui.QWidget()
        layout_l_7 = QtGui.QHBoxLayout()
        layout_l_7.setContentsMargins(0, 0, 0, 0)
        widget_l_7.setLayout(layout_l_7)
        self.left_layout.addWidget(widget_l_7)

        type_rb_sonst = QtGui.QRadioButton(u'Sonstiges:')
        # noinspection PyUnresolvedReferences
        type_rb_sonst.toggled.connect(self.calculate_focus_policies)
        type_rb_sonst.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_sonst, Global.DATEINAMEN.RB_TYPE.SONST)
        self.type_radio_buttons.append(type_rb_sonst)
        layout_l_7.addWidget(type_rb_sonst)

        self.type_line_edit = MtWidgets.CompleterLineEdit()
        self.type_line_edit.setCompleter(self.model.sonst_file_completer)
        # noinspection PyUnresolvedReferences
        self.type_line_edit.textChanged.connect(self.type_line_edit_text_changed)
        # noinspection PyUnresolvedReferences
        self.type_line_edit.returnPressed.connect(self.fifo_insert)
        layout_l_7.addWidget(self.type_line_edit)

        type_rb_nichts = QtGui.QRadioButton(u'Nichts')
        # noinspection PyUnresolvedReferences
        type_rb_nichts.toggled.connect(self.calculate_focus_policies)
        type_rb_nichts.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.type_radio_button_group.addButton(type_rb_nichts, Global.DATEINAMEN.RB_TYPE.NICHTS)
        self.type_radio_buttons.append(type_rb_nichts)
        layout_l_7.addWidget(type_rb_nichts)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        self.left_layout.addWidget(line)

        widget_l_8 = QtGui.QWidget()
        layout_l_8 = QtGui.QHBoxLayout()
        layout_l_8.setContentsMargins(0, 0, 0, 0)
        widget_l_8.setLayout(layout_l_8)
        self.left_layout.addWidget(widget_l_8)

        self.name_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.name_radio_button_group.buttonReleased[int].connect(self.name_radio_button_released)
        self.name_radio_buttons = list()

        name_rb_name = QtGui.QRadioButton(u'Vortragender:')
        # noinspection PyUnresolvedReferences
        name_rb_name.toggled.connect(self.calculate_focus_policies)
        name_rb_name.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.name_radio_button_group.addButton(name_rb_name, 0)
        self.name_radio_buttons.append(name_rb_name)
        layout_l_8.addWidget(name_rb_name)

        self.name_line_edit = MtWidgets.CompleterLineEdit()
        self.name_line_edit.setCompleter(self.model.name_completer)
        # noinspection PyUnresolvedReferences
        self.name_line_edit.textChanged.connect(self.name_line_edit_text_changed)
        # noinspection PyUnresolvedReferences
        self.name_line_edit.returnPressed.connect(self.fifo_insert)
        layout_l_8.addWidget(self.name_line_edit, 1)

        name_rb_nichts = QtGui.QRadioButton(u'Keiner')
        # noinspection PyUnresolvedReferences
        name_rb_nichts.toggled.connect(self.calculate_focus_policies)
        name_rb_nichts.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.name_radio_button_group.addButton(name_rb_nichts, 1)
        self.name_radio_buttons.append(name_rb_nichts)
        layout_l_8.addWidget(name_rb_nichts)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        self.left_layout.addWidget(line)

        widget_l_9 = QtGui.QWidget()
        layout_l_9 = QtGui.QHBoxLayout()
        layout_l_9.setContentsMargins(0, 0, 0, 0)
        widget_l_9.setLayout(layout_l_9)
        self.left_layout.addWidget(widget_l_9)

        self.stelle_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.stelle_radio_button_group.buttonReleased[int].connect(self.stelle_radio_button_released)
        self.stelle_radio_buttons = list()

        stelle_rb_stelle = QtGui.QRadioButton(u'Bibelstelle:')
        # noinspection PyUnresolvedReferences
        stelle_rb_stelle.toggled.connect(self.calculate_focus_policies)
        stelle_rb_stelle.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.stelle_radio_button_group.addButton(stelle_rb_stelle, 0)
        self.stelle_radio_buttons.append(stelle_rb_stelle)
        layout_l_9.addWidget(stelle_rb_stelle)

        self.line_edit_stelle_buch = MtWidgets.CompleterLineEdit()
        self.line_edit_stelle_buch.setCompleter(self.model.stelle_list_completer)
        self.line_edit_stelle_buch.setFixedWidth(65)
        # noinspection PyUnresolvedReferences
        self.line_edit_stelle_buch.textChanged.connect(self.line_edit_stelle_buch_text_changed)
        layout_l_9.addWidget(self.line_edit_stelle_buch)

        self.line_edit_stelle = QtGui.QLineEdit()
        # noinspection PyUnresolvedReferences
        self.line_edit_stelle.textChanged.connect(self.line_edit_stelle_text_changed)
        # noinspection PyUnresolvedReferences
        self.line_edit_stelle.returnPressed.connect(self.fifo_insert)
        layout_l_9.addWidget(self.line_edit_stelle)

        stelle_rb_nichts = QtGui.QRadioButton(u'Nichts')
        # noinspection PyUnresolvedReferences
        stelle_rb_nichts.toggled.connect(self.calculate_focus_policies)
        stelle_rb_nichts.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.stelle_radio_button_group.addButton(stelle_rb_nichts, 1)
        self.stelle_radio_buttons.append(stelle_rb_nichts)
        layout_l_9.addWidget(stelle_rb_nichts)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        self.left_layout.addWidget(line)

        widget_l_10 = QtGui.QWidget()
        layout_l_10 = QtGui.QHBoxLayout()
        layout_l_10.setContentsMargins(0, 0, 0, 0)
        widget_l_10.setLayout(layout_l_10)
        self.left_layout.addWidget(widget_l_10)

        self.titel_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.titel_radio_button_group.buttonReleased[int].connect(self.titel_radio_button_released)
        self.titel_radio_buttons = list()

        titel_rb_titel = QtGui.QRadioButton(u'Titel:')
        # noinspection PyUnresolvedReferences
        titel_rb_titel.toggled.connect(self.calculate_focus_policies)
        titel_rb_titel.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.titel_radio_button_group.addButton(titel_rb_titel, 0)
        self.titel_radio_buttons.append(titel_rb_titel)
        layout_l_10.addWidget(titel_rb_titel)

        self.titel_line_edit = MtWidgets.CompleterLineEdit()
        self.titel_line_edit.setCompleter(self.model.titel_file_completer)
        # noinspection PyUnresolvedReferences
        self.titel_line_edit.textChanged.connect(self.titel_line_edit_text_changed)
        # noinspection PyUnresolvedReferences
        self.titel_line_edit.returnPressed.connect(self.fifo_insert)
        layout_l_10.addWidget(self.titel_line_edit)

        titel_rb_nichts = QtGui.QRadioButton(u'Nichts')
        # noinspection PyUnresolvedReferences
        titel_rb_nichts.toggled.connect(self.calculate_focus_policies)
        titel_rb_nichts.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.titel_radio_button_group.addButton(titel_rb_nichts, 1)
        self.titel_radio_buttons.append(titel_rb_nichts)
        layout_l_10.addWidget(titel_rb_nichts)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        self.left_layout.addWidget(line)

        widget_l_11 = QtGui.QWidget()
        layout_l_11 = QtGui.QHBoxLayout()
        layout_l_11.setContentsMargins(0, 0, 0, 0)
        widget_l_11.setLayout(layout_l_11)
        self.left_layout.addWidget(widget_l_11)

        self.sonst_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.sonst_radio_button_group.buttonReleased[int].connect(self.sonst_radio_button_released)
        self.sonst_radio_buttons = list()

        sonst_rb_sonst = QtGui.QRadioButton(u'Sonst:')
        # noinspection PyUnresolvedReferences
        sonst_rb_sonst.toggled.connect(self.calculate_focus_policies)
        sonst_rb_sonst.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.sonst_radio_button_group.addButton(sonst_rb_sonst, 0)
        self.sonst_radio_buttons.append(sonst_rb_sonst)
        layout_l_11.addWidget(sonst_rb_sonst)

        self.sonst_line_edit = QtGui.QLineEdit()
        # noinspection PyUnresolvedReferences
        self.sonst_line_edit.textChanged.connect(self.sonst_line_edit_text_changed)
        # noinspection PyUnresolvedReferences
        self.sonst_line_edit.returnPressed.connect(self.fifo_insert)
        layout_l_11.addWidget(self.sonst_line_edit)

        sonst_rb_nichts = QtGui.QRadioButton(u'Nichts')
        # noinspection PyUnresolvedReferences
        sonst_rb_nichts.toggled.connect(self.calculate_focus_policies)
        sonst_rb_nichts.setFocusPolicy(QtCore.Qt.ClickFocus)
        self.sonst_radio_button_group.addButton(sonst_rb_nichts, 1)
        self.sonst_radio_buttons.append(sonst_rb_nichts)
        layout_l_11.addWidget(sonst_rb_nichts)

        widget = QtGui.QWidget()
        self.left_layout.addWidget(widget, 1)

        # right side
        widget_r_1 = QtGui.QWidget()
        right_layout.addWidget(widget_r_1)
        layout_r_1 = QtGui.QHBoxLayout()
        layout_r_1.setContentsMargins(0, 0, 0, 0)
        widget_r_1.setLayout(layout_r_1)

        self.button_fifo_insert = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-73-240.png'))
        self.button_fifo_insert.setIcon(icon)
        self.button_fifo_insert.setToolTip(u'Dateinamen in Speicher einfügen')
        # noinspection PyUnresolvedReferences
        self.button_fifo_insert.clicked.connect(self.fifo_insert)
        layout_r_1.addWidget(self.button_fifo_insert)

        self.button_fifo_copy = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-copy-7-240.png'))
        self.button_fifo_copy.setIcon(icon)
        self.button_fifo_copy.setToolTip(u'In Zwischenablage kopieren')
        # noinspection PyUnresolvedReferences
        self.button_fifo_copy.clicked.connect(self.model.fifo_copy_to_clipboard)
        layout_r_1.addWidget(self.button_fifo_copy)

        self.button_fifo_undo = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-undo-2-240.png'))
        self.button_fifo_undo.setIcon(icon)
        self.button_fifo_undo.setToolTip(u'Dateinamen einfügen rückgängig machen')
        # noinspection PyUnresolvedReferences
        self.button_fifo_undo.clicked.connect(self.undo)
        layout_r_1.addWidget(self.button_fifo_undo)

        self.button_fifo_redo = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-redo-2-240.png'))
        self.button_fifo_redo.setIcon(icon)
        self.button_fifo_redo.setToolTip(u'Dateinamen wieder einfügen')
        # noinspection PyUnresolvedReferences
        self.button_fifo_redo.clicked.connect(self.redo)
        layout_r_1.addWidget(self.button_fifo_redo)

        self.button_fifo_renumber = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-hashtag-1-240.png'))
        self.button_fifo_renumber.setIcon(icon)
        self.button_fifo_renumber.setToolTip(u'Neu nummerieren')
        # noinspection PyUnresolvedReferences
        self.button_fifo_renumber.clicked.connect(self.model.renumber_fifo)
        layout_r_1.addWidget(self.button_fifo_renumber)

        layout_r_1.addWidget(QtGui.QLabel(u''), 1)

        self.button_reset_fifo = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-12-240.png'))
        self.button_reset_fifo.setIcon(icon)
        self.button_reset_fifo.setToolTip(u'Speicher löschen')
        # noinspection PyUnresolvedReferences
        self.button_reset_fifo.clicked.connect(self.model.clear_fifo)
        layout_r_1.addWidget(self.button_reset_fifo)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        right_layout.addWidget(line)

        self.table_widget_fifo = FiFoWidget(self.model)
        right_layout.addWidget(self.table_widget_fifo, 1)

        widget_r_3 = QtGui.QWidget()
        right_layout.addWidget(widget_r_3)
        layout_r_3 = QtGui.QHBoxLayout()
        layout_r_3.setContentsMargins(0, 0, 0, 0)
        widget_r_3.setLayout(layout_r_3)

        self.button_save_dateinamen = QtGui.QPushButton(u'Speichern')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-save-3-240.png'))
        self.button_save_dateinamen.setIcon(icon)
        self.button_save_dateinamen.setToolTip(u'Dateinamen speichern')
        # noinspection PyUnresolvedReferences
        self.button_save_dateinamen.clicked.connect(self.save_dateinamen)
        layout_r_3.addWidget(self.button_save_dateinamen)

        self.button_load_dateinamen = QtGui.QPushButton(u'Öffnen')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-disk-15-240.png'))
        self.button_load_dateinamen.setIcon(icon)
        self.button_load_dateinamen.setToolTip(u'Dateinamen öffnen')
        # noinspection PyUnresolvedReferences
        self.button_load_dateinamen.clicked.connect(self.load_dateinamen)
        layout_r_3.addWidget(self.button_load_dateinamen)

        layout_r_3.addStretch()

        self.clear_view()
        self.model.fifo_changed.connect(self.load_fifo)
        self.model.model_changed.connect(self.load_fifo)
        self.model.type_definitions_changed.connect(self.load_type_definitions)
        self.load_fifo()
        self.load_type_definitions()

        self.load_dateiname(self.model.last_dateiname)

    def reset_counter(self):
        self.counter_spin_box.setValue(1)

    def handle_button_arrow_left_clicked(self):
        button = self.sender()
        index = self.table_widget_fifo.indexAt(button.parentWidget().pos())
        if index.isValid():
            old_dateiname = self.get_dateiname()
            self.model.shown_dateinamen.append(old_dateiname)
            self.load_dateiname(self.model.get_fifo_at_index(index.row()))
            self.model.set_dateiname_under_edit(index.row())
        self.model.fifo_changed.emit()

    def handle_button_arrow_right_clicked(self):
        button = self.sender()
        index = self.table_widget_fifo.indexAt(button.parentWidget().pos())
        if index.isValid():
            if self.fifo_insert(index.row()):
                try:
                    dateiname = self.model.shown_dateinamen.pop()
                except IndexError:
                    pass
                else:
                    self.load_dateiname(dateiname)
        self.model.fifo_changed.emit()

    def handle_button_delete_clicked(self):
        button = self.sender()
        index = self.table_widget_fifo.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.delete_fifo_at_index(index.row())

    def type_combo_box_index_changed(self):
        self.type_radio_buttons[Global.DATEINAMEN.RB_TYPE.DEFINITION].setChecked(True)
        self.type_combo_validate_selection()
        self.type_radio_button_released(Global.DATEINAMEN.RB_TYPE.DEFINITION)

    def __type_combo_box_check_valid_input(self):
        combo_box_index = self.type_combo_box.currentIndex()
        try:
            type_definition = self.model.type_definitions[combo_box_index]
            if type_definition.display_name == self.type_combo_box.currentText():
                return True
            else:
                return False
        except IndexError:
            return False

    def type_combo_validate_selection(self):
        if self.type_radio_buttons[Global.DATEINAMEN.RB_TYPE.DEFINITION].isChecked() or self.type_combo_box.hasFocus():
            if self.__type_combo_box_check_valid_input():
                self.type_combo_box.lineEdit().setStyleSheet(u"")
            else:
                self.type_combo_box.lineEdit().setStyleSheet(u"background-color: {0};".format(
                    Global.DATEINAMEN.EDITOR.BACKGROUND_COLOR_ERROR))
        else:
            self.type_combo_box.lineEdit().setStyleSheet(u"")

    def type_radio_button_released(self, index):
        if index == Global.DATEINAMEN.RB_TYPE.DEFINITION:
            combo_box_index = self.type_combo_box.currentIndex()
            try:
                type_definition = self.model.type_definitions[combo_box_index]
            except IndexError:
                type_definition = None
        else:
            type_definition = Global.DATEINAMEN.RB_TYPE.TYPE_DEFINITIONS[index]

        if type_definition:
            if type_definition.sonst_radio_button:
                self.sonst_line_edit.setFocus()
                self.sonst_line_edit.selectAll()
                self.sonst_radio_buttons[Global.DATEINAMEN.RB_SONST.SONST].setChecked(True)
            else:
                self.sonst_radio_buttons[Global.DATEINAMEN.RB_SONST.NICHTS].setChecked(True)
            if type_definition.titel_radio_button:
                self.titel_line_edit.setFocus()
                self.titel_line_edit.selectAll()
                self.titel_radio_buttons[Global.DATEINAMEN.RB_TITEL.TITEL].setChecked(True)
            else:
                self.titel_radio_buttons[Global.DATEINAMEN.RB_TITEL.NICHTS].setChecked(True)
            if type_definition.stelle_radio_button:
                self.line_edit_stelle_buch.setFocus()
                self.line_edit_stelle_buch.selectAll()
                self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.STELLE].setChecked(True)
            else:
                self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.NICHTS].setChecked(True)
            if type_definition.name_radio_button:
                self.name_line_edit.setFocus()
                self.name_line_edit.selectAll()
                self.name_radio_buttons[Global.DATEINAMEN.RB_NAME.NAME].setChecked(True)
            else:
                self.name_radio_buttons[Global.DATEINAMEN.RB_NAME.NICHTS].setChecked(True)
        else:
            self.type_line_edit.setFocus()
            self.type_line_edit.selectAll()

        if index == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
            self.line_edit_allg_lied.setFocus()
            self.line_edit_allg_lied.selectAll()

        self.type_combo_validate_selection()

    def name_radio_button_released(self, index):
        if index == Global.DATEINAMEN.RB_NAME.NAME:
            self.name_line_edit.setFocus()
            self.name_line_edit.selectAll()
        elif index == Global.DATEINAMEN.RB_NAME.NICHTS:
            if self.stelle_radio_button_group.checkedId() == Global.DATEINAMEN.RB_STELLE.STELLE:
                self.line_edit_stelle_buch.setFocus()
                self.line_edit_stelle_buch.selectAll()
            elif self.titel_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TITEL.TITEL:
                self.titel_line_edit.setFocus()
                self.titel_line_edit.selectAll()
            elif self.sonst_radio_button_group.checkedId() == Global.DATEINAMEN.RB_SONST.SONST:
                self.sonst_line_edit.setFocus()
                self.sonst_line_edit.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
                self.line_edit_allg_lied.setFocus()
                self.line_edit_allg_lied.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.SONST:
                self.type_line_edit.setFocus()
                self.type_line_edit.selectAll()

    def stelle_radio_button_released(self, index):
        if index == Global.DATEINAMEN.RB_STELLE.STELLE:
            self.line_edit_stelle_buch.setFocus()
            self.line_edit_stelle_buch.selectAll()
        elif index == Global.DATEINAMEN.RB_STELLE.NICHTS:
            if self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TITEL.TITEL:
                self.titel_line_edit.setFocus()
                self.titel_line_edit.selectAll()
            elif self.sonst_radio_button_group.checkedId() == Global.DATEINAMEN.RB_SONST.SONST:
                self.sonst_line_edit.setFocus()
                self.sonst_line_edit.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
                self.line_edit_allg_lied.setFocus()
                self.line_edit_allg_lied.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.SONST:
                self.type_line_edit.setFocus()
                self.type_line_edit.selectAll()
            elif self.name_radio_button_group.checkedId() == Global.DATEINAMEN.RB_NAME.NAME:
                self.name_line_edit.setFocus()
                self.name_line_edit.selectAll()

    def titel_radio_button_released(self, index):
        if index == Global.DATEINAMEN.RB_TITEL.TITEL:
            self.titel_line_edit.setFocus()
            self.titel_line_edit.selectAll()
        elif index == Global.DATEINAMEN.RB_TITEL.NICHTS:
            if self.sonst_radio_button_group.checkedId() == Global.DATEINAMEN.RB_SONST.SONST:
                self.sonst_line_edit.setFocus()
                self.sonst_line_edit.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
                self.line_edit_allg_lied.setFocus()
                self.line_edit_allg_lied.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.SONST:
                self.type_line_edit.setFocus()
                self.type_line_edit.selectAll()
            elif self.name_radio_button_group.checkedId() == Global.DATEINAMEN.RB_NAME.NAME:
                self.name_line_edit.setFocus()
                self.name_line_edit.selectAll()
            elif self.stelle_radio_button_group.checkedId() == Global.DATEINAMEN.RB_STELLE.STELLE:
                self.line_edit_stelle_buch.setFocus()
                self.line_edit_stelle_buch.selectAll()

    def sonst_radio_button_released(self, index):
        if index == Global.DATEINAMEN.RB_SONST.SONST:
            self.sonst_line_edit.setFocus()
            self.sonst_line_edit.selectAll()
        elif index == Global.DATEINAMEN.RB_SONST.NICHTS:
            if self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
                self.line_edit_allg_lied.setFocus()
                self.line_edit_allg_lied.selectAll()
            elif self.type_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TYPE.SONST:
                self.type_line_edit.setFocus()
                self.type_line_edit.selectAll()
            elif self.name_radio_button_group.checkedId() == Global.DATEINAMEN.RB_NAME.NAME:
                self.name_line_edit.setFocus()
                self.name_line_edit.selectAll()
            elif self.stelle_radio_button_group.checkedId() == Global.DATEINAMEN.RB_STELLE.STELLE:
                self.line_edit_stelle_buch.setFocus()
                self.line_edit_stelle_buch.selectAll()
            elif self.titel_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TITEL.TITEL:
                self.titel_line_edit.setFocus()
                self.titel_line_edit.selectAll()

    def line_edit_allg_lied_text_changed(self):
        if self.line_edit_allg_lied.text():
            self.type_radio_buttons[Global.DATEINAMEN.RB_TYPE.ALLG_LIED].setChecked(True)
            self.name_radio_buttons[Global.DATEINAMEN.RB_NAME.NICHTS].setChecked(True)
            self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.NICHTS].setChecked(True)
            self.titel_radio_buttons[Global.DATEINAMEN.RB_TITEL.NICHTS].setChecked(True)
            self.sonst_radio_buttons[Global.DATEINAMEN.RB_SONST.NICHTS].setChecked(True)

    def type_line_edit_text_changed(self):
        if self.type_line_edit.text():
            self.type_radio_buttons[Global.DATEINAMEN.RB_TYPE.SONST].setChecked(True)

    def name_line_edit_text_changed(self):
        if self.name_line_edit.text():
            self.name_radio_buttons[Global.DATEINAMEN.RB_NAME.NAME].setChecked(True)
        else:
            self.name_radio_buttons[Global.DATEINAMEN.RB_NAME.NICHTS].setChecked(True)

    def line_edit_stelle_buch_text_changed(self):
        if self.line_edit_stelle_buch.text():
            self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.STELLE].setChecked(True)
        elif not self.line_edit_stelle.text():
            self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.NICHTS].setChecked(True)

    def line_edit_stelle_text_changed(self):
        if self.line_edit_stelle.text():
            self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.STELLE].setChecked(True)
        elif not self.line_edit_stelle_buch.text():
            self.stelle_radio_buttons[Global.DATEINAMEN.RB_STELLE.NICHTS].setChecked(True)

    def titel_line_edit_text_changed(self):
        if self.titel_line_edit.text():
            self.titel_radio_buttons[Global.DATEINAMEN.RB_TITEL.TITEL].setChecked(True)
        else:
            self.titel_radio_buttons[Global.DATEINAMEN.RB_TITEL.NICHTS].setChecked(True)

    def sonst_line_edit_text_changed(self):
        if self.sonst_line_edit.text():
            self.sonst_radio_buttons[Global.DATEINAMEN.RB_SONST.SONST].setChecked(True)
        else:
            self.sonst_radio_buttons[Global.DATEINAMEN.RB_SONST.NICHTS].setChecked(True)

    def calculate_focus_policies(self):
        if self.name_radio_button_group.checkedId() == Global.DATEINAMEN.RB_NAME.NICHTS:
            self.name_line_edit.setFocusPolicy(QtCore.Qt.ClickFocus)
        else:
            self.name_line_edit.setFocusPolicy(QtCore.Qt.StrongFocus)
        if self.stelle_radio_button_group.checkedId() == Global.DATEINAMEN.RB_STELLE.NICHTS:
            self.line_edit_stelle_buch.setFocusPolicy(QtCore.Qt.ClickFocus)
            self.line_edit_stelle.setFocusPolicy(QtCore.Qt.ClickFocus)
        else:
            self.line_edit_stelle_buch.setFocusPolicy(QtCore.Qt.StrongFocus)
            self.line_edit_stelle.setFocusPolicy(QtCore.Qt.StrongFocus)
        if self.titel_radio_button_group.checkedId() == Global.DATEINAMEN.RB_TITEL.NICHTS:
            self.titel_line_edit.setFocusPolicy(QtCore.Qt.ClickFocus)
        else:
            self.titel_line_edit.setFocusPolicy(QtCore.Qt.StrongFocus)
        if self.sonst_radio_button_group.checkedId() == Global.DATEINAMEN.RB_SONST.NICHTS:
            self.sonst_line_edit.setFocusPolicy(QtCore.Qt.ClickFocus)
        else:
            self.sonst_line_edit.setFocusPolicy(QtCore.Qt.StrongFocus)

    def get_dateiname(self):
        dateiname = Dateiname(self.model)
        dateiname.counter = self.counter_spin_box.value()
        dateiname.type = self.type_radio_button_group.checkedId()
        dateiname.general_song = self.line_edit_allg_lied.text()
        dateiname.type_other_text = self.type_line_edit.text()
        dateiname.name = self.name_radio_button_group.checkedId()
        dateiname.name_text = self.name_line_edit.text()
        dateiname.scripture = self.stelle_radio_button_group.checkedId()
        dateiname.scripture_book = self.line_edit_stelle_buch.text()
        dateiname.scripture_text = self.line_edit_stelle.text()
        dateiname.title = self.titel_radio_button_group.checkedId()
        dateiname.title_text = self.titel_line_edit.text()
        dateiname.other = self.sonst_radio_button_group.checkedId()
        dateiname.other_text = self.sonst_line_edit.text()
        if dateiname.type == Global.DATEINAMEN.RB_TYPE.DEFINITION:
            if self.model.type_definitions:
                try:
                    dateiname.type_definition = self.model.type_definitions[self.type_combo_box.currentIndex()]
                except IndexError:
                    dateiname.type_definition = None
            else:
                dateiname.type_definition = None
        else:
            dateiname.type_definition = Global.DATEINAMEN.RB_TYPE.TYPE_DEFINITIONS[dateiname.type]
        return dateiname

    def load_dateiname(self, dateiname):
        self.counter_spin_box.setValue(dateiname.counter)
        self.line_edit_allg_lied.setText(dateiname.general_song)
        self.type_line_edit.setText(dateiname.type_other_text)
        self.name_line_edit.setText(dateiname.name_text)
        self.line_edit_stelle_buch.setText(dateiname.scripture_book)
        self.line_edit_stelle.setText(dateiname.scripture_text)
        self.titel_line_edit.setText(dateiname.title_text)
        self.sonst_line_edit.setText(dateiname.other_text)
        self.type_radio_buttons[dateiname.type].setChecked(True)
        if dateiname.type == Global.DATEINAMEN.RB_TYPE.DEFINITION:
            if dateiname.type_definition not in self.model.type_definitions:
                self.model.add_to_type_definitions(dateiname.type_definition)
            self.load_type_definitions()
            index = self.model.type_definitions.index(dateiname.type_definition)
            self.type_combo_box.setCurrentIndex(index)
        self.name_radio_buttons[dateiname.name].setChecked(True)
        self.stelle_radio_buttons[dateiname.scripture].setChecked(True)
        self.titel_radio_buttons[dateiname.title].setChecked(True)
        self.sonst_radio_buttons[dateiname.other].setChecked(True)

    def fifo_insert(self, index=None):
        dateiname = self.get_dateiname()
        errors = dateiname.get_errors()
        if dateiname.type == Global.DATEINAMEN.RB_TYPE.DEFINITION and \
                not self.__type_combo_box_check_valid_input():
            message = u'Fehler beim Erstellen des Dateinamens: \nWähle einen Typ aus.'
            self.application.logger.log(message, __name__, MediaToolsLogger.SEVERITY_WARNING, True, True)
        elif errors:
            message = u'Fehler beim Erstellen des Dateinamens: \n' + '\n'.join(errors)
            self.application.logger.log(message, __name__, MediaToolsLogger.SEVERITY_WARNING, True, True)
        else:
            if index is None:
                if self.model.add_to_fifo(dateiname):
                    self.counter_spin_box.setValue(self.counter_spin_box.value() + 1)
            else:
                self.model.set_fifo_at_index(index, dateiname)

            # collect data
            type_other_text = dateiname.correct_text(dateiname.type_other_text)
            self.type_line_edit.setText(type_other_text)
            self.model.sonst_file_completer.add_item(type_other_text)
            name_text = dateiname.correct_name(dateiname.name_text)
            self.name_line_edit.setText(name_text)
            self.model.name_completer.add_items(name_text)
            scripture_text = dateiname.correct_text(dateiname.scripture_text)
            self.line_edit_stelle.setText(scripture_text)
            title_text = dateiname.correct_text(dateiname.title_text)
            self.titel_line_edit.setText(title_text)
            self.model.titel_file_completer.add_item(title_text)
            other_text = dateiname.correct_text(dateiname.other_text)
            self.sonst_line_edit.setText(other_text)

            return True
        return False

    def load_fifo(self):
        self.table_widget_fifo.clear()
        counter = 0
        for dateiname in self.model.fifo:
            item = QtGui.QListWidgetItem()
            widget = QtGui.QWidget()
            layout = QtGui.QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            widget.setLayout(layout)

            button_arrow_left = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-3-240-left.png'))
            button_arrow_left.setIcon(icon)
            button_arrow_left.setToolTip(u'Dateiname bearbeiten')
            # noinspection PyUnresolvedReferences
            button_arrow_left.clicked.connect(self.handle_button_arrow_left_clicked)
            button_arrow_left.setFixedWidth(24)
            if self.model.item_under_edit:
                button_arrow_left.setEnabled(False)
            else:
                button_arrow_left.setEnabled(True)
            if not self.model.item_under_edit:
                layout.addWidget(button_arrow_left)

            button_arrow_right = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-3-240-right.png'))
            button_arrow_right.setIcon(icon)
            button_arrow_right.setToolTip(u'Dateiname überschreiben')
            # noinspection PyUnresolvedReferences
            button_arrow_right.clicked.connect(self.handle_button_arrow_right_clicked)
            button_arrow_right.setFixedWidth(24)
            if self.model.item_under_edit and dateiname.under_edit:
                button_arrow_right.setEnabled(True)
            else:
                button_arrow_right.setEnabled(False)
            if self.model.item_under_edit:
                layout.addWidget(button_arrow_right)

            label_dateiname = QtGui.QLabel(unicode(dateiname))
            layout.addWidget(label_dateiname)
            layout.addStretch()

            button_delete = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-4-240.png'))
            button_delete.setIcon(icon)
            button_delete.setToolTip(u'Element löschen')
            # noinspection PyUnresolvedReferences
            button_delete.clicked.connect(self.handle_button_delete_clicked)
            button_delete.setFixedWidth(24)
            layout.addWidget(button_delete)

            widget_size = widget.sizeHint()
            item_size = QtCore.QSize(widget_size.width() + 16, widget_size.height())
            item.setSizeHint(item_size)

            self.table_widget_fifo.insertItem(counter, item)
            self.table_widget_fifo.setItemWidget(item, widget)

            counter += 1

        if counter:
            self.button_reset_fifo.setEnabled(True)
        else:
            self.button_reset_fifo.setEnabled(False)

        if counter and not self.model.item_under_edit:
            self.button_fifo_copy.setEnabled(True)
            self.button_fifo_undo.setEnabled(True)
        else:
            self.button_fifo_copy.setEnabled(False)
            self.button_fifo_undo.setEnabled(False)

        if self.model.item_under_edit:
            self.button_fifo_insert.setEnabled(False)
        else:
            self.button_fifo_insert.setEnabled(True)

        if self.model.has_undone and not self.model.item_under_edit:
            self.button_fifo_redo.setEnabled(True)
        else:
            self.button_fifo_redo.setEnabled(False)

        if len(self.model.fifo) > 1:
            self.button_fifo_renumber.setEnabled(True)
        else:
            self.button_fifo_renumber.setEnabled(False)

        if self.model.item_under_edit:
            self.setStyleSheet(u"background-color:{0};".format(
                Global.DATEINAMEN.EDITOR.BACKGROUND_COLOR_EDIT))
        else:
            self.setStyleSheet(u"")

    def load_type_definitions(self):
        self.type_combo_box.clear()
        self.type_combo_box.addItems([unicode(i) for i in self.model.type_definitions])

    def undo(self):
        self.model.undo()
        counter = self.counter_spin_box.value()
        self.counter_spin_box.setValue(counter - 1)

    def redo(self):
        counter = self.counter_spin_box.value()
        if self.model.redo(counter):
            self.counter_spin_box.setValue(counter + 1)

    def clear_view(self):
        dateiname = Dateiname(self.model)
        counter = self.counter_spin_box.value()
        self.load_dateiname(dateiname)
        self.counter_spin_box.setValue(counter)

    def save_dateiname(self):
        self.model.last_dateiname = self.get_dateiname()

    def load_dateinamen(self):
        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.expanduser(u'~')

        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window, u'Dateinamen öffnen',
                                                      default_path,
                                                      u'JSON (*.json)')
        file_name = file_name[0]
        if not file_name:
            return
        if not os.path.isfile(file_name):
            self.application.logger.log(u'Datei konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            self.__save_path(file_name)
            try:
                read_file = open(file_name, 'r')
            except IOError:
                self.application.logger.log(u'Datei konnte nicht geöffnet werden', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            try:
                json_data = json.load(read_file)
            except Exception as e:
                self.application.logger.log(u'Datei konnte nicht ausgelesen werden'.format(str(e)), __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            else:
                read_file.close()
            try:
                dateinamen_dicts = list(json_data)
            except Exception:
                self.application.logger.log(u'Unerwartetes Dateiformat', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            errors = 0
            for dateiname in dateinamen_dicts:
                try:
                    self.model.add_to_fifo(Dateiname(self.model, dateiname))
                except Exception as e:
                    errors += 1
                    self.application.logger.log(u'Failed to deserialize Dateiname: {0}'.format(str(e)), __name__,
                                                MediaToolsLogger.SEVERITY_ERROR, False, False)
                if errors:
                    self.application.logger.log(u'{0} von {1} Dateinamen konnten nicht geladen '
                                                u'werden'.format(errors, len(dateinamen_dicts)), __name__,
                                                MediaToolsLogger.SEVERITY_WARNING, True, True)

    def save_dateinamen(self):
        if not self.model.fifo:
            self.application.logger.log(u'Keine Dateinamen im Speicher.', __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, True)
            return

        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'Dateinamen.json')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Dateinamen speichern',
                                                      default_path,
                                                      u'JSON (*.json)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)

            try:
                write_file = open(file_name, 'w')
            except IOError as e:
                self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
            else:
                dateinamen = [i.get_dict() for i in self.model.fifo]
                try:
                    json.dump(dateinamen, write_file)
                except Exception as e:
                    self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                                __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
                write_file.close()

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.DATEINAMEN.FILES.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.DATEINAMEN.FILES.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


class FiFoWidget(QtGui.QListWidget):

    def __init__(self, model):
        super(self.__class__, self).__init__()
        self.model = model

        self.setSortingEnabled(False)
        self.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAlwaysOn)

        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        self.setDragDropMode(QtGui.QAbstractItemView.DragDropMode.InternalMove)
        self.setDropIndicatorShown(True)
        self.drag_row = None
        self.drag_item = None

    def startDrag(self, actions):
        self.drag_item = self.currentItem()
        self.drag_row = self.row(self.drag_item)
        super(FiFoWidget, self).startDrag(actions)

    def dropEvent(self, event):
        super(FiFoWidget, self).dropEvent(event)
        self.model.move_fifo_item(self.drag_row, self.row(self.drag_item))


class DateinamenSettingsWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()

        self.application = application
        self.model = application.main_window.dateinamen_model

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        widget_0 = QtGui.QWidget()
        layout.addWidget(widget_0)
        layout_0 = QtGui.QHBoxLayout()
        layout_0.setContentsMargins(0, 0, 0, 0)
        widget_0.setLayout(layout_0)

        layout_0.addWidget(QtGui.QLabel(u'<b>Allgemein</b>'))
        layout_0.addStretch()

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        layout_1.addWidget(QtGui.QLabel(u'Anzahl Zeichen Zähler:'))

        self.number_length_spin_box = QtGui.QSpinBox()
        self.number_length_spin_box.setMinimum(0)
        self.number_length_spin_box.setMaximum(Global.DATEINAMEN.MODEL.NUMBER_LENGTH_MAX)
        # noinspection PyUnresolvedReferences
        self.number_length_spin_box.valueChanged.connect(self.number_length_spin_box_value_changed)
        layout_1.addWidget(self.number_length_spin_box)

        layout_1.addWidget(QtGui.QLabel(u'Trennzeichen:'))

        self.delimiter_combo_box = QtGui.QComboBox()
        for item in Global.DATEINAMEN.MODEL.DELIMITERS_COMBO_BOX:
            self.delimiter_combo_box.addItem(item)
        # noinspection PyUnresolvedReferences
        self.delimiter_combo_box.currentIndexChanged.connect(self.delimiter_combo_box_current_index_changed)
        layout_1.addWidget(self.delimiter_combo_box)

        widget_2 = QtGui.QWidget()
        # layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        label = QtGui.QLabel(u'Unzulässige Zeichen:')
        layout_1.addWidget(label)

        self.invalid_chars_line_edit = QtGui.QLineEdit()
        # noinspection PyUnresolvedReferences
        self.invalid_chars_line_edit.textChanged.connect(self.invalid_chars_line_edit_text_changed)
        layout_1.addWidget(self.invalid_chars_line_edit)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_x = QtGui.QWidget()
        layout.addWidget(widget_x)
        layout_x = QtGui.QHBoxLayout()
        layout_x.setContentsMargins(0, 0, 0, 0)
        widget_x.setLayout(layout_x)

        layout_x.addWidget(QtGui.QLabel(u'<b>Suchmodi</b> für Autovervollständigungen'))
        layout_x.addStretch()

        suggestion_widget = QtGui.QWidget()
        suggestion_layout = QtGui.QHBoxLayout()
        suggestion_layout.setContentsMargins(0, 0, 0, 0)
        suggestion_widget.setLayout(suggestion_layout)
        layout.addWidget(suggestion_widget)

        sonst_widget = QtGui.QWidget()
        sonst_layout = QtGui.QVBoxLayout()
        sonst_layout.setContentsMargins(0, 0, 0, 0)
        sonst_widget.setLayout(sonst_layout)
        suggestion_layout.addWidget(sonst_widget)

        sonst_widget_1 = QtGui.QWidget()
        sonst_layout.addWidget(sonst_widget_1)
        sonst_layout_1 = QtGui.QHBoxLayout()
        sonst_layout_1.setContentsMargins(0, 0, 0, 0)
        sonst_widget_1.setLayout(sonst_layout_1)

        label = QtGui.QLabel(u'Suchmodus Sonst:')
        sonst_layout_1.addWidget(label)

        sonst_layout_1.addWidget(QtGui.QLabel(), 1)

        self.sonst_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.sonst_radio_button_group.buttonReleased[int].connect(self.sonst_radio_button_released)
        self.sonst_radio_buttons = list()

        sonst_widget_2 = QtGui.QWidget()
        sonst_layout.addWidget(sonst_widget_2)
        sonst_layout_2 = QtGui.QHBoxLayout()
        sonst_layout_2.setContentsMargins(0, 0, 0, 0)
        sonst_widget_2.setLayout(sonst_layout_2)

        sonst_rb_begin = QtGui.QRadioButton(u'Am Anfang des Textes')
        self.sonst_radio_button_group.addButton(sonst_rb_begin, 0)
        self.sonst_radio_buttons.append(sonst_rb_begin)
        sonst_layout_2.addWidget(sonst_rb_begin)

        sonst_layout_2.addWidget(QtGui.QLabel(), 1)

        sonst_widget_3 = QtGui.QWidget()
        sonst_layout.addWidget(sonst_widget_3)
        sonst_layout_3 = QtGui.QHBoxLayout()
        sonst_layout_3.setContentsMargins(0, 0, 0, 0)
        sonst_widget_3.setLayout(sonst_layout_3)

        sonst_rb_in = QtGui.QRadioButton(u'Im Text')
        self.sonst_radio_button_group.addButton(sonst_rb_in, 1)
        self.sonst_radio_buttons.append(sonst_rb_in)
        sonst_layout_3.addWidget(sonst_rb_in)

        sonst_layout_3.addWidget(QtGui.QLabel(), 1)

        sonst_widget_4 = QtGui.QWidget()
        sonst_layout.addWidget(sonst_widget_4)
        sonst_layout_4 = QtGui.QHBoxLayout()
        sonst_layout_4.setContentsMargins(0, 0, 0, 0)
        sonst_widget_4.setLayout(sonst_layout_4)

        sonst_rb_not = QtGui.QRadioButton(u'Nicht suchen')
        self.sonst_radio_button_group.addButton(sonst_rb_not, 2)
        self.sonst_radio_buttons.append(sonst_rb_not)
        sonst_layout_4.addWidget(sonst_rb_not)

        sonst_layout_4.addWidget(QtGui.QLabel(), 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setContentsMargins(0, 0, 0, 0)
        suggestion_layout.addWidget(line)

        name_widget = QtGui.QWidget()
        name_layout = QtGui.QVBoxLayout()
        name_layout.setContentsMargins(0, 0, 0, 0)
        name_widget.setLayout(name_layout)
        suggestion_layout.addWidget(name_widget)

        name_widget_1 = QtGui.QWidget()
        name_layout.addWidget(name_widget_1)
        name_layout_1 = QtGui.QHBoxLayout()
        name_layout_1.setContentsMargins(0, 0, 0, 0)
        name_widget_1.setLayout(name_layout_1)

        label = QtGui.QLabel(u'Suchmodus Vortragender:')
        name_layout_1.addWidget(label)

        name_layout_1.addWidget(QtGui.QLabel(), 1)

        self.name_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.name_radio_button_group.buttonReleased[int].connect(self.name_radio_button_released)
        self.name_radio_buttons = list()

        name_widget_2 = QtGui.QWidget()
        name_layout.addWidget(name_widget_2)
        name_layout_2 = QtGui.QHBoxLayout()
        name_layout_2.setContentsMargins(0, 0, 0, 0)
        name_widget_2.setLayout(name_layout_2)

        name_rb_begin = QtGui.QRadioButton(u'Am Anfang des Textes')
        self.name_radio_button_group.addButton(name_rb_begin, 0)
        self.name_radio_buttons.append(name_rb_begin)
        name_layout_2.addWidget(name_rb_begin)

        name_layout_2.addWidget(QtGui.QLabel(), 1)

        name_widget_3 = QtGui.QWidget()
        name_layout.addWidget(name_widget_3)
        name_layout_3 = QtGui.QHBoxLayout()
        name_layout_3.setContentsMargins(0, 0, 0, 0)
        name_widget_3.setLayout(name_layout_3)

        name_rb_in = QtGui.QRadioButton(u'Im Text')
        self.name_radio_button_group.addButton(name_rb_in, 1)
        self.name_radio_buttons.append(name_rb_in)
        name_layout_3.addWidget(name_rb_in)

        name_layout_3.addWidget(QtGui.QLabel(), 1)

        name_widget_4 = QtGui.QWidget()
        name_layout.addWidget(name_widget_4)
        name_layout_4 = QtGui.QHBoxLayout()
        name_layout_4.setContentsMargins(0, 0, 0, 0)
        name_widget_4.setLayout(name_layout_4)

        name_rb_not = QtGui.QRadioButton(u'Nicht suchen')
        self.name_radio_button_group.addButton(name_rb_not, 2)
        self.name_radio_buttons.append(name_rb_not)
        name_layout_4.addWidget(name_rb_not)

        name_layout_4.addWidget(QtGui.QLabel(), 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setContentsMargins(0, 0, 0, 0)
        suggestion_layout.addWidget(line)

        stelle_widget = QtGui.QWidget()
        stelle_layout = QtGui.QVBoxLayout()
        stelle_layout.setContentsMargins(0, 0, 0, 0)
        stelle_widget.setLayout(stelle_layout)
        suggestion_layout.addWidget(stelle_widget)

        stelle_widget_1 = QtGui.QWidget()
        stelle_layout.addWidget(stelle_widget_1)
        stelle_layout_1 = QtGui.QHBoxLayout()
        stelle_layout_1.setContentsMargins(0, 0, 0, 0)
        stelle_widget_1.setLayout(stelle_layout_1)

        label = QtGui.QLabel(u'Suchmodus Bibelbuch:')
        stelle_layout_1.addWidget(label)

        stelle_layout_1.addWidget(QtGui.QLabel(), 1)

        self.stelle_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.stelle_radio_button_group.buttonReleased[int].connect(self.stelle_radio_button_released)
        self.stelle_radio_buttons = list()

        stelle_widget_2 = QtGui.QWidget()
        stelle_layout.addWidget(stelle_widget_2)
        stelle_layout_2 = QtGui.QHBoxLayout()
        stelle_layout_2.setContentsMargins(0, 0, 0, 0)
        stelle_widget_2.setLayout(stelle_layout_2)

        stelle_rb_begin = QtGui.QRadioButton(u'Am Anfang des Textes')
        self.stelle_radio_button_group.addButton(stelle_rb_begin, 0)
        self.stelle_radio_buttons.append(stelle_rb_begin)
        stelle_layout_2.addWidget(stelle_rb_begin)

        stelle_layout_2.addWidget(QtGui.QLabel(), 1)

        stelle_widget_3 = QtGui.QWidget()
        stelle_layout.addWidget(stelle_widget_3)
        stelle_layout_3 = QtGui.QHBoxLayout()
        stelle_layout_3.setContentsMargins(0, 0, 0, 0)
        stelle_widget_3.setLayout(stelle_layout_3)

        stelle_rb_in = QtGui.QRadioButton(u'Im Text')
        self.stelle_radio_button_group.addButton(stelle_rb_in, 1)
        self.stelle_radio_buttons.append(stelle_rb_in)
        stelle_layout_3.addWidget(stelle_rb_in)

        stelle_layout_3.addWidget(QtGui.QLabel(), 1)

        stelle_widget_4 = QtGui.QWidget()
        stelle_layout.addWidget(stelle_widget_4)
        stelle_layout_4 = QtGui.QHBoxLayout()
        stelle_layout_4.setContentsMargins(0, 0, 0, 0)
        stelle_widget_4.setLayout(stelle_layout_4)

        stelle_rb_not = QtGui.QRadioButton(u'Nicht suchen')
        self.stelle_radio_button_group.addButton(stelle_rb_not, 2)
        self.stelle_radio_buttons.append(stelle_rb_not)
        stelle_layout_4.addWidget(stelle_rb_not)

        stelle_layout_4.addWidget(QtGui.QLabel(), 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.VLine)
        line.setContentsMargins(0, 0, 0, 0)
        suggestion_layout.addWidget(line)

        titel_widget = QtGui.QWidget()
        titel_layout = QtGui.QVBoxLayout()
        titel_layout.setContentsMargins(0, 0, 0, 0)
        titel_widget.setLayout(titel_layout)
        suggestion_layout.addWidget(titel_widget)

        titel_widget_1 = QtGui.QWidget()
        titel_layout.addWidget(titel_widget_1)
        titel_layout_1 = QtGui.QHBoxLayout()
        titel_layout_1.setContentsMargins(0, 0, 0, 0)
        titel_widget_1.setLayout(titel_layout_1)

        label = QtGui.QLabel(u'Suchmodus Titel:')
        titel_layout_1.addWidget(label)

        titel_layout_1.addWidget(QtGui.QLabel(), 1)

        self.titel_radio_button_group = QtGui.QButtonGroup()
        # noinspection PyUnresolvedReferences
        self.titel_radio_button_group.buttonReleased[int].connect(self.titel_radio_button_released)
        self.titel_radio_buttons = list()

        titel_widget_2 = QtGui.QWidget()
        titel_layout.addWidget(titel_widget_2)
        titel_layout_2 = QtGui.QHBoxLayout()
        titel_layout_2.setContentsMargins(0, 0, 0, 0)
        titel_widget_2.setLayout(titel_layout_2)

        titel_rb_begin = QtGui.QRadioButton(u'Am Anfang des Textes')
        self.titel_radio_button_group.addButton(titel_rb_begin, 0)
        self.titel_radio_buttons.append(titel_rb_begin)
        titel_layout_2.addWidget(titel_rb_begin)

        titel_layout_2.addWidget(QtGui.QLabel(), 1)

        titel_widget_3 = QtGui.QWidget()
        titel_layout.addWidget(titel_widget_3)
        titel_layout_3 = QtGui.QHBoxLayout()
        titel_layout_3.setContentsMargins(0, 0, 0, 0)
        titel_widget_3.setLayout(titel_layout_3)

        titel_rb_in = QtGui.QRadioButton(u'Im Text')
        self.titel_radio_button_group.addButton(titel_rb_in, 1)
        self.titel_radio_buttons.append(titel_rb_in)
        titel_layout_3.addWidget(titel_rb_in)

        titel_layout_3.addWidget(QtGui.QLabel(), 1)

        titel_widget_4 = QtGui.QWidget()
        titel_layout.addWidget(titel_widget_4)
        titel_layout_4 = QtGui.QHBoxLayout()
        titel_layout_4.setContentsMargins(0, 0, 0, 0)
        titel_widget_4.setLayout(titel_layout_4)

        titel_rb_not = QtGui.QRadioButton(u'Nicht suchen')
        self.titel_radio_button_group.addButton(titel_rb_not, 2)
        self.titel_radio_buttons.append(titel_rb_not)
        titel_layout_4.addWidget(titel_rb_not)

        titel_layout_4.addWidget(QtGui.QLabel(), 1)

        suggestion_widget_space = QtGui.QWidget()
        suggestion_layout.addWidget(suggestion_widget_space, 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_5 = QtGui.QWidget()
        layout.addWidget(widget_5)
        layout_5 = QtGui.QHBoxLayout()
        layout_5.setContentsMargins(0, 0, 0, 0)
        widget_5.setLayout(layout_5)

        layout_5.addWidget(QtGui.QLabel(u'<b>Allgemeines Liederbuch</b>'))

        import_general_songs_button = QtGui.QPushButton(u'Importiere Allgemeines Liederbuch')
        import_general_songs_button.setToolTip(u'Allgemeines Liederbuch importieren')
        layout_5.addWidget(import_general_songs_button)
        # noinspection PyUnresolvedReferences
        import_general_songs_button.pressed.connect(self.import_general_songs_book)

        reset_general_songs_button = QtGui.QPushButton(u'Setze Allg. Liederbuch zurück')
        reset_general_songs_button.setToolTip(u'Allgemeines Liederbuch zurücksetzen')
        layout_5.addWidget(reset_general_songs_button)
        # noinspection PyUnresolvedReferences
        reset_general_songs_button.pressed.connect(self.reset_general_songs_book)

        widget_space = QtGui.QWidget()
        layout_5.addWidget(widget_space, 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_6 = QtGui.QWidget()
        layout.addWidget(widget_6)
        layout_6 = QtGui.QHBoxLayout()
        layout_6.setContentsMargins(0, 0, 0, 0)
        widget_6.setLayout(layout_6)

        layout_6.addWidget(QtGui.QLabel(u'<b>Eigene Typdefinitionen</b>'))

        button_add_type_definition = QtGui.QPushButton(u'Neu')
        # noinspection PyUnresolvedReferences
        button_add_type_definition.clicked.connect(self.new_type_definition)
        layout_6.addWidget(button_add_type_definition)

        layout_6.addStretch()

        self.button_save_type_definitions = QtGui.QPushButton(u'Typdefinitionen speichern')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-save-3-240.png'))
        self.button_save_type_definitions.setIcon(icon)
        self.button_save_type_definitions.setToolTip(u'Typdefinitionen speichern')
        # noinspection PyUnresolvedReferences
        self.button_save_type_definitions.clicked.connect(self.save_type_definitions_to_file)
        layout_6.addWidget(self.button_save_type_definitions)

        self.button_load_type_definitions = QtGui.QPushButton(u'Typdefinitionen öffnen')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-disk-15-240.png'))
        self.button_load_type_definitions.setIcon(icon)
        self.button_load_type_definitions.setToolTip(u'Typdefinitionen öffnen')
        # noinspection PyUnresolvedReferences
        self.button_load_type_definitions.clicked.connect(self.load_type_definitions_from_file)
        layout_6.addWidget(self.button_load_type_definitions)

        self.table_widget_type_definitions = QtGui.QListWidget()
        self.table_widget_type_definitions.setSortingEnabled(False)
        layout.addWidget(self.table_widget_type_definitions, 1)

        self.load_values_from_model()

        self.model.type_definitions_changed.connect(self.load_type_definitions)
        self.model.model_changed.connect(self.load_values_from_model)

    def import_general_songs_book(self):
        default_path = os.path.expanduser(u'~')
        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window,
                                                      u'Allgemeines Liederbuch importieren',
                                                      default_path,
                                                      u'Textdateien (*.txt)')
        file_name = file_name[0]
        if not file_name:
            return
        if not os.path.isfile(file_name):
            return
        self.model.general_songs.import_file(file_name)

    def reset_general_songs_book(self):
        question = u'Soll das Allgemeine Liederbuch wirklich auf Werkseinstellungen zurückgesetzt werden? Mögliche ' \
                   u'Änderungen gehen dabei verloren.'
        message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                        u'Allg. Liederbuch zurücksetzen',
                                        question,
                                        QtGui.QMessageBox.NoButton,
                                        self.application.main_window)
        message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
        message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)

        response = message_box.exec_()
        if response == 0:  # yes
            self.model.general_songs.reset()

    def number_length_spin_box_value_changed(self, value):
        self.model.number_length = value

    def delimiter_combo_box_current_index_changed(self, index):
        self.model.delimiter = Global.DATEINAMEN.MODEL.DELIMITERS[index]

    def invalid_chars_line_edit_text_changed(self, text):
        self.model.invalid_chars = text

    def name_radio_button_released(self, index):
        self.model.suggestion_policy_name_completer = index

    def sonst_radio_button_released(self, index):
        self.model.suggestion_policy_sonst_completer = index

    def titel_radio_button_released(self, index):
        self.model.suggestion_policy_titel_completer = index

    def stelle_radio_button_released(self, index):
        self.model.suggestion_policy_stelle_completer = index

    def load_values_from_model(self):
        if self.number_length_spin_box.value() != self.model.number_length:
            self.number_length_spin_box.setValue(self.model.number_length)
        if self.delimiter_combo_box.currentIndex() != Global.DATEINAMEN.MODEL.DELIMITERS.index(self.model.delimiter):
            self.delimiter_combo_box.setCurrentIndex(Global.DATEINAMEN.MODEL.DELIMITERS.index(self.model.delimiter))
        if self.invalid_chars_line_edit.text() != self.model.invalid_chars:
            self.invalid_chars_line_edit.setText(self.model.invalid_chars)
        self.sonst_radio_buttons[self.model.suggestion_policy_sonst_completer].setChecked(True)
        self.name_radio_buttons[self.model.suggestion_policy_name_completer].setChecked(True)
        self.stelle_radio_buttons[self.model.suggestion_policy_stelle_completer].setChecked(True)
        self.titel_radio_buttons[self.model.suggestion_policy_titel_completer].setChecked(True)
        self.load_type_definitions()

    def load_type_definitions(self):
        self.table_widget_type_definitions.clear()
        counter = 0
        for type_definition in self.model.type_definitions:
            item = QtGui.QListWidgetItem()
            widget = QtGui.QWidget()
            layout = QtGui.QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            widget.setLayout(layout)

            button_edit = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-pencil-8-240.png'))
            button_edit.setIcon(icon)
            button_edit.setToolTip(u'Typdefinition bearbeiten')
            # noinspection PyUnresolvedReferences
            button_edit.clicked.connect(self.handle_button_edit_clicked)
            button_edit.setFixedWidth(24)
            layout.addWidget(button_edit)

            label_dateiname = QtGui.QLabel(unicode(type_definition))
            label_dateiname.setTextFormat(QtCore.Qt.PlainText)
            layout.addWidget(label_dateiname)
            layout.addStretch()

            button_delete = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-4-240.png'))
            button_delete.setIcon(icon)
            button_delete.setToolTip(u'Typdefinition löschen')
            # noinspection PyUnresolvedReferences
            button_delete.clicked.connect(self.handle_button_delete_clicked)
            button_delete.setFixedWidth(24)
            layout.addWidget(button_delete)

            widget_size = widget.sizeHint()
            item_size = QtCore.QSize(widget_size.width() + 16, widget_size.height())
            item.setSizeHint(item_size)

            self.table_widget_type_definitions.insertItem(counter, item)
            self.table_widget_type_definitions.setItemWidget(item, widget)

            counter += 1

    def handle_button_edit_clicked(self):
        button = self.sender()
        index = self.table_widget_type_definitions.indexAt(button.parentWidget().pos())
        if index.isValid():
            type_definition = self.model.type_definitions[index.row()]
            dialog = MtWidgets.TypeDefinitionDialog(self.application.main_window, type_definition)
            dialog.exec_()
            if dialog.ok:
                type_definition = dialog.get_type_definition()
                self.model.update_type_definition(type_definition, index.row())

    def handle_button_delete_clicked(self):
        button = self.sender()
        index = self.table_widget_type_definitions.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.remove_type_definition_at_index(index.row())

    def new_type_definition(self):
        type_definition = TypeDefinition()
        dialog = MtWidgets.TypeDefinitionDialog(self.application.main_window, type_definition)
        dialog.exec_()
        if dialog.ok:
            self.model.add_to_type_definitions(dialog.get_type_definition())

    def load_type_definitions_from_file(self):
        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.expanduser(u'~')

        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window, u'Typdefinitionen öffnen',
                                                      default_path,
                                                      u'JSON (*.json)')
        file_name = file_name[0]
        if not file_name:
            return
        if not os.path.isfile(file_name):
            self.application.logger.log(u'Datei konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            self.__save_path(file_name)
            try:
                read_file = open(file_name, 'r')
            except IOError:
                self.application.logger.log(u'Datei konnte nicht geöffnet werden', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            try:
                json_data = json.load(read_file)
            except Exception as e:
                self.application.logger.log(u'Datei konnte nicht ausgelesen werden'.format(str(e)), __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            else:
                read_file.close()
            try:
                type_definitions_dicts = list(json_data)
            except Exception:
                self.application.logger.log(u'Unerwartetes Dateiformat', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            errors = 0
            for type_definition in type_definitions_dicts:
                try:
                    self.model.add_to_type_definitions(TypeDefinition(type_definition))
                except Exception as e:
                    errors += 1
                    self.application.logger.log(u'Failed to deserialize type definition: {0}'.format(str(e)), __name__,
                                                MediaToolsLogger.SEVERITY_ERROR, False, False)
                if errors:
                    self.application.logger.log(u'{0} von {1} Typdefinitionen konnten nicht geladen '
                                                u'werden'.format(errors, len(type_definitions_dicts)), __name__,
                                                MediaToolsLogger.SEVERITY_WARNING, True, True)

    def save_type_definitions_to_file(self):
        if not self.model.type_definitions:
            self.application.logger.log(u'Keine Typdefinitionen vorhanden.', __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, True)
            return

        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'TypDefinitionen.json')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Typdefinitionen speichern',
                                                      default_path,
                                                      u'JSON (*.json)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)

            try:
                write_file = open(file_name, 'w')
            except IOError as e:
                self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
            else:
                type_definitions = [i.get_dict() for i in self.model.type_definitions]
                try:
                    json.dump(type_definitions, write_file)
                except Exception as e:
                    self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                                __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
                write_file.close()

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.DATEINAMEN.FILES.TYPE_DEFINITION_DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.DATEINAMEN.FILES.TYPE_DEFINITION_DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


class CreateDirsWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()

        self.application = application
        self.model = application.main_window.dateinamen_model

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        layout_1.addWidget(QtGui.QLabel(u'<b>Jahr</b>'))

        self.year_spin_box = QtGui.QSpinBox()
        self.year_spin_box.setMinimum(1970)
        self.year_spin_box.setMaximum(2100)
        self.year_spin_box.setValue(datetime.datetime.now().year)
        layout_1.addWidget(self.year_spin_box)

        layout_1.addStretch()

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        layout_2.addWidget(QtGui.QLabel(u'<b>Wochentage</b>'))

        self.montag_check_box = QtGui.QCheckBox(u'Montag')
        layout_2.addWidget(self.montag_check_box)
        self.dienstag_check_box = QtGui.QCheckBox(u'Dienstag')
        layout_2.addWidget(self.dienstag_check_box)
        self.mittwoch_check_box = QtGui.QCheckBox(u'Mittwoch')
        layout_2.addWidget(self.mittwoch_check_box)
        self.donnerstag_check_box = QtGui.QCheckBox(u'Donnerstag')
        layout_2.addWidget(self.donnerstag_check_box)
        self.freitag_check_box = QtGui.QCheckBox(u'Freitag')
        layout_2.addWidget(self.freitag_check_box)
        self.samstag_check_box = QtGui.QCheckBox(u'Samstag')
        layout_2.addWidget(self.samstag_check_box)
        self.sonntag_check_box = QtGui.QCheckBox(u'Sonntag')
        layout_2.addWidget(self.sonntag_check_box)
        layout_2.addWidget(QtGui.QLabel(), 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_4 = QtGui.QWidget()
        layout.addWidget(widget_4)
        layout_4 = QtGui.QHBoxLayout()
        layout_4.setContentsMargins(0, 0, 0, 0)
        widget_4.setLayout(layout_4)

        layout_4.addWidget(QtGui.QLabel(u'<b>Unterordner</b>'))

        self.unterordner_check_box = QtGui.QCheckBox(u'Mit Unterordnern')
        self.unterordner_check_box.setToolTip(u'Nur wenn angewählt werden Unterordner generiert')
        layout_4.addWidget(self.unterordner_check_box)
        button_add_sub_dir = QtGui.QPushButton(u'Unterordner hinzufügen')
        button_add_sub_dir.setToolTip(u'Neuen Unterordner hinzufügen')
        # noinspection PyUnresolvedReferences
        button_add_sub_dir.clicked.connect(self.__add_sub_dir)
        layout_4.addWidget(button_add_sub_dir)
        layout_4.addStretch()

        self.table_sub_dirs = QtGui.QListWidget()
        self.table_sub_dirs.setSortingEnabled(False)
        layout.addWidget(self.table_sub_dirs, 1)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_6 = QtGui.QWidget()
        layout.addWidget(widget_6)
        layout_6 = QtGui.QHBoxLayout()
        layout_6.setContentsMargins(0, 0, 0, 0)
        widget_6.setLayout(layout_6)

        create_dirs_button = QtGui.QPushButton(u'Ordner erstellen')
        create_dirs_button.setToolTip(u'Ordner erstellen')
        # noinspection PyUnresolvedReferences
        create_dirs_button.pressed.connect(self.create_dirs)
        layout_6.addWidget(create_dirs_button)
        layout_6.addStretch()
        self.load_sub_dirs()
        self.model.sub_dirs_changed.connect(self.load_sub_dirs)

    def load_sub_dirs(self):
        self.table_sub_dirs.clear()
        counter = 0
        for sub_dir in self.model.sub_dirs:
            item = QtGui.QListWidgetItem()
            widget = QtGui.QWidget()
            layout = QtGui.QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            widget.setLayout(layout)

            label_dateiname = QtGui.QLabel(unicode(sub_dir))
            label_dateiname.setTextFormat(QtCore.Qt.PlainText)
            layout.addWidget(label_dateiname)
            layout.addStretch()

            button_delete = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-4-240.png'))
            button_delete.setIcon(icon)
            button_delete.setToolTip(u'Unterordner entfernen')
            # noinspection PyUnresolvedReferences
            button_delete.clicked.connect(self.handle_button_delete_clicked)
            button_delete.setFixedWidth(24)
            layout.addWidget(button_delete)

            widget_size = widget.sizeHint()
            item_size = QtCore.QSize(widget_size.width() + 16, widget_size.height())
            item.setSizeHint(item_size)

            self.table_sub_dirs.insertItem(counter, item)
            self.table_sub_dirs.setItemWidget(item, widget)

            counter += 1

    def handle_button_delete_clicked(self):
        button = self.sender()
        index = self.table_sub_dirs.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.remove_sub_dir_at_index(index.row())

    def __add_sub_dir(self):
        dialog = QtGui.QInputDialog(self.application.main_window)
        dialog.setOkButtonText(u'OK')
        dialog.setCancelButtonText(u'Abbrechen')
        answer = dialog.getText(self.application.main_window, u'Neuer Unterordner',
                                u'Gib den Namens des Unterordners ein:',
                                QtGui.QLineEdit.Normal,
                                u'',
                                QtCore.Qt.WindowTitleHint)
        if answer[1]:
            self.model.add_to_sub_dirs(answer[0])

    def create_dirs(self):
        weekdays = list()
        if self.montag_check_box.isChecked():
            weekdays.append(0)
        if self.dienstag_check_box.isChecked():
            weekdays.append(1)
        if self.mittwoch_check_box.isChecked():
            weekdays.append(2)
        if self.donnerstag_check_box.isChecked():
            weekdays.append(3)
        if self.freitag_check_box.isChecked():
            weekdays.append(4)
        if self.samstag_check_box.isChecked():
            weekdays.append(5)
        if self.sonntag_check_box.isChecked():
            weekdays.append(6)

        default_dir = self.__load_path()
        if not os.path.isdir(default_dir):
            default_dir = os.path.expanduser(u'~')

        dialog = QtGui.QFileDialog()
        answer = dialog.getExistingDirectory(dir=default_dir)

        if os.path.isdir(answer):
            self.__save_path(answer)
            if self.unterordner_check_box.isChecked():
                sub_dirs = self.model.sub_dirs
            else:
                sub_dirs = []
            try:
                self.make_folder(self.year_spin_box.value(), answer, weekdays, sub_dirs)
            except ValueError as ve:
                self.application.logger.log(u"Fehler beim Erstellen von Ordnern: {0}".format(unicode(ve)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
            else:
                self.application.logger.log(u'Ordner erfolgreich erstellt', __name__,
                                            MediaToolsLogger.SEVERITY_INFO, True, True)

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.DATEINAMEN.CREATE_DIRS.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.DATEINAMEN.CREATE_DIRS.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path

    @staticmethod
    def make_folder(year, folder, weekdays, sub_dirs):
        try:
            year_int = int(year)
        except ValueError:
            raise ValueError(u'Das Jahr muss eine Zahl sein')
        if year_int < 1970:
            raise ValueError(u'Das Jahr muss größer oder gleich 1970 sein')
        if year_int > 2100:
            raise ValueError(u'Wenn es wirklich das angegebene Jahr ist frag nach einer neuen Version')

        month_names = (u'', u"01 Januar", u"02 Februar", u"03 März", u"04 April", u"05 Mai", u"06 Juni", u"07 Juli",
                       u"08 August", u"09 September", u"10 Oktober", u"11 November", u"12 Dezember")

        CreateDirsWidget.make_dirs_if_not_exists(os.path.join(folder, u'%d' % year_int))
        for month in range(1, 13):
            CreateDirsWidget.make_dirs_if_not_exists(os.path.join(folder, u'%d\\%s' % (year_int, month_names[month])))
            for day in range(1, 32):
                try:
                    calendar.weekday(year_int, month, day)
                except ValueError:
                    break
                else:
                    if calendar.weekday(year_int, month, day) in weekdays:
                        if calendar.weekday(year_int, month, day) == 6:
                            dir_path = os.path.join(folder, u'%d\\%s\\%d_%02d_%02d_m_' % (year_int, month_names[month],
                                                                                          year_int, month, day))
                            CreateDirsWidget.make_dirs_if_not_exists(dir_path)
                            for sub_dir in sub_dirs:
                                CreateDirsWidget.make_dirs_if_not_exists(os.path.join(dir_path, sub_dir))
                            dir_path = os.path.join(folder, u'%d\\%s\\%d_%02d_%02d_n_' % (year_int, month_names[month],
                                                                                          year_int, month, day))
                            CreateDirsWidget.make_dirs_if_not_exists(dir_path)
                            for sub_dir in sub_dirs:
                                CreateDirsWidget.make_dirs_if_not_exists(os.path.join(dir_path, sub_dir))

                        else:
                            dir_path = os.path.join(folder, u'%d\\%s\\%d_%02d_%02d' % (year_int, month_names[month],
                                                                                       year_int, month, day))
                            CreateDirsWidget.make_dirs_if_not_exists(dir_path)
                            for sub_dir in sub_dirs:
                                CreateDirsWidget.make_dirs_if_not_exists(os.path.join(dir_path, sub_dir))

    @staticmethod
    def make_dirs_if_not_exists(path):
        if not os.path.isdir(path):
            os.makedirs(path)


class PlaylistWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()

        self.application = application
        self.model = application.main_window.dateinamen_model

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        self.exported_widget = MtWidgets.ExportedNamesWidget(application, True)
        layout.addWidget(self.exported_widget, 1)

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        layout_1.addWidget(QtGui.QLabel(u'Dateityp der Aufnahmen:'))
        self.type_line_edit = QtGui.QLineEdit()
        self.type_completer = MtWidgets.FileCompleter(self.application,
                                                      Global.DATEINAMEN.PLAYLIST.TYPE_COMPLETER_PATH)
        self.type_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.type_line_edit.setCompleter(self.type_completer)
        linecache.checkcache(Global.DATEINAMEN.PLAYLIST.TYPE_COMPLETER_PATH)
        first_line = linecache.getline(Global.DATEINAMEN.PLAYLIST.TYPE_COMPLETER_PATH, 1).decode('utf-8').strip('\n\r')
        self.type_line_edit.setText(first_line)
        layout_1.addWidget(self.type_line_edit)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        self.button_save_all = QtGui.QPushButton(u'Alles als Wiedergabeliste speichern')
        self.button_save_all.setToolTip(u'Alles als Wiedergabeliste speichern')
        # noinspection PyUnresolvedReferences
        self.button_save_all.clicked.connect(self.save_all)
        layout_2.addWidget(self.button_save_all)
        self.button_save_selected = QtGui.QPushButton(u'Auswahl als Wiedergabeliste speichern')
        self.button_save_selected.setToolTip(u'Auswahl als Wiedergabeliste speichern')
        # noinspection PyUnresolvedReferences
        self.button_save_selected.clicked.connect(self.save_selected)
        layout_2.addWidget(self.button_save_selected)
        layout_2.addStretch()

    def save_all(self):
        self.save_file(range(len(self.model.copied_names)))

    def save_selected(self):
        self.save_file(self.exported_widget.get_selected_item_indexes())

    @staticmethod
    def get_type_from_filter(filter_):
        filter_0 = filter_.split(';;')[0].strip()
        type_ = filter_0.split(' ')[0]
        extension = Global.DATEINAMEN.PLAYLIST.TYPES.get(type_, [''])[0]
        return extension

    @staticmethod
    def get_types_file_filter(filter_dict):
        return ';;'.join(type_ + ' (' + ' '.join(['*.' + i for i in filter_dict[type_]]) + ')'
                         for type_ in filter_dict.keys())

    def __get_playlist_string(self, indexes, type_):
        media_type = self.type_line_edit.text()
        text = ''
        items = [self.model.copied_names[index].copy_name for index in indexes]
        if type_ in ('m3u', 'txt'):
            if media_type:
                text = '\n'.join([item + '.' + media_type for item in items]).encode('mbcs')
            else:
                text = '\n'.join(items).encode('mbcs')
        elif type_ == 'm3u8':
            if media_type:
                text = '\n'.join(item + '.' + media_type for item in items).encode('utf-8')
            else:
                text = '\n'.join(items).encode('utf-8')
        return text

    def save_file(self, indexes):

        if not indexes:
            self.application.logger.log(u'Keine Dateinamen ausgewählt.', __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, True)
            return

        default_path = self.__load_path()
        if not os.path.isdir(os.path.dirname(default_path)):
            default_path = os.path.join(os.path.expanduser(u'~'), u'Wiedergabeliste.m3u8')

        default_extension = '' if default_path.count('.') < 1 else default_path.split('.')[-1].lower()
        default_description = 'M3U8'
        for description, types in Global.DATEINAMEN.PLAYLIST.TYPES.iteritems():
            if default_extension in types:
                default_description = description
                break

        file_path, type_ = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Wiedergabeliste speichern',
                                                             default_path,
                                                             self.get_types_file_filter(
                                                                 Global.DATEINAMEN.PLAYLIST.TYPES
                                                             ),
                                                             self.get_types_file_filter(
                                                                 dict(filter(lambda items:
                                                                             items[0] == default_description,
                                                                             Global.DATEINAMEN.PLAYLIST.TYPES.iteritems()))
                                                             ),
                                                             )

        if not file_path:
            return

        file_name = os.path.basename(file_path)

        extension = '' if file_name.count('.') < 1 else file_name.split('.')[-1].lower()

        if extension == '':
            extension = self.get_type_from_filter(type_)
            file_path += '.' + extension

        if not any([extension in i for i in Global.DATEINAMEN.PLAYLIST.TYPES.values()]):
            self.application.logger.log(u'Unbekannter Dateityp für Wiedergabeliste.', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        elif not os.path.isdir(os.path.dirname(file_path)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            media_type = self.type_line_edit.text()
            self.type_completer.add_item(media_type)

            self.__save_path(file_path)

            try:
                w_file = open(file_path, 'wb')
            except IOError as error:
                self.application.logger.log(u'Ein Fehler ist beim Schreiben der Wiedergabeliste aufgetreten: %s' %
                                            unicode(error), __name__, MediaToolsLogger.SEVERITY_ERROR,
                                            True, True)
            else:
                w_file.write(self.__get_playlist_string(indexes, extension))
                self.application.logger.log(u'Wiedergabeliste erfolgreich gespeichert.', __name__,
                                            MediaToolsLogger.SEVERITY_DEBUG, True, True)

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.DATEINAMEN.PLAYLIST.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.DATEINAMEN.PLAYLIST.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


class DateinamenModel(QtGui.QWidget):
    fifo_changed = QtCore.Signal()
    copied_changed = QtCore.Signal()
    model_changed = QtCore.Signal()
    type_definitions_changed = QtCore.Signal()
    sub_dirs_changed = QtCore.Signal()

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application
        self.clipboard = QtGui.QClipboard()

        # settings
        if os.path.exists(Global.DATEINAMEN.SETTINGS.PATH):
            self.__change_time = os.path.getmtime(Global.DATEINAMEN.SETTINGS.PATH)
        else:
            self.__change_time = 0
        self.__loaded = False
        self.__settings = Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS.copy()
        self.__load_settings()

        self.stelle_list_completer = MtWidgets.ListCompleter(self.application, Global.DATEINAMEN.MODEL.BIBLE_BOOKS)
        self.name_completer = MtWidgets.MultiCompleter(self.application, Global.DATEINAMEN.FILES.PATH_NAMES,
                                                       separators=Global.DATEINAMEN.MODEL.NAME_SEPARATORS)
        self.titel_file_completer = MtWidgets.FileCompleter(self.application, Global.DATEINAMEN.FILES.PATH_TITEL)
        self.sonst_file_completer = MtWidgets.FileCompleter(self.application, Global.DATEINAMEN.FILES.PATH_SONST)
        self.set_suggestion_policies()
        # noinspection PyUnresolvedReferences
        self.model_changed.connect(self.set_suggestion_policies)
        self.__fifo = []
        self.shown_dateinamen = []
        self.__undone_dateinamen = []
        self.__copied_names = []
        self.last_dateiname = Dateiname(self)
        self.last_dateiname.counter = 1
        self.general_songs = GeneralSongsHandler(self.application, self, Global.DATEINAMEN.FILES.PATH_GENERAL_SONGS)
        self.__type_definitions = []
        self.__load_type_definitions()
        self.__sub_dirs = []
        self.__load_sub_dirs()

    def __load_settings(self):
        """Einstellungen aus Datei laden."""
        write = False
        emit_changed = False
        checked_settings = dict()
        if not os.path.exists(Global.DATEINAMEN.SETTINGS.PATH):
            self.__write_settings()
        try:
            settings_file = codecs.open(Global.DATEINAMEN.SETTINGS.PATH, 'r', 'utf-8')
        except IOError:
            self.__loaded = False
            self.application.logger.log(u"Fehler beim Lesen der Einstellungsdatei für Dateinamen.",
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            unchecked_settings = Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS.copy()
            try:
                unchecked_settings = json.loads(settings_file.read())
            except ValueError:
                self.application.logger.log(u"Fehler beim Lesen der Einstellungsdatei für Dateinamen.",
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
                unchecked_settings = self.__settings.copy()
                write = True
            else:
                settings_file.close()
            finally:
                if not isinstance(unchecked_settings, dict):
                    unchecked_settings = dict()
                    write = True

                # number_length
                if 'number_length' in unchecked_settings:
                    checked_value = self.__check_number_length(unchecked_settings['number_length'])
                    if checked_value != self.__settings['number_length']:
                        emit_changed = True
                    if checked_value != unchecked_settings['number_length']:
                        write = True
                    checked_settings['number_length'] = checked_value
                else:
                    checked_settings['number_length'] = Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['number_length']
                    write = True

                # delimiter
                if 'delimiter' in unchecked_settings:
                    checked_value = self.__check_delimiter(unchecked_settings['delimiter'])
                    if checked_value != self.__settings['delimiter']:
                        emit_changed = True
                    if checked_value != unchecked_settings['delimiter']:
                        write = True
                    checked_settings['delimiter'] = checked_value
                else:
                    checked_settings['delimiter'] = Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['delimiter']
                    write = True

                # invalid_chars
                if 'invalid_chars' in unchecked_settings:
                    checked_value = self.__check_invalid_chars(unchecked_settings['invalid_chars'])
                    if checked_value != self.__settings['invalid_chars']:
                        emit_changed = True
                    if checked_value != unchecked_settings['invalid_chars']:
                        write = True
                    checked_settings['invalid_chars'] = checked_value
                else:
                    checked_settings['invalid_chars'] = Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['invalid_chars']
                    write = True

                # suggestion_policy_name_completer
                if 'suggestion_policy_name_completer' in unchecked_settings:
                    checked_value = self.__check_suggestion_policy_name_completer(
                        unchecked_settings['suggestion_policy_name_completer'])
                    if checked_value != self.__settings['suggestion_policy_name_completer']:
                        emit_changed = True
                    if checked_value != unchecked_settings['suggestion_policy_name_completer']:
                        write = True
                    checked_settings['suggestion_policy_name_completer'] = checked_value
                else:
                    checked_settings['suggestion_policy_name_completer'] = \
                        Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['suggestion_policy_name_completer']
                    write = True

                # suggestion_policy_titel_completer
                if 'suggestion_policy_titel_completer' in unchecked_settings:
                    checked_value = self.__check_suggestion_policy_titel_completer(
                        unchecked_settings['suggestion_policy_titel_completer'])
                    if checked_value != self.__settings['suggestion_policy_titel_completer']:
                        emit_changed = True
                    if checked_value != unchecked_settings['suggestion_policy_titel_completer']:
                        write = True
                    checked_settings['suggestion_policy_titel_completer'] = checked_value
                else:
                    checked_settings['suggestion_policy_titel_completer'] = \
                        Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['suggestion_policy_titel_completer']
                    write = True

                # suggestion_policy_sonst_completer
                if 'suggestion_policy_sonst_completer' in unchecked_settings:
                    checked_value = self.__check_suggestion_policy_sonst_completer(
                        unchecked_settings['suggestion_policy_sonst_completer'])
                    if checked_value != self.__settings['suggestion_policy_sonst_completer']:
                        emit_changed = True
                    if checked_value != unchecked_settings['suggestion_policy_sonst_completer']:
                        write = True
                    checked_settings['suggestion_policy_sonst_completer'] = checked_value
                else:
                    checked_settings['suggestion_policy_sonst_completer'] = \
                        Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['suggestion_policy_sonst_completer']
                    write = True

                # suggestion_policy_stelle_completer
                if 'suggestion_policy_stelle_completer' in unchecked_settings:
                    checked_value = self.__check_suggestion_policy_stelle_completer(
                        unchecked_settings['suggestion_policy_stelle_completer'])
                    if checked_value != self.__settings['suggestion_policy_stelle_completer']:
                        emit_changed = True
                    if checked_value != unchecked_settings['suggestion_policy_stelle_completer']:
                        write = True
                    checked_settings['suggestion_policy_stelle_completer'] = checked_value
                else:
                    checked_settings['suggestion_policy_stelle_completer'] = \
                        Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS['suggestion_policy_stelle_completer']
                    write = True

                self.__settings = checked_settings
                self.__loaded = True

        if write:
            self.__write_settings()
        if emit_changed:
            self.model_changed.emit()

    def __write_settings(self):
        """Einstellungen in Datei schreiben."""
        self.model_changed.emit()
        try:
            settings_file = codecs.open(Global.DATEINAMEN.SETTINGS.PATH, 'w', 'utf-8')
        except IOError:
            self.application.logger.log(u"Fehler beim Schreiben der Einstellungsdatei für Dateinamen.",
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            settings_file.write(json.dumps(self.__settings))
            settings_file.close()
            self.__change_time = os.path.getmtime(Global.DATEINAMEN.SETTINGS.PATH)

    def __check_reload_settings(self):
        """Prüft ob die Einstellungsdatei sich geändert hat und neu geladen werden muss."""
        if not os.path.exists(Global.DATEINAMEN.SETTINGS.PATH):
            return True
        return not self.__loaded or self.__change_time != os.path.getmtime(Global.DATEINAMEN.SETTINGS.PATH)

    def load_default_settings(self):
        """Setzt alles auf Defaulteinstellungen zurück."""
        self.__settings = Global.DATEINAMEN.MODEL.DEFAULT_SETTINGS.copy()
        self.__write_settings()

    @property
    def fifo(self):
        return tuple(self.__fifo)

    def add_to_fifo(self, dateiname):
        if self.item_under_edit:
            return False
        if isinstance(dateiname, Dateiname):
            self.__fifo.append(dateiname)
            self.fifo_changed.emit()
            return True
        return False

    def get_fifo_at_index(self, index):
        return self.__fifo[index]

    def set_fifo_at_index(self, index, dateiname):
        if isinstance(dateiname, Dateiname):
            self.__fifo[index] = dateiname
            self.fifo_changed.emit()

    def delete_fifo_at_index(self, index):
        try:
            del self.__fifo[index]
        except IndexError:
            pass
        self.fifo_changed.emit()

    def fifo_copy_to_clipboard(self):
        if self.__fifo and not self.item_under_edit:
            dateiname = self.__fifo.pop(0)
            dateiname.copy_name = unicode(dateiname)
            self.clipboard.setText(dateiname.copy_name)
            self.application.logger.log(u'"{0}" in Zwischenablage kopiert'.format(dateiname.copy_name), __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, False)
            self.add_to_copied_names(dateiname)
            self.fifo_changed.emit()

    def clear_fifo(self):
        self.__fifo = list()
        self.fifo_changed.emit()

    def undo(self):
        if self.__fifo and not self.item_under_edit:
            dateiname = self.__fifo.pop()
            self.__undone_dateinamen.append(dateiname)
            self.fifo_changed.emit()

    def redo(self, counter):
        if self.__undone_dateinamen and not self.item_under_edit:
            dateiname = self.__undone_dateinamen.pop()
            dateiname.counter = counter
            self.__fifo.append(dateiname)
            self.fifo_changed.emit()
            return True
        return False

    def set_dateiname_under_edit(self, index):
        self.__fifo[index].under_edit = True

    def move_fifo_item(self, old_index, new_index):
        try:
            dateiname = self.__fifo.pop(old_index)
            self.__fifo.insert(new_index, dateiname)
        except IndexError:
            pass
        finally:
            self.fifo_changed.emit()

    @property
    def item_under_edit(self):
        return any([i.under_edit for i in self.__fifo])

    @property
    def has_undone(self):
        return bool(self.__undone_dateinamen)

    def renumber_fifo(self):
        if not self.__fifo:
            return
        counter = self.__fifo[0].counter + 1
        for dateiname in self.__fifo[1:]:
            dateiname.counter = counter
            counter += 1
        self.fifo_changed.emit()

    @property
    def copied_names(self):
        return tuple(self.__copied_names)

    def add_to_copied_names(self, dateiname):
        if isinstance(dateiname, Dateiname):
            self.__copied_names.append(dateiname)
            self.copied_changed.emit()
            return True
        return False

    def remove_copied_name_at_index(self, index):
        try:
            del self.__copied_names[index]
        except IndexError:
            pass
        finally:
            self.copied_changed.emit()

    def move_copied_name_up(self, index):
        try:
            dateiname = self.__copied_names.pop(index)
            self.__copied_names.insert(index - 1, dateiname)
        finally:
            self.copied_changed.emit()

    def move_copied_name_down(self, index):
        try:
            dateiname = self.__copied_names.pop(index)
            self.__copied_names.insert(index + 1, dateiname)
        finally:
            self.copied_changed.emit()

    def copy_name_again(self, index):
        try:
            new_name = unicode(self.__copied_names[index])
            self.__copied_names[index].copy_name = new_name
        except IndexError:
            pass
        else:
            self.clipboard.setText(new_name)
            self.application.logger.log(u'"{0}" in Zwischenablage kopiert'.format(new_name), __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, False)
        finally:
            self.copied_changed.emit()

    def get_general_song_title(self, number):
        if number.isdigit() and int(number) > 0:
            number = int(number)
            try:
                title = self.general_songs[number - 1]
            except IndexError:
                return u''
            else:
                return title
        else:
            return u''

    @property
    def max_general_song(self):
        return len(self.general_songs)

    @property
    def number_length(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['number_length']

    @number_length.setter
    def number_length(self, value):
        checked_value = self.__check_number_length(value)
        if checked_value == self.__settings['number_length']:
            return
        else:
            self.__settings['number_length'] = checked_value
            self.__write_settings()

    def __check_number_length(self, value):
        if not isinstance(value, int) or not (Global.DATEINAMEN.MODEL.NUMBER_LENGTH_MIN <= value <=
                                              Global.DATEINAMEN.MODEL.NUMBER_LENGTH_MAX):
            return self.__settings['number_length']
        else:
            return value

    @property
    def delimiter(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['delimiter']

    @delimiter.setter
    def delimiter(self, value):
        checked_value = self.__check_delimiter(value)
        if checked_value == self.__settings['delimiter']:
            return
        else:
            self.__settings['delimiter'] = checked_value
            self.__write_settings()

    def __check_delimiter(self, value):
        if value not in Global.DATEINAMEN.MODEL.DELIMITERS:
            return self.__settings['delimiter']
        else:
            return value

    @property
    def invalid_chars(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['invalid_chars']

    @invalid_chars.setter
    def invalid_chars(self, value):
        checked_value = self.__check_invalid_chars(value)
        if checked_value == self.__settings['invalid_chars']:
            return
        else:
            self.__settings['invalid_chars'] = checked_value
            self.__write_settings()

    def __check_invalid_chars(self, value):
        if not isinstance(value, unicode):
            return self.__settings['invalid_chars']
        else:
            return value

    @property
    def suggestion_policy_name_completer(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['suggestion_policy_name_completer']

    @suggestion_policy_name_completer.setter
    def suggestion_policy_name_completer(self, value):
        checked_value = self.__check_suggestion_policy_name_completer(value)
        if checked_value == self.__settings['suggestion_policy_name_completer']:
            return
        else:
            self.__settings['suggestion_policy_name_completer'] = checked_value
            self.__write_settings()

    def __check_suggestion_policy_name_completer(self, value):
        if value not in MtWidgets.SUGGESTION_POLICIES:
            return self.__settings['suggestion_policy_name_completer']
        else:
            return value

    @property
    def suggestion_policy_titel_completer(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['suggestion_policy_titel_completer']

    @suggestion_policy_titel_completer.setter
    def suggestion_policy_titel_completer(self, value):
        checked_value = self.__check_suggestion_policy_titel_completer(value)
        if checked_value == self.__settings['suggestion_policy_titel_completer']:
            return
        else:
            self.__settings['suggestion_policy_titel_completer'] = checked_value
            self.__write_settings()

    def __check_suggestion_policy_titel_completer(self, value):
        if value not in MtWidgets.SUGGESTION_POLICIES:
            return self.__settings['suggestion_policy_titel_completer']
        else:
            return value

    @property
    def suggestion_policy_sonst_completer(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['suggestion_policy_sonst_completer']

    @suggestion_policy_sonst_completer.setter
    def suggestion_policy_sonst_completer(self, value):
        checked_value = self.__check_suggestion_policy_sonst_completer(value)
        if checked_value == self.__settings['suggestion_policy_sonst_completer']:
            return
        else:
            self.__settings['suggestion_policy_sonst_completer'] = checked_value
            self.__write_settings()

    def __check_suggestion_policy_sonst_completer(self, value):
        if value not in MtWidgets.SUGGESTION_POLICIES:
            return self.__settings['suggestion_policy_sonst_completer']
        else:
            return value

    @property
    def suggestion_policy_stelle_completer(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['suggestion_policy_stelle_completer']

    @suggestion_policy_stelle_completer.setter
    def suggestion_policy_stelle_completer(self, value):
        checked_value = self.__check_suggestion_policy_stelle_completer(value)
        if checked_value == self.__settings['suggestion_policy_stelle_completer']:
            return
        else:
            self.__settings['suggestion_policy_stelle_completer'] = checked_value
            self.__write_settings()

    def __check_suggestion_policy_stelle_completer(self, value):
        if value not in MtWidgets.SUGGESTION_POLICIES:
            return self.__settings['suggestion_policy_stelle_completer']
        else:
            return value

    def set_suggestion_policies(self):
        self.name_completer.set_suggestion_policy(self.suggestion_policy_name_completer)
        self.titel_file_completer.set_suggestion_policy(self.suggestion_policy_titel_completer)
        self.sonst_file_completer.set_suggestion_policy(self.suggestion_policy_sonst_completer)
        self.stelle_list_completer.set_suggestion_policy(self.suggestion_policy_stelle_completer)

    @property
    def type_definitions(self):
        return tuple(self.__type_definitions)

    def add_to_type_definitions(self, type_definition):
        if type_definition in self.__type_definitions:
            return True
        if isinstance(type_definition, TypeDefinition):
            self.__type_definitions.append(type_definition)
            self.__type_definitions.sort(key=lambda a: a.display_name.lower())
            self.__write_type_definitions()
            self.type_definitions_changed.emit()
            return True
        return False

    def get_type_definitions_at_index(self, index):
        return self.__type_definitions[index]

    def remove_type_definition_at_index(self, index):
        try:
            del self.__type_definitions[index]
        except IndexError:
            pass
        self.__write_type_definitions()
        self.type_definitions_changed.emit()

    def update_type_definition(self, type_definition, index):
        if index < len(self.__type_definitions):
            old_type_definition = self.__type_definitions[index]
            if old_type_definition == type_definition:
                return
            for dateiname in self.__fifo:
                if dateiname.type_definition == old_type_definition:
                    dateiname.type_definition = type_definition
            for dateiname in self.__copied_names:
                if dateiname.type_definition == old_type_definition:
                    dateiname.type_definition = type_definition
            self.__type_definitions.sort(key=lambda a: a.display_name.lower())
            self.__type_definitions[index] = type_definition
            self.__write_type_definitions()
            self.fifo_changed.emit()
            self.copied_changed.emit()
            self.type_definitions_changed.emit()
            self.model_changed.emit()

    def __write_type_definitions(self):
        json_data = [i.get_dict() for i in self.__type_definitions]
        try:
            write_file = open(Global.DATEINAMEN.SETTINGS.TYPE_DEFINITIONS_PATH, 'w')
            json.dump(json_data, write_file)
        except IOError as e:
            self.application.logger.log(u'Fehler beim schreiben von Typdefinitionen in Datei: {0}'.format(unicode(e)),
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, False, False)

    def __load_type_definitions(self):
        if os.path.isfile(Global.DATEINAMEN.SETTINGS.TYPE_DEFINITIONS_PATH):
            try:
                read_file = open(Global.DATEINAMEN.SETTINGS.TYPE_DEFINITIONS_PATH, 'r')
                json_data = json.load(read_file)
            except IOError as e:
                self.application.logger.log(u'Fehler beim laden von Typdefinitionen aus Datei: {0}'.format(unicode(e)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, False, False)
            else:
                for type_definition_dict in json_data:
                    self.add_to_type_definitions(TypeDefinition(type_definition_dict))
        else:
            self.__type_definitions = list()
        self.type_definitions_changed.emit()

    @property
    def sub_dirs(self):
        return tuple(self.__sub_dirs)

    def add_to_sub_dirs(self, sub_dir):
        if sub_dir in self.__sub_dirs:
            return True
        if isinstance(sub_dir, basestring):
            sub_dir = re.sub(Global.WHITESPACES, u'', sub_dir)
            sub_dir = re.sub(Global.ILLEGAL_PATH_CHARS, u'', sub_dir)
            self.__sub_dirs.append(sub_dir)
            self.__write_sub_dirs()
            self.sub_dirs_changed.emit()
            return True
        return False

    def remove_sub_dir_at_index(self, index):
        try:
            del self.__sub_dirs[index]
        except IndexError:
            pass
        self.__write_sub_dirs()
        self.sub_dirs_changed.emit()

    def __write_sub_dirs(self):
        json_data = self.__sub_dirs
        try:
            write_file = open(Global.DATEINAMEN.SETTINGS.SUB_DIRS_PATH, 'w')
            json.dump(json_data, write_file)
        except IOError as e:
            self.application.logger.log(u'Fehler beim schreiben von Unterordnern in Datei: {0}'.format(unicode(e)),
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, False, False)

    def __load_sub_dirs(self):
        self.__sub_dirs = list()
        if os.path.isfile(Global.DATEINAMEN.SETTINGS.SUB_DIRS_PATH):
            try:
                read_file = open(Global.DATEINAMEN.SETTINGS.SUB_DIRS_PATH, 'r')
                json_data = json.load(read_file)
            except IOError as e:
                self.application.logger.log(u'Fehler beim laden von Unterordnern aus Datei: {0}'.format(unicode(e)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, False, False)
            else:
                for sub_dir in json_data:
                    self.add_to_sub_dirs(sub_dir)
        self.sub_dirs_changed.emit()


class Dateiname(object):
    def __init__(self, model, dateinamen_dict=None):
        self.model = model
        self.counter = 0
        self.type = -1
        self.general_song = u''
        self.type_other_text = u''
        self.name = -1
        self.name_text = u''
        self.scripture = -1
        self.scripture_book = u''
        self.scripture_text = u''
        self.title = -1
        self.title_text = u''
        self.other = -1
        self.other_text = u''
        self.copy_name = u''
        self.under_edit = False
        self.type_definition = None
        if dateinamen_dict:
            self.load_dict(dateinamen_dict)

    def __remove_illegal_chars(self, text):
        text = re.sub(Global.WHITESPACES, u'', text)
        text = re.sub(Global.ILLEGAL_PATH_CHARS, u'', text)
        for char in self.model.invalid_chars:
            text = text.replace(char, u'')
        return text

    def correct_name(self, name):
        name = name.strip()
        return self.__remove_illegal_chars(name)

    def correct_text(self, text):
        return self.__remove_illegal_chars(text.strip())

    def __unicode__(self):

        temp_list = []
        if self.model.number_length:
            temp_number = u'%0' + unicode(self.model.number_length) + u'i'
            temp_list.append(temp_number % self.counter)

        if self.name == Global.DATEINAMEN.RB_NAME.NAME:
            temp_name = u'(' + self.correct_name(self.name_text) + u')'
        else:
            temp_name = u''

        if self.scripture == Global.DATEINAMEN.RB_STELLE.STELLE:
            temp_scripture = self.correct_text(self.scripture_book) + self.model.delimiter + \
                             self.correct_text(self.scripture_text)
        else:
            temp_scripture = u''

        if self.title == Global.DATEINAMEN.RB_TITEL.TITEL:
            temp_title = self.correct_text(self.title_text)
        else:
            temp_title = u''

        if self.other == Global.DATEINAMEN.RB_SONST.SONST:
            temp_other = self.correct_text(self.other_text)
        else:
            temp_other = u''

        temp_general_song = self.correct_text(self.general_song.lstrip('0'))
        temp_general_song += self.model.delimiter
        temp_general_song += self.correct_text(self.model.get_general_song_title(self.general_song))
        temp_type_other = self.correct_text(self.type_other_text)

        if self.type == Global.DATEINAMEN.RB_TYPE.EROEFFNUNG:
            temp_list += [u"Eröffnung", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.GEBET:
            temp_list += [u"Gebet", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.ANSPRACHE:
            temp_list += [u"Ansprache", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.MODERATION:
            temp_list += [u"Moderation", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.ABSCHLUSS:
            temp_list += [u"Abschluss", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.CHOR:
            temp_list += [u"ChL", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
            temp_list += [u"AllgL", temp_general_song]
        elif self.type == Global.DATEINAMEN.RB_TYPE.MUSIK:
            temp_list += [u"Musik", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.ORCHESTER:
            temp_list += [u"Orch", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.GRUPPENLIED:
            temp_list += [u"GrLied", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.JUGENDCHOR:
            temp_list += [u"JugChor", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.KINDERCHOR:
            temp_list += [u"KiChor", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.ANSPIEL:
            temp_list += [u"Anspiel", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.ZEUGNIS:
            temp_list += [u"Zeugnis", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.TEXT:
            temp_list += [u"Text", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.GEDICHT:
            temp_list += [u"Gedicht", temp_title, temp_scripture, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.PROSA:
            temp_list += [u"Prosa", temp_title, temp_scripture, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.BEITRAG:
            temp_list += [u"Beitrag", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.BIBELSTUNDE:
            temp_list += [u"Bibelstunde", temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.SONST:
            temp_list += [temp_type_other, temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.NICHTS:
            temp_list += [temp_scripture, temp_title, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.THEMA:
            temp_list += [u"Thema", temp_title, temp_scripture, temp_other, temp_name]
        elif self.type == Global.DATEINAMEN.RB_TYPE.DEFINITION:
            if self.type_definition is None:
                temp_list += [temp_title, temp_scripture, temp_other, temp_name]
            else:
                temp_list += [self.type_definition.abbreviation, temp_title, temp_scripture, temp_other, temp_name]
        else:
            pass

        while u'' in temp_list:
            temp_list.remove(u'')
        temp = self.model.delimiter.join(temp_list)

        temp = self.__remove_illegal_chars(temp)

        return temp

    def get_errors(self):
        errors = list()

        if self.type == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
            if self.general_song.isdigit():
                number = int(self.general_song)
                if number < 1:
                    errors.append(u'Nummer für das Allg. Lied muss >= 1 sein')
                if number > self.model.max_general_song:
                    errors.append(u'Nummer für das Allg. Lied muss <= %i sein' % self.model.max_general_song)
            else:
                errors.append(u'Allgemeines Lied muss eine Zahl sein')
        else:
            if self.name == Global.DATEINAMEN.RB_NAME.NAME and not self.correct_name(self.name_text):
                errors.append(u'Kein Name angegeben')
            if self.scripture == Global.DATEINAMEN.RB_STELLE.STELLE:
                book = self.correct_text(self.scripture_book)
                if not book:
                    errors.append(u'Kein Bibelbuch angegeben')
                elif not self.model.stelle_list_completer.has_item(book):
                    errors.append(u'Fehlerhafte Abkürzung für Bibelbuch')
                if not self.correct_text(self.scripture_text):
                    errors.append(u'Keine Bibelstelle angegeben')
            if self.title == Global.DATEINAMEN.RB_TITEL.TITEL and not self.correct_text(self.title_text):
                errors.append(u'Kein Titel angegeben')
            if self.other == Global.DATEINAMEN.RB_SONST.SONST and not self.correct_text(self.other_text):
                errors.append(u'Nichts im Feld "Sonst" eingeben')
            if self.type == Global.DATEINAMEN.RB_TYPE.SONST and not self.correct_text(self.type_other_text):
                errors.append(u'Nichts in "Sonstiges" angegeben')

        return errors

    def get_dict(self):
        dateinamen_dict = dict()
        dateinamen_dict['counter'] = self.counter
        if self.type == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
            dateinamen_dict['general_song'] = self.general_song
        else:
            dateinamen_dict['general_song'] = None
        if self.type == Global.DATEINAMEN.RB_TYPE.SONST:
            dateinamen_dict['type_other_text'] = self.type_other_text
        else:
            dateinamen_dict['type_other_text'] = None
        if self.name == Global.DATEINAMEN.RB_NAME.NAME:
            dateinamen_dict['name'] = self.name_text
        else:
            dateinamen_dict['name'] = None
        if self.scripture == Global.DATEINAMEN.RB_STELLE.STELLE:
            dateinamen_dict['scripture_book'] = self.scripture_book
            dateinamen_dict['scripture_text'] = self.scripture_text
        else:
            dateinamen_dict['scripture_book'] = None
            dateinamen_dict['scripture_text'] = None
        if self.title == Global.DATEINAMEN.RB_TITEL.TITEL:
            dateinamen_dict['title'] = self.title_text
        else:
            dateinamen_dict['title'] = None
        if self.other == Global.DATEINAMEN.RB_SONST.SONST:
            dateinamen_dict['other'] = self.other_text
        else:
            dateinamen_dict['other'] = None
        if self.type_definition is None:
            dateinamen_dict['type_definition'] = None
        else:
            dateinamen_dict['type_definition'] = self.type_definition.get_dict()
        return dateinamen_dict

    def load_dict(self, dateinamen_dict):
        counter = int(dateinamen_dict['counter'])
        if dateinamen_dict['type_definition'] is None:
            type_definition = None
            if dateinamen_dict['type_other_text'] is None:
                type_ = Global.DATEINAMEN.RB_TYPE.NICHTS
                general_song = u''
            else:
                type_ = Global.DATEINAMEN.RB_TYPE.SONST
                general_song = u''
        else:
            type_definition = TypeDefinition(dateinamen_dict['type_definition'])
            if type_definition in Global.DATEINAMEN.RB_TYPE.TYPE_DEFINITIONS:
                type_ = Global.DATEINAMEN.RB_TYPE.TYPE_DEFINITIONS.index(type_definition)
                if type_ == Global.DATEINAMEN.RB_TYPE.ALLG_LIED and dateinamen_dict['general_song'] is not None:
                    general_song = unicode(dateinamen_dict['general_song'])
                else:
                    general_song = u''
            else:
                type_ = Global.DATEINAMEN.RB_TYPE.DEFINITION
                general_song = u''
                if type_definition not in self.model.type_definitions:
                    self.model.add_to_type_definitions(type_definition)
        if type_ == Global.DATEINAMEN.RB_TYPE.SONST and dateinamen_dict['type_other_text'] is not None:
            type_other_text = unicode(dateinamen_dict['type_other_text'])
        else:
            type_other_text = u''
        if type_ == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
            name = Global.DATEINAMEN.RB_NAME.NICHTS
            name_text = u''
            scripture = Global.DATEINAMEN.RB_STELLE.NICHTS
            scripture_book = u''
            scripture_text = u''
            title = Global.DATEINAMEN.RB_TITEL.NICHTS
            title_text = u''
            other = Global.DATEINAMEN.RB_TITEL.NICHTS
            other_text = u''
        else:
            if dateinamen_dict['name'] is None:
                name = Global.DATEINAMEN.RB_NAME.NICHTS
                name_text = u''
            else:
                name = Global.DATEINAMEN.RB_NAME.NAME
                name_text = unicode(dateinamen_dict['name'])
            if dateinamen_dict['scripture_book'] is None or dateinamen_dict['scripture_text'] is None:
                scripture = Global.DATEINAMEN.RB_STELLE.NICHTS
                scripture_book = u''
                scripture_text = u''
            else:
                scripture = Global.DATEINAMEN.RB_STELLE.STELLE
                scripture_book = unicode(dateinamen_dict['scripture_book'])
                scripture_text = unicode(dateinamen_dict['scripture_text'])
            if dateinamen_dict['title'] is None:
                title = Global.DATEINAMEN.RB_TITEL.NICHTS
                title_text = u''
            else:
                title = Global.DATEINAMEN.RB_TITEL.TITEL
                title_text = unicode(dateinamen_dict['title'])
            if dateinamen_dict['other'] is None:
                other = Global.DATEINAMEN.RB_SONST.NICHTS
                other_text = u''
            else:
                other = Global.DATEINAMEN.RB_SONST.SONST
                other_text = unicode(dateinamen_dict['other'])

        self.counter = counter
        self.type = type_
        self.general_song = general_song
        self.type_other_text = type_other_text
        self.name = name
        self.name_text = name_text
        self.scripture = scripture
        self.scripture_book = scripture_book
        self.scripture_text = scripture_text
        self.title = title
        self.title_text = title_text
        self.other = other
        self.other_text = other_text
        self.under_edit = False
        self.type_definition = type_definition
        self.copy_name = unicode(self)


class GeneralSongsHandler(object):

    def __init__(self, application, model, path):
        self.application = application
        self.model = model
        self.path = path
        self.modification_time = 0
        self.songs = []
        self.load_file()

    def check_reload(self):
        try:
            return self.modification_time != os.path.getmtime(self.path)
        except IOError:
            return True

    def __write_default_file(self):
        try:
            w_file = codecs.open(self.path, 'w', 'utf-8')
        except IOError:
            self.application.logger.log(u'Fehler trat beim Schreiben des Allgemeinen Liederbuchs auf.',
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            w_file.write('\n'.join(Global.DATEINAMEN.MODEL.GENERAL_SONGS))
            w_file.close()

    def load_file(self):
        if not os.path.isfile(self.path):
            self.__write_default_file()
        try:
            r_file = codecs.open(self.path, 'r', 'utf-8')
        except IOError:
            self.application.logger.log(u'Fehler trat beim Lesen des Allgemeinen Liederbuchs auf.',
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)

        else:
            content = r_file.readlines()
            r_file.close()
            self.songs = [i.strip('\r\n') for i in content]
            self.modification_time = os.path.getmtime(self.path)
            self.model.model_changed.emit()

    def import_file(self, path):
        try:
            shutil.copyfile(path, self.path)
        except shutil.Error:
            self.application.logger.log(u'Fehler trat beim importieren des Allgemeinen Liederbuchs auf.',
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.application.logger.log(u'Allgemeines Liederbuch wurde importiert.',
                                        __name__, MediaToolsLogger.SEVERITY_INFO, True, False)
            self.load_file()

    def reset(self):
        self.__write_default_file()
        self.application.logger.log(u'Allgemeines Liederbuch wurde auf Werkseinstellungen zurückgesetzt.',
                                    __name__, MediaToolsLogger.SEVERITY_INFO, True, False)
        self.load_file()

    def __len__(self):
        if self.check_reload():
            self.load_file()
        return len(self.songs)

    def __getitem__(self, item):
        if self.check_reload():
            self.load_file()
        return self.songs[item]


class TypeDefinition(object):

    def __init__(self, type_definition_dict=None):
        self.display_name = u''
        self.abbreviation = u''
        self.id3_tag_title = u''
        self.id3_tag_overwrite_title = False
        self.id3_tag_genre = u''
        self.id3_tag_interpret = u''
        self.id3_tag_overwrite_interpret = False
        self.name_radio_button = False
        self.stelle_radio_button = False
        self.titel_radio_button = False
        self.sonst_radio_button = False
        if type_definition_dict:
            self.load_dict(type_definition_dict)

    def __unicode__(self):
        return self.display_name

    def get_dict(self):
        values_dict = dict()
        values_dict['display_name'] = self.display_name
        values_dict['abbreviation'] = self.abbreviation
        values_dict['id3_tag_title'] = self.id3_tag_title
        values_dict['id3_tag_overwrite_title'] = self.id3_tag_overwrite_title
        values_dict['id3_tag_genre'] = self.id3_tag_genre
        values_dict['id3_tag_interpret'] = self.id3_tag_interpret
        values_dict['id3_tag_overwrite_interpret'] = self.id3_tag_overwrite_interpret
        values_dict['name_radio_button'] = self.name_radio_button
        values_dict['stelle_radio_button'] = self.stelle_radio_button
        values_dict['titel_radio_button'] = self.titel_radio_button
        values_dict['sonst_radio_button'] = self.sonst_radio_button
        return values_dict

    def load_dict(self, type_definition_dict):
        self_copy = copy.deepcopy(self)
        try:
            self.display_name = unicode(type_definition_dict['display_name'])
            self.abbreviation = unicode(type_definition_dict['abbreviation'])
            self.id3_tag_title = unicode(type_definition_dict['id3_tag_title'])
            self.id3_tag_overwrite_title = bool(type_definition_dict['id3_tag_overwrite_title'])
            self.id3_tag_genre = unicode(type_definition_dict['id3_tag_genre'])
            self.id3_tag_interpret = unicode(type_definition_dict['id3_tag_interpret'])
            self.id3_tag_overwrite_interpret = bool(type_definition_dict['id3_tag_overwrite_interpret'])
            self.name_radio_button = bool(type_definition_dict['name_radio_button'])
            self.stelle_radio_button = bool(type_definition_dict['stelle_radio_button'])
            self.titel_radio_button = bool(type_definition_dict['titel_radio_button'])
            self.sonst_radio_button = bool(type_definition_dict['sonst_radio_button'])
        except Exception as e:
            self.display_name = self_copy.display_name
            self.abbreviation = self_copy.abbreviation
            self.id3_tag_title = self_copy.id3_tag_title
            self.id3_tag_overwrite_title = self_copy.id3_tag_overwrite_title
            self.id3_tag_genre = self_copy.id3_tag_genre
            self.id3_tag_interpret = self_copy.id3_tag_interpret
            self.id3_tag_overwrite_interpret = self_copy.id3_tag_overwrite_interpret
            self.name_radio_button = self_copy.name_radio_button
            self.stelle_radio_button = self_copy.stelle_radio_button
            self.titel_radio_button = self_copy.titel_radio_button
            self.sonst_radio_button = self_copy.sonst_radio_button
            raise e

    def __eq__(self, other):
        if not isinstance(other, TypeDefinition):
            return False
        if not self.display_name == other.display_name:
            return False
        if not self.abbreviation == other.abbreviation:
            return False
        if not self.id3_tag_title == other.id3_tag_title:
            return False
        if not self.id3_tag_overwrite_title == other.id3_tag_overwrite_title:
            return False
        if not self.id3_tag_genre == other.id3_tag_genre:
            return False
        if not self.id3_tag_interpret == other.id3_tag_interpret:
            return False
        if not self.id3_tag_overwrite_interpret == other.id3_tag_overwrite_interpret:
            return False
        if not self.name_radio_button == other.name_radio_button:
            return False
        if not self.stelle_radio_button == other.stelle_radio_button:
            return False
        if not self.titel_radio_button == other.titel_radio_button:
            return False
        if not self.sonst_radio_button == other.sonst_radio_button:
            return False
        return True
