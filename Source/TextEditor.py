# coding=utf-8

import os
import codecs
from PySide import QtGui
from PySide import QtCore

import Global
import MediaToolsLogger


class TextEditorWidget(QtGui.QWidget):

    def __init__(self, application, file_path=None, text=None, encoding="utf-8", immutable=False, sort_enabled=False):
        super(self.__class__, self).__init__()

        self.application = application
        self.file_path = None
        self.encoding = encoding
        self.immutable = bool(immutable)
        self.modification_time = 0
        self.changes_saved = True
        self.sort_ascending = True

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        buttons_widget = QtGui.QWidget()
        buttons_layout = QtGui.QHBoxLayout()
        buttons_layout.setContentsMargins(0, 0, 0, 0)
        buttons_widget.setLayout(buttons_layout)

        if not self.immutable:
            save_button = QtGui.QPushButton(u'Speichern')
            save_button.setToolTip(u'Datei speichern')
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-save-1-240.png'))
            save_button.setIcon(icon)
            # noinspection PyUnresolvedReferences
            save_button.clicked.connect(self.save_file)
            buttons_layout.addWidget(save_button)
            self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.Save), self),
                         QtCore.SIGNAL('activated()'), self.save_file)

        save_as_button = QtGui.QPushButton(u'Speichern unter\u2026')
        save_as_button.setToolTip(u'Datei speichern unter\u2026')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-save-3-240.png'))
        save_as_button.setIcon(icon)
        # noinspection PyUnresolvedReferences
        save_as_button.clicked.connect(self.save_file_as)
        buttons_layout.addWidget(save_as_button)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.SHIFT + QtCore.Qt.Key_S), self),
                     QtCore.SIGNAL('activated()'), self.save_file_as)

        if not self.immutable:
            undo_button = QtGui.QPushButton(u'')
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-undo-2-240.png'))
            undo_button.setIcon(icon)
            undo_button.setToolTip(u'Rückgängig machen')
            # noinspection PyUnresolvedReferences
            undo_button.clicked.connect(self.undo)
            buttons_layout.addWidget(undo_button)

            redo_button = QtGui.QPushButton(u'')
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-redo-2-240.png'))
            redo_button.setIcon(icon)
            redo_button.setToolTip(u'Wiederherstellen')
            # noinspection PyUnresolvedReferences
            redo_button.clicked.connect(self.redo)
            buttons_layout.addWidget(redo_button)

        buttons_layout.addStretch()

        if sort_enabled:
            self.sort_button = QtGui.QPushButton(u'Sortieren')
            self.icon_sort_ascending = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-sort-14-240.png'))
            self.icon_sort_descending = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-sort-16-240.png'))
            self.sort_button.setIcon(self.icon_sort_ascending)
            self.sort_button.setToolTip(u'Alphabetisch sortieren')
            # noinspection PyUnresolvedReferences
            self.sort_button.clicked.connect(self.sort)
            buttons_layout.addWidget(self.sort_button)

        layout.addWidget(buttons_widget)

        self.text_edit = QtGui.QPlainTextEdit(self)
        self.text_edit.setLineWrapMode(QtGui.QPlainTextEdit.NoWrap)
        self.text_edit.setUndoRedoEnabled(True)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.Find), self),
                     QtCore.SIGNAL('activated()'), self.show_search)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.FindNext), self),
                     QtCore.SIGNAL('activated()'), self.search)
        self.connect(QtGui.QShortcut(QtGui.QKeySequence(QtGui.QKeySequence.StandardKey.FindPrevious), self),
                     QtCore.SIGNAL('activated()'), lambda: self.search(False))
        layout.addWidget(self.text_edit)

        self.search_widget = QtGui.QWidget()
        search_layout = QtGui.QHBoxLayout()
        search_layout.setContentsMargins(0, 0, 0, 0)
        self.search_widget.setLayout(search_layout)

        search_layout.addWidget(QtGui.QLabel('Suche:'))
        self.search_line_edit = QtGui.QLineEdit()
        # noinspection PyUnresolvedReferences
        self.search_line_edit.returnPressed.connect(self.search)
        self.search_line_edit.connect(QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Escape), self.search_line_edit),
                                      QtCore.SIGNAL('activated()'), self.hide_search)
        search_layout.addWidget(self.search_line_edit, 1)

        button_search_down = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-3-240-down.png'))
        button_search_down.setIcon(icon)
        button_search_down.setToolTip(u'Suche nach unten')
        # noinspection PyUnresolvedReferences
        button_search_down.clicked.connect(lambda: self.search(True))
        search_layout.addWidget(button_search_down)

        button_search_up = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-3-240-up.png'))
        button_search_up.setIcon(icon)
        button_search_up.setToolTip(u'Suche nach oben')
        # noinspection PyUnresolvedReferences
        button_search_up.clicked.connect(lambda: self.search(False))
        search_layout.addWidget(button_search_up)

        button_hide_search = QtGui.QPushButton()
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-4-240.png'))
        button_hide_search.setIcon(icon)
        button_hide_search.setToolTip(u'Suchfenster schließen')
        # noinspection PyUnresolvedReferences
        button_hide_search.clicked.connect(self.hide_search)
        search_layout.addWidget(button_hide_search)
        self.search_widget.setVisible(False)

        layout.addWidget(self.search_widget)

        if file_path is not None:
            if os.path.isfile(file_path):
                self.file_path = file_path
                self.__read_file()
            else:
                self.application.logger.log(u'Die zu öffnende Datei existiert nicht. '
                                            u'Es wird eine leere Datei geöffnet.',
                                            __name__, MediaToolsLogger.SEVERITY_WARNING,
                                            dialog=True,
                                            status_bar=True)
        elif text is not None:
            self.text_edit.setPlainText(text)

        self.text_edit.setReadOnly(self.immutable)

        # noinspection PyUnresolvedReferences
        self.text_edit.textChanged.connect(self.on_change)

    def on_change(self):
        self.changes_saved = False

    def undo(self):
        self.text_edit.undo()

    def redo(self):
        self.text_edit.redo()

    def sort(self):
        buffer_lines = self.text_edit.toPlainText().splitlines()
        while u'\n' in buffer_lines:
            buffer_lines.remove(u'\n')
        while u'\r' in buffer_lines:
            buffer_lines.remove(u'\r')
        while u'\r\n' in buffer_lines:
            buffer_lines.remove(u'\r\n')
        buffer_lines.sort(reverse=not self.sort_ascending, key=lambda a: a.lower())
        self.sort_ascending = not self.sort_ascending
        if self.sort_ascending:
            self.sort_button.setIcon(self.icon_sort_ascending)
        else:
            self.sort_button.setIcon(self.icon_sort_descending)
        self.text_edit.selectAll()
        cursor = self.text_edit.textCursor()
        cursor.clearSelection()
        self.text_edit.insertPlainText(os.linesep.join(buffer_lines).rstrip(u'\n\r') + os.linesep)

    def hide_search(self):
        self.search_widget.setVisible(False)

    def show_search(self):
        if self.search_widget.isVisible():
            self.search()
        else:
            self.search_widget.setVisible(True)
            self.search_line_edit.setFocus()
            self.search_line_edit.selectAll()

    def search(self, search_direction_up=True):
        if not self.search_widget.isVisible():
            return
        search_text = self.search_line_edit.text().lower()
        if not search_text:
            return
        cursor = self.text_edit.textCursor()
        text = self.text_edit.toPlainText().lower()
        if search_direction_up:
            find_pos = text.find(search_text, cursor.position())
        else:
            find_pos = text.rfind(search_text, 0, cursor.position())
        if find_pos < 0:
            if search_direction_up:
                find_pos = text.find(search_text)
            else:
                find_pos = text.rfind(search_text)
        if find_pos >= 0:
            self.text_edit.setFocus()
            if search_direction_up:
                cursor.setPosition(find_pos)
                cursor.setPosition(find_pos + len(search_text), QtGui.QTextCursor.KeepAnchor)
            else:
                cursor.setPosition(find_pos + len(search_text))
                cursor.setPosition(find_pos, QtGui.QTextCursor.KeepAnchor)
            self.text_edit.setTextCursor(cursor)

    def check_reload(self):
        if not self.file_path:
            return
        if os.path.exists(self.file_path) and self.modification_time != os.path.getmtime(self.file_path):
            self.modification_time = os.path.getmtime(self.file_path)
            question = u"Die Datei wurde extern geändert. Soll die Datei neu geladen werden? " \
                       u"Eventuelle Änderungen gehen dabei verloren."
            message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                            u'Datei wurde geändert',
                                            question,
                                            QtGui.QMessageBox.NoButton,
                                            self.application.main_window)
            message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
            message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)

            response = message_box.exec_()
            if response == 0:  # yes
                self.__read_file()

    def __read_file(self):
        try:
            read_file = codecs.open(self.file_path, 'r', self.encoding)
        except IOError:
            self.application.logger.log(u'Fehler beim öffnen der Datei: %s' % self.file_path, __name__,
                                        severity=MediaToolsLogger.SEVERITY_ERROR,
                                        dialog=True,
                                        status_bar=True)
        else:
            self.text_edit.setPlainText(read_file.read())
            self.modification_time = os.path.getmtime(self.file_path)
            self.changes_saved = True

    def save_file_as(self):
        dialog = QtGui.QFileDialog()
        answer = dialog.getSaveFileName(dir=os.path.expanduser('~'))
        if answer[0]:
            self.file_path = answer[0]
            self.save_file()

    def save_file(self):
        if self.file_path is None:
            self.save_file_as()
            return True
        try:
            write_file = codecs.open(self.file_path, 'w', self.encoding)
        except IOError:
            self.application.logger.log(u'Fehler beim speichern der Datei: %s' % self.file_path, __name__,
                                        severity=MediaToolsLogger.SEVERITY_ERROR,
                                        dialog=True,
                                        status_bar=True)
            return False
        else:
            write_file.write(self.text_edit.toPlainText())
            self.application.logger.log(u"Datei gespeichert unter: %s" % self.file_path, __name__,
                                        MediaToolsLogger.SEVERITY_INFO)
            write_file.close()
            self.modification_time = os.path.getmtime(self.file_path)
            self.changes_saved = True
            return True

    def ask_save_file_on_close(self):
        if self.file_path is None or self.immutable:
            return True
        if self.changes_saved:
            return True
        else:
            question = u'Änderungen an der Datei wurden nicht gespeichert. Soll die Datei jetzt gespeichert werden?'
            message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                            u'Frage',
                                            question,
                                            QtGui.QMessageBox.NoButton,
                                            self.application.main_window)
            message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
            message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)
            message_box.addButton(u"Abbrechen", QtGui.QMessageBox.RejectRole)

            response = message_box.exec_()
            if response == 0:
                return self.save_file()
            elif response == 1:
                return True
            elif response == 2:
                return False
            else:
                return True
