# coding=utf-8

import codecs
import datetime
import linecache
from PySide import QtGui
from PySide import QtCore
import os

import Global
import MediaToolsLogger
import MtWidgets


class Mp3tagWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application
        self.dateinamen_model = application.main_window.dateinamen_model

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        self.exported_widget = MtWidgets.ExportedNamesWidget(application, True)
        layout.addWidget(self.exported_widget, 1)

        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        widget_2_1 = QtGui.QWidget()
        layout_2.addWidget(widget_2_1)
        layout_2_1 = QtGui.QVBoxLayout()
        layout_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_1.setLayout(layout_2_1)

        label = QtGui.QLabel(u'Ereignis:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Gemeinde:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Datum:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Formatstring:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)

        widget_2_2 = QtGui.QWidget()
        layout_2.addWidget(widget_2_2)
        layout_2_2 = QtGui.QVBoxLayout()
        layout_2_2.setContentsMargins(0, 0, 0, 0)
        widget_2_2.setLayout(layout_2_2)

        # Ereignis
        self.ereignis_line_edit = QtGui.QLineEdit()
        self.ereignis_completer = MtWidgets.FileCompleter(self.application, Global.MEDIADOCKINFOFILES.FILES.EVENTS)
        self.ereignis_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.ereignis_line_edit.setCompleter(self.ereignis_completer)
        layout_2_2.addWidget(self.ereignis_line_edit)
        self.ereignis_line_edit.setText(u'')
        # Gemeinde
        self.gemeinde_line_edit = QtGui.QLineEdit()
        self.gemeinde_completer = MtWidgets.FileCompleter(self.application, Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        self.gemeinde_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.gemeinde_line_edit.setCompleter(self.gemeinde_completer)
        layout_2_2.addWidget(self.gemeinde_line_edit)
        linecache.checkcache(Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        first_line = linecache.getline(Global.MEDIADOCKINFOFILES.FILES.CHURCHES, 1).decode('utf-8').strip('\n\r')
        self.gemeinde_line_edit.setText(first_line)
        # Datum
        widget_2_2_0 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_0)
        layout_2_2_0 = QtGui.QHBoxLayout()
        layout_2_2_0.setContentsMargins(0, 0, 0, 0)
        widget_2_2_0.setLayout(layout_2_2_0)

        self.date_edit = QtGui.QDateEdit()
        self.date_edit.setCalendarPopup(True)
        self.__show_today()
        layout_2_2_0.addWidget(self.date_edit, 1)

        today_button = QtGui.QPushButton(u'Heute')
        today_button.setToolTip(u'Heute anzeigen')
        # noinspection PyUnresolvedReferences
        today_button.clicked.connect(self.__show_today)
        layout_2_2_0.addWidget(today_button)

        # Formatstring
        widget_2_2_1 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_1)
        layout_2_2_1 = QtGui.QHBoxLayout()
        layout_2_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_2_1.setLayout(layout_2_2_1)

        self.formatstring_combo_box = QtGui.QComboBox()
        for item in Global.ID3TAGS.MP3TAG.FORMATSTRINGS:
            self.formatstring_combo_box.addItem(item)
        # noinspection PyUnresolvedReferences)
        layout_2_2_1.addWidget(self.formatstring_combo_box, 1)

        button_copy_formatstring = QtGui.QPushButton(u'')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-copy-7-240.png'))
        button_copy_formatstring.setIcon(icon)
        button_copy_formatstring.setToolTip(u'Formatstring in Zwischenablage kopieren')
        # noinspection PyUnresolvedReferences
        button_copy_formatstring.clicked.connect(self.__copy_formatstring)
        layout_2_2_1.addWidget(button_copy_formatstring)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_0 = QtGui.QWidget()
        layout.addWidget(widget_0)
        layout_0 = QtGui.QHBoxLayout()
        layout_0.setContentsMargins(0, 0, 0, 0)
        widget_0.setLayout(layout_0)

        speichere_datei_button = QtGui.QPushButton(u'Alles speichern')
        speichere_datei_button.setToolTip(u'Alles als Textdatei für Mp3tag speichern')
        # noinspection PyUnresolvedReferences
        speichere_datei_button.clicked.connect(self.save_all)
        layout_0.addWidget(speichere_datei_button)

        self.button_save_selected = QtGui.QPushButton(u'Auswahl speichern')
        self.button_save_selected.setToolTip(u'Auswahl als Textdatei für Mp3tag speichern')
        # noinspection PyUnresolvedReferences
        self.button_save_selected.clicked.connect(self.save_selected)
        layout_0.addWidget(self.button_save_selected)

        layout_0.addStretch()

    def save_all(self):
        self.save_file(range(len(self.dateinamen_model.copied_names)))

    def save_selected(self):
        self.save_file(self.exported_widget.get_selected_item_indexes())

    def __show_today(self):
        now = datetime.datetime.now()
        self.date_edit.setDate(QtCore.QDate(now.year, now.month, now.day))

    def __copy_formatstring(self):
        text = self.formatstring_combo_box.itemText(self.formatstring_combo_box.currentIndex())
        clipboard = QtGui.QClipboard()
        clipboard.setText(text)
        self.application.logger.log(u"Formatstring in Zwischenablage kopiert",
                                    __name__, MediaToolsLogger.SEVERITY_INFO, True, False)

    def __get_formated_text(self, indexes, event, church, date):
        dateinamen = [self.dateinamen_model.copied_names[index] for index in indexes]
        lines = list()
        for dateiname in dateinamen:
            id3dict = convert_dateiname_to_id3dict(self.application, dateiname)
            raw_line = u'{0} !|! {4}_{3}_{2} {1}  !|! {5} !|! {6} !|! {7} !|! {8} !|! {9} !|! {10}{11}'
            lines.append(raw_line.format(id3dict[u'Interpret'],
                                         event,
                                         str(date.day()).zfill(2),
                                         str(date.month()).zfill(2),
                                         date.year(),
                                         id3dict[u'Title'],
                                         id3dict[u'Track'],
                                         date.year(),
                                         id3dict[u'Genre'],
                                         church,
                                         str(date.day()).zfill(2),
                                         str(date.month()).zfill(2),
                                         )
                         )
        text = u'\r\n'.join(lines)
        return text

    def save_file(self, indexes):

        if not indexes:
            self.application.logger.log(u'Keine Dateinamen ausgewählt.', __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, True)
            return
        event = self.ereignis_line_edit.text().strip()
        if not event:
            self.application.logger.log(u'Kein Ereignis angegeben.', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        church = self.gemeinde_line_edit.text().strip()
        if not church:
            self.application.logger.log(u'Keine Gemeinde angegeben.', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        date = self.date_edit.date()
        if not date:
            self.application.logger.log(u'Fehlerhaftes Datum angegeben.', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return

        default_path = self.__load_path()
        if not os.path.isdir(os.path.dirname(default_path)):
            default_path = os.path.join(os.path.expanduser(u'~'), u'mp3tag.txt')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Datei für Mp3tag speichern',
                                                      default_path,
                                                      u'Textdateien (*.txt)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)
            self.gemeinde_completer.add_item(church)
            self.ereignis_completer.add_item(event)

            try:
                write_file = codecs.open(file_name, 'w', 'utf-8')
            except IOError as e:
                self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
            else:
                write_file.write(codecs.BOM_UTF8.decode('utf-8'))
                try:
                    write_file.write(self.__get_formated_text(indexes, event, church, date))
                except Exception as e:
                    self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                                __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
                write_file.close()

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.ID3TAGS.MP3TAG.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.ID3TAGS.MP3TAG.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


def convert_dateiname_to_id3dict(application, dateiname):
    id3dict = dict()

    # initialize
    id3dict[u'Title'] = u''
    id3dict[u'Interpret'] = u''
    id3dict[u'Genre'] = u''
    id3dict[u'Track'] = u''
    id3dict[u'Track'] = dateiname.counter

    # fill dict
    if dateiname.type == Global.DATEINAMEN.RB_TYPE.SONST:
        id3dict[u'Title'] = dateiname.type_other_text
        id3dict[u'Genre'] = u''
    elif dateiname.type == Global.DATEINAMEN.RB_TYPE.NICHTS:
        pass
    elif dateiname.type_definition is not None:
        if dateiname.type == Global.DATEINAMEN.RB_TYPE.ALLG_LIED:
            id3dict[u'Title'] = u'Allg. Lied {0} {1}'.format(dateiname.general_song,
                                                             application.main_window.dateinamen_model.
                                                             get_general_song_title(dateiname.general_song))
        elif dateiname.title == Global.DATEINAMEN.RB_TITEL.TITEL and dateiname.type_definition.id3_tag_overwrite_title:
            id3dict[u'Title'] = ''
        else:
            id3dict[u'Title'] = dateiname.type_definition.id3_tag_title

        if dateiname.name == Global.DATEINAMEN.RB_NAME.NAME and dateiname.type_definition.id3_tag_overwrite_interpret:
            id3dict[u'Interpret'] = u''
        else:
            id3dict[u'Interpret'] = dateiname.type_definition.id3_tag_interpret

        id3dict[u'Genre'] = dateiname.type_definition.id3_tag_genre

    if dateiname.title == Global.DATEINAMEN.RB_TITEL.TITEL:
        if id3dict[u'Title']:
            id3dict[u'Title'] += u' ' + dateiname.title_text
        else:
            id3dict[u'Title'] = dateiname.title_text
    if dateiname.scripture == Global.DATEINAMEN.RB_STELLE.STELLE:
        if id3dict[u'Title']:
            id3dict[u'Title'] += u' {0} {1}'.format(dateiname.scripture_book, dateiname.scripture_text)
        else:
            id3dict[u'Title'] = u'{0} {1}'.format(dateiname.scripture_book, dateiname.scripture_text)
    if dateiname.other == Global.DATEINAMEN.RB_SONST.SONST:
        if id3dict[u'Title']:
            id3dict[u'Title'] += u' {0}'.format(dateiname.other_text)
        else:
            id3dict[u'Title'] = dateiname.other_text
    if dateiname.name == Global.DATEINAMEN.RB_NAME.NAME:
        if id3dict[u'Interpret']:
            id3dict[u'Interpret'] += u' ' + dateiname.name_text
        else:
            id3dict[u'Interpret'] = dateiname.name_text

    if not id3dict[u'Genre']:
        id3dict[u'Genre'] = Global.ID3TAGS.GENRE_DEFAULT

    return id3dict
