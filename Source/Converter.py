# coding=utf-8

from PySide import QtGui

import Global
import MediaToolsLogger


class ConverterWidget(QtGui.QWidget):
    """
    Konvertiert CopyCenter Verzeichnisse für MediaDock
    """

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application
