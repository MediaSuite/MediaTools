# coding=utf-8

import os
import codecs
import json
import re
from PySide import QtGui
from PySide import QtWebKit
from PySide import QtCore
import urllib2
import threading
import time
import socket

import Global
import MediaToolsLogger
import DiagnosticModule


class LiedanzeigeDisplayWidget(QtGui.QWidget):
    set_html = QtCore.Signal(unicode)
    __load_started_signal = QtCore.Signal()
    __load_finished_signal = QtCore.Signal()

    def __init__(self, application, model):
        super(self.__class__, self).__init__()

        self.application = application
        self.model = model

        self.__thread = None
        self.__run = False

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        buttons_widget = QtGui.QWidget()
        buttons_layout = QtGui.QHBoxLayout()
        buttons_layout.setContentsMargins(0, 0, 0, 0)
        buttons_widget.setLayout(buttons_layout)

        start_button = QtGui.QPushButton(u'Start')
        self.connect(start_button, QtCore.SIGNAL("clicked()"), self.start)
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-media-control-3-240.png'))
        start_button.setIcon(icon)
        buttons_layout.addWidget(start_button)

        stop_button = QtGui.QPushButton(u'Stop')
        self.connect(stop_button, QtCore.SIGNAL("clicked()"), self.stop)
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-media-control-11-240.png'))
        stop_button.setIcon(icon)
        buttons_layout.addWidget(stop_button)

        self.running_icon = QtGui.QLabel()
        self.running_icon.setFixedHeight(18)
        self.running_icon.setFixedWidth(18)
        buttons_layout.addWidget(self.running_icon)

        label = QtGui.QLabel(u"Zoom:")
        buttons_layout.addWidget(label)

        self.spinbox = QtGui.QSpinBox()
        self.spinbox.setMinimum(Global.LIEDANZEIGE.DISPLAY.MIN_ZOOM)
        self.spinbox.setMaximum(Global.LIEDANZEIGE.DISPLAY.MAX_ZOOM)
        self.spinbox.setValue(100)
        buttons_layout.addWidget(self.spinbox)

        label = QtGui.QLabel(u"Hostname:")
        buttons_layout.addWidget(label)

        self.liedanzeige_line_edit = QtGui.QLineEdit()
        self.liedanzeige_line_edit.setText(self.model.hostname)
        # noinspection PyUnresolvedReferences
        self.liedanzeige_line_edit.textChanged.connect(self.__set_hostname)
        buttons_layout.addWidget(self.liedanzeige_line_edit)

        layout.addWidget(buttons_widget)

        self.liedanzeige_web_view = QtWebKit.QWebView()
        # noinspection PyUnresolvedReferences
        self.liedanzeige_web_view.loadStarted.connect(self.__load_started)
        # noinspection PyUnresolvedReferences
        self.liedanzeige_web_view.loadFinished.connect(self.__load_finished)
        # noinspection PyUnresolvedReferences
        self.set_html.connect(self.__set_html)
        # noinspection PyUnresolvedReferences
        self.spinbox.valueChanged[unicode].connect(self.__set_zoom)
        self.spinbox.setValue(self.model.zoom)
        layout.addWidget(self.liedanzeige_web_view)

        self.__load_started_signal.connect(self.__load_started)
        self.__load_finished_signal.connect(self.__load_finished)

    def __del__(self):
        self.__run = False

    @QtCore.Slot(unicode)
    def __set_html(self, html):
        self.liedanzeige_web_view.setHtml(html)

    @QtCore.Slot()
    def __load_started(self):
        if not self.__run:
            self.liedanzeige_web_view.stop()
        else:
            icon = QtGui.QMovie(Global.LIEDANZEIGE.DISPLAY.WAIT_ICON_PATH)
            self.running_icon.setMovie(icon)
            icon.start()

    @QtCore.Slot()
    def __load_finished(self):
        self.running_icon.setMovie(QtGui.QMovie())

    def __set_zoom(self):
        self.model.zoom = self.spinbox.value()
        self.liedanzeige_web_view.setZoomFactor(self.spinbox.value() / 100.0)

    def __set_hostname(self):
        self.model.hostname = self.liedanzeige_line_edit.text()

    def __check_host(self, host):
        ok = False
        message = u'Ein unbekannter Fehler ist beim Verbinden mit der Liedanzeige aufgetreten.'
        self.__load_started_signal.emit()
        try:
            answer = urllib2.urlopen(host + '/erstetelefonnachricht', timeout=Global.LIEDANZEIGE.DISPLAY.TIMEOUT)
        except urllib2.HTTPError as error:
            message = u'HTTP-Fehler beim Abfragen der Liedanzeige ist aufgetreten: %s' % error.code
        except urllib2.URLError:
            message = u'Fehler in der URL.'
        except socket.timeout:
            message = u'Zeitüberschreitung beim Verbinden mit der Liedanzeige.'
        except Exception:
            pass
        else:
            if answer.read() == 'daspferdfrisstkeinengurkensalat':
                ok = True
            else:
                u"Host wurde nicht als Liedanzeige erkannt."
        finally:
            self.__load_finished_signal.emit()
        if not ok:
            self.application.logger.log(message, __name__, MediaToolsLogger.SEVERITY_WARNING,
                                        dialog=True, status_bar=True)
        return ok

    def __update(self, url):
        self.__load_started_signal.emit()
        answer = urllib2.urlopen(url, timeout=Global.LIEDANZEIGE.DISPLAY.TIMEOUT)
        html = answer.read().decode('utf-8')
        html = re.sub(r'<meta http-equiv="refresh" content=".*">', '', html)
        self.set_html.emit(html)
        self.__load_finished_signal.emit()

    @DiagnosticModule.execute_with_exception_handling
    def __loop(self):
        self.__run = True
        self.application.logger.log(u'Verbindungsaufbau mit Liedanzeige...', __name__, status_bar=True)
        host = self.liedanzeige_line_edit.text()
        url = "http://" + host + ":" + str(Global.LIEDANZEIGE.DISPLAY.SERVER_PORT)
        if not self.__check_host(url):
            self.__run = False
            return
        self.application.logger.log(u'Verbindung mit Liedanzeige aufgebaut.', __name__, status_bar=True)
        counter = 0
        error_count = 0
        while self.__run:
            if not counter:
                try:
                    self.__update(url)
                except Exception as e:
                    self.application.logger.log(u'Error occurred in request of %s: %s' % (url, e), __name__,
                                                severity=MediaToolsLogger.SEVERITY_ERROR)
                    error_count += 1
                else:
                    error_count = 0
            if error_count >= 3:
                self.application.logger.log(u'Verbindung mit Liedanzeige gestört.', __name__,
                                            severity=MediaToolsLogger.SEVERITY_ERROR, dialog=True, status_bar=True)
                break
            counter += 1
            counter %= 30
            time.sleep(0.1)
        # time.sleep(0.1)
        # self.set_html.emit("")
        # self.set_html.emit("")
        self.set_html.emit("")
        # self.liedanzeige_web_view.setHtml('')
        # time.sleep(0.1)
        self.__run = False
        self.application.logger.log(u'Verbindung mit Liedanzeige abgebaut.', __name__, status_bar=True)

    def is_running(self):
        """Gibt zurück, ob die Updateschleife läuft.

        :return: Bool Wahr, wenn die Updateschleife läuft
        """
        if self.__thread is None:
            return False
        else:
            return self.__thread.is_alive()

    def start(self):
        """Startet die Updateschleife wenn sie noch nicht läuft."""
        if not self.is_running():
            self.__thread = threading.Thread(target=self.__loop, args=(), name='Liedanzeige')
            self.__thread.daemon = True
            self.__thread.start()

    def stop(self):
        """Beendet die Updateschleife."""
        self.__run = False
        self.liedanzeige_web_view.stop()


class LiedanzeigeDisplayModel(object):
    def __init__(self, application):
        self.application = application
        self.__loaded = False
        if os.path.exists(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH):
            self.__change_time = os.path.getmtime(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH)
        else:
            self.__change_time = 0
        self.__settings = Global.LIEDANZEIGE.DISPLAY.DEFAULT_SETTINGS.copy()
        self.__load()

    def __load(self):
        """Einstellungen aus Datei laden."""
        write = False
        checked_settings = dict()
        if not os.path.exists(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH):
            self.__write()
        try:
            settings_file = codecs.open(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH, 'r', 'utf-8')
        except IOError:
            self.__loaded = False
            self.application.logger.log("Error occurred reading general settings file.", __name__,
                                        MediaToolsLogger.SEVERITY_ERROR)
        else:
            unchecked_settings = Global.LIEDANZEIGE.DISPLAY.DEFAULT_SETTINGS.copy()
            try:
                unchecked_settings = json.loads(settings_file.read())
            except ValueError:
                self.application.logger.log("Error occurred decoding general settings file.", __name__,
                                            MediaToolsLogger.SEVERITY_ERROR)
                unchecked_settings = self.__settings.copy()
                write = True
            finally:
                if not isinstance(unchecked_settings, dict):
                    unchecked_settings = dict()
                    write = True

                # zoom
                if 'zoom' in unchecked_settings:
                    checked_value = self.__check_zoom(unchecked_settings['zoom'])
                    if checked_value != unchecked_settings['zoom']:
                        write = True
                    checked_settings['zoom'] = checked_value
                else:
                    checked_settings['zoom'] = Global.LIEDANZEIGE.DISPLAY.DEFAULT_SETTINGS['zoom']
                    write = True

                # hostname
                if 'hostname' in unchecked_settings:
                    checked_value = self.__check_hostname(unchecked_settings['hostname'])
                    if checked_value != unchecked_settings['hostname']:
                        write = True
                    checked_settings['hostname'] = checked_value
                else:
                    checked_settings['hostname'] = Global.LIEDANZEIGE.DISPLAY.DEFAULT_SETTINGS['hostname']
                    write = True

                self.__settings = checked_settings
                self.__loaded = True

        if write:
            self.__write()

    def __write(self):
        """Einstellungen in Datei schreiben."""
        try:
            settings_file = codecs.open(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH, 'w', 'utf-8')
        except IOError:
            self.application.logger.log("Error occurred writing general settings file.", __name__,
                                        MediaToolsLogger.SEVERITY_ERROR)
        else:
            settings_file.write(json.dumps(self.__settings))
            settings_file.close()
            self.__change_time = os.path.getmtime(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH)

    def __check_reload(self):
        """Prüft ob die Einstellungsdatei sich geändert hat und neu geladen werden muss."""
        if not os.path.exists(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH):
            return True
        return not self.__loaded or self.__change_time != \
                                    os.path.getmtime(Global.LIEDANZEIGE.DISPLAY.SETTINGS_FILE_PATH)

    def load_defaults(self):
        """Setzt alles auf Defaulteinstellungen zurück."""
        self.__settings = Global.LIEDANZEIGE.DISPLAY.DEFAULT_SETTINGS.copy()
        self.__write()

    @property
    def zoom(self):
        if self.__check_reload():
            self.__load()
        return self.__settings['zoom']

    @zoom.setter
    def zoom(self, value):
        checked_value = self.__check_zoom(value)
        if checked_value == self.__settings['zoom']:
            return
        else:
            self.__settings['zoom'] = checked_value
            self.__write()

    def __check_zoom(self, value):
        try:
            int_value = float(value)
        except ValueError:
            return self.__settings['zoom']
        else:
            int_value = max(int_value, Global.LIEDANZEIGE.DISPLAY.MIN_ZOOM)
            int_value = min(int_value, Global.LIEDANZEIGE.DISPLAY.MAX_ZOOM)
            return int_value

    @property
    def hostname(self):
        if self.__check_reload():
            self.__load()
        return self.__settings['hostname']

    @hostname.setter
    def hostname(self, value):
        checked_value = self.__check_hostname(value)
        if checked_value == self.__settings['hostname']:
            return
        else:
            self.__settings['hostname'] = checked_value
            self.__write()

    def __check_hostname(self, value):
        if not isinstance(value, (str, unicode)):
            value = self.__settings['hostname']
        return unicode(value)
