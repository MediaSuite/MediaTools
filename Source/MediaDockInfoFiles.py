# coding=utf-8

import codecs
import datetime
import linecache
import xml.etree.ElementTree as ElementTree
from PySide import QtGui
from PySide import QtCore
import os

import Global
import MediaToolsLogger
import MtWidgets


def indent_xml(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent_xml(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i


class RecordInfoWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(8, 8, 8, 8)
        self.setLayout(layout)

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        widget_1_1 = QtGui.QWidget()
        layout_1.addWidget(widget_1_1)
        layout_1_1 = QtGui.QVBoxLayout()
        layout_1_1.setContentsMargins(0, 0, 0, 0)
        widget_1_1.setLayout(layout_1_1)

        widget_1_1_1 = QtGui.QWidget()
        layout_1_1.addWidget(widget_1_1_1)
        layout_1_1_1 = QtGui.QHBoxLayout()
        layout_1_1_1.setContentsMargins(0, 0, 0, 0)
        widget_1_1_1.setLayout(layout_1_1_1)

        layout_1_1_1.addWidget(QtGui.QLabel(u'Datum der Aufnahme:'), 1)

        today_button = QtGui.QPushButton(u'Heute')
        today_button.setToolTip(u'Heute anzeigen')
        # noinspection PyUnresolvedReferences
        today_button.clicked.connect(self.__show_today)
        layout_1_1_1.addWidget(today_button)

        self.calendar_widget = QtGui.QCalendarWidget()
        self.calendar_widget.setFirstDayOfWeek(QtCore.Qt.DayOfWeek(1))
        layout_1_1.addWidget(self.calendar_widget)

        layout_1.addWidget(QtGui.QWidget(), 1)

        # Anzeigereihenfolge
        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        widget_2_1 = QtGui.QWidget()
        layout_2.addWidget(widget_2_1)
        layout_2_1 = QtGui.QVBoxLayout()
        layout_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_1.setLayout(layout_2_1)

        label = QtGui.QLabel(u'Anzeigereihenfolge:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Ereignis:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Gemeinde:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Kommentar:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)

        widget_2_2 = QtGui.QWidget()
        layout_2.addWidget(widget_2_2)
        layout_2_2 = QtGui.QVBoxLayout()
        layout_2_2.setContentsMargins(0, 0, 0, 0)
        widget_2_2.setLayout(layout_2_2)

        widget_2_2_1 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_1)
        layout_2_2_1 = QtGui.QHBoxLayout()
        layout_2_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_2_1.setLayout(layout_2_2_1)

        self.reihenfolge_spin_box = QtGui.QSpinBox()
        self.reihenfolge_spin_box.setMaximum(Global.MEDIADOCKINFOFILES.RECORD_INFO.ORDER_MAX)
        self.reihenfolge_spin_box.setMinimum(Global.MEDIADOCKINFOFILES.RECORD_INFO.ORDER_MIN)
        layout_2_2_1.addWidget(self.reihenfolge_spin_box)

        layout_2_2_1.addWidget(QtGui.QLabel(u'Höhere Zahlen werden weiter oben angezeigt'), 1)

        # Ereignis
        self.ereignis_line_edit = QtGui.QLineEdit()
        self.ereignis_completer = MtWidgets.FileCompleter(self.application, Global.MEDIADOCKINFOFILES.FILES.EVENTS)
        self.ereignis_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.ereignis_line_edit.setCompleter(self.ereignis_completer)
        layout_2_2.addWidget(self.ereignis_line_edit)
        linecache.checkcache(Global.MEDIADOCKINFOFILES.FILES.EVENTS)
        first_line = linecache.getline(Global.MEDIADOCKINFOFILES.FILES.EVENTS, 1).decode('utf-8').strip('\n\r')
        self.ereignis_line_edit.setText(first_line)
        # Gemeinde
        self.gemeinde_line_edit = QtGui.QLineEdit()
        self.gemeinde_completer = MtWidgets.FileCompleter(self.application, Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        self.gemeinde_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.gemeinde_line_edit.setCompleter(self.gemeinde_completer)
        layout_2_2.addWidget(self.gemeinde_line_edit)
        linecache.checkcache(Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        first_line = linecache.getline(Global.MEDIADOCKINFOFILES.FILES.CHURCHES, 1).decode('utf-8').strip('\n\r')
        self.gemeinde_line_edit.setText(first_line)
        # Kommentar
        self.kommentar_line_edit = QtGui.QLineEdit()
        layout_2_2.addWidget(self.kommentar_line_edit)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_0 = QtGui.QWidget()
        layout.addWidget(widget_0)
        layout_0 = QtGui.QHBoxLayout()
        layout_0.setContentsMargins(0, 0, 0, 0)
        widget_0.setLayout(layout_0)

        speichere_datei_button = QtGui.QPushButton(u'Datei speichern')
        speichere_datei_button.setToolTip(u'Aufnahme speichern')
        # noinspection PyUnresolvedReferences
        speichere_datei_button.clicked.connect(self.save_file)
        layout_0.addWidget(speichere_datei_button)

        oeffne_datei_button = QtGui.QPushButton(u'Datei öffnen')
        oeffne_datei_button.setToolTip(u'Aufnahme öffnen')
        # noinspection PyUnresolvedReferences
        oeffne_datei_button.clicked.connect(self.load_file)
        layout_0.addWidget(oeffne_datei_button)

        layout_0.addWidget(QtGui.QWidget(), 1)

        layout.addWidget(QtGui.QWidget(), 1)

    def __show_today(self):
        now = datetime.datetime.now()
        self.calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))

    def load_file(self):
        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'record_info.xml')

        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window, u'Aufnahme öffnen',
                                                      default_path,
                                                      u'XML-Dateien (*.xml)')
        file_name = file_name[0]

        if not file_name:
            return

        if not os.path.isfile(file_name):
            self.application.logger.log(u'Datei konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            self.__save_path(file_name)

            try:
                tree = ElementTree.parse(file_name)
            except IOError:
                self.application.logger.log(u'Datei konnte nicht geöffnet werden', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            except ElementTree.ParseError:
                self.application.logger.log(u'Datei ist keine gültige XML-Datei', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            root = tree.getroot()

            version_tag = root.find('version')
            try:
                version = int(version_tag.text)
            except ValueError:
                version = 2
            except AttributeError:
                version = 2

            now = datetime.datetime.now()
            day_tag = root.find('day')
            month_tag = root.find('month')
            year_tag = root.find('year')
            use_today = False

            try:
                day = int(day_tag.text)
            except ValueError:
                day = now.day
                use_today = True
            try:
                month = int(month_tag.text)
            except ValueError:
                month = now.month
                use_today = True
            try:
                year = int(year_tag.text)
            except ValueError:
                year = now.year
                use_today = True

            if use_today:
                self.calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))
                self.application.logger.log(u'Datum konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)
            else:
                try:
                    datetime.datetime(day=day, month=month, year=year)
                    self.calendar_widget.setSelectedDate(QtCore.QDate(year, month, day))
                except ValueError:
                    self.calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))
                    self.application.logger.log(u'Datum konnte nicht aus Datei gelesen werden', __name__,
                                                MediaToolsLogger.SEVERITY_WARNING, True, True)

            event_tag = root.find('event')
            if event_tag is not None:
                if event_tag.text is not None:
                    self.ereignis_line_edit.setText(event_tag.text.strip())
                else:
                    self.ereignis_line_edit.setText(u'')
            else:
                self.ereignis_line_edit.setText(u'')
                self.application.logger.log(u'Ereignis konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

            church_tag = root.find('church')
            if church_tag is not None:
                if church_tag.text is not None:
                    self.gemeinde_line_edit.setText(church_tag.text.strip())
                else:
                    self.gemeinde_line_edit.setText(u'')
            else:
                self.gemeinde_line_edit.setText(u'')
                self.application.logger.log(u'Gemeinde konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

            info_tag = root.find('info')
            if info_tag is not None:
                if info_tag.text is not None:
                    self.kommentar_line_edit.setText(info_tag.text.strip())
                else:
                    self.kommentar_line_edit.setText(u'')
            else:
                self.kommentar_line_edit.setText(u'')
                self.application.logger.log(u'Kommentar konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

            if version >= 1:
                order_tag = root.find('order')
                try:
                    order = int(order_tag.text)
                except ValueError:
                    order = 0
                self.reihenfolge_spin_box.setValue(order)
            else:
                self.reihenfolge_spin_box.setValue(0)

    def save_file(self):
        order = self.reihenfolge_spin_box.value()
        if order == 0:
            question = u'Anzeigereihenfolge ist auf Standardwert "0". Ist das richtig?'
            message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                            u'Frage',
                                            question,
                                            QtGui.QMessageBox.NoButton,
                                            self.application.main_window)
            message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
            message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)

            response = message_box.exec_()
            if response != 0:  # not yes
                return

        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'record_info.xml')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Aufnahme speichern',
                                                      default_path,
                                                      u'XML-Dateien (*.xml)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if os.path.basename(file_name) != u'record_info.xml':
            self.application.logger.log(u'Dateiname muss "record_info.xml" sein', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        elif not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)

            data = ElementTree.Element('data')

            version = '2'
            version_tag = ElementTree.SubElement(data, 'version')
            version_tag.text = version

            date = self.calendar_widget.selectedDate()
            year = str(date.year())
            month = str(date.month())
            day = str(date.day())

            day_tag = ElementTree.SubElement(data, 'day')
            day_tag.text = day

            month_tag = ElementTree.SubElement(data, 'month')
            month_tag.text = month

            year_tag = ElementTree.SubElement(data, 'year')
            year_tag.text = year

            event_tag = ElementTree.SubElement(data, 'event')
            event = self.ereignis_line_edit.text()
            event_tag.text = event
            self.ereignis_completer.add_item(event)

            church_tag = ElementTree.SubElement(data, 'church')
            church = self.gemeinde_line_edit.text()
            church_tag.text = church
            self.gemeinde_completer.add_item(church)

            info_tag = ElementTree.SubElement(data, 'info')
            info = self.kommentar_line_edit.text()
            info_tag.text = info

            order_tag = ElementTree.SubElement(data, 'order')
            order_tag.text = unicode(order)

            automatic_tag = ElementTree.SubElement(data, 'automatic')
            automatic_tag.text = u'False'

            indent_xml(data)
            xml = ElementTree.ElementTree(data)
            xml.write(file_name, xml_declaration=True, encoding='UTF-8')

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.MEDIADOCKINFOFILES.RECORD_INFO.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.MEDIADOCKINFOFILES.RECORD_INFO.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


class CollectionInfoWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(8, 8, 8, 8)
        self.setLayout(layout)

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        widget_1_1 = QtGui.QWidget()
        layout_1.addWidget(widget_1_1)
        layout_1_1 = QtGui.QVBoxLayout()
        layout_1_1.setContentsMargins(0, 0, 0, 0)
        widget_1_1.setLayout(layout_1_1)

        widget_1_1_1 = QtGui.QWidget()
        layout_1_1.addWidget(widget_1_1_1)
        layout_1_1_1 = QtGui.QHBoxLayout()
        layout_1_1_1.setContentsMargins(0, 0, 0, 0)
        widget_1_1_1.setLayout(layout_1_1_1)

        self.use_start_date_check_box = QtGui.QCheckBox(u'Startdatum verwenden')
        self.use_start_date_check_box.setChecked(True)
        # noinspection PyUnresolvedReferences
        self.use_start_date_check_box.stateChanged.connect(self.use_start_date_check_box_state_changed)
        layout_1_1_1.addWidget(self.use_start_date_check_box)

        layout_1_1_1.addWidget(QtGui.QWidget(), 1)

        today_button = QtGui.QPushButton(u'Heute')
        today_button.setToolTip(u'Heute anzeigen')
        # noinspection PyUnresolvedReferences
        today_button.clicked.connect(self.__show_today_start)
        layout_1_1_1.addWidget(today_button)

        self.start_calendar_widget = QtGui.QCalendarWidget()
        self.start_calendar_widget.setFirstDayOfWeek(QtCore.Qt.DayOfWeek(1))
        layout_1_1.addWidget(self.start_calendar_widget)

        widget_1_2 = QtGui.QWidget()
        layout_1.addWidget(widget_1_2)
        layout_1_2 = QtGui.QVBoxLayout()
        layout_1_2.setContentsMargins(0, 0, 0, 0)
        widget_1_2.setLayout(layout_1_2)

        widget_1_2_1 = QtGui.QWidget()
        layout_1_2.addWidget(widget_1_2_1)
        layout_1_2_1 = QtGui.QHBoxLayout()
        layout_1_2_1.setContentsMargins(0, 0, 0, 0)
        widget_1_2_1.setLayout(layout_1_2_1)

        self.use_end_date_check_box = QtGui.QCheckBox(u'Enddatum verwenden')
        self.use_end_date_check_box.setChecked(False)
        # noinspection PyUnresolvedReferences
        self.use_end_date_check_box.stateChanged.connect(self.use_end_date_check_box_state_changed)
        layout_1_2_1.addWidget(self.use_end_date_check_box)

        layout_1_2_1.addWidget(QtGui.QWidget(), 1)

        today_button = QtGui.QPushButton(u'Heute')
        today_button.setToolTip(u'Heute anzeigen')
        # noinspection PyUnresolvedReferences
        today_button.clicked.connect(self.__show_today_end)
        layout_1_2_1.addWidget(today_button)

        self.end_calendar_widget = QtGui.QCalendarWidget()
        self.end_calendar_widget.setFirstDayOfWeek(QtCore.Qt.DayOfWeek(1))
        self.end_calendar_widget.setEnabled(False)
        layout_1_2.addWidget(self.end_calendar_widget)

        layout_1.addWidget(QtGui.QWidget(), 1)

        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        widget_2_1 = QtGui.QWidget()
        layout_2.addWidget(widget_2_1)
        layout_2_1 = QtGui.QVBoxLayout()
        layout_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_1.setLayout(layout_2_1)

        label = QtGui.QLabel(u'Anzeigereihenfolge:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Name:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Gemeinde:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Kommentar:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)

        widget_2_2 = QtGui.QWidget()
        layout_2.addWidget(widget_2_2)
        layout_2_2 = QtGui.QVBoxLayout()
        layout_2_2.setContentsMargins(0, 0, 0, 0)
        widget_2_2.setLayout(layout_2_2)

        widget_2_2_1 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_1)
        layout_2_2_1 = QtGui.QHBoxLayout()
        layout_2_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_2_1.setLayout(layout_2_2_1)

        self.reihenfolge_spin_box = QtGui.QSpinBox()
        self.reihenfolge_spin_box.setMaximum(Global.MEDIADOCKINFOFILES.COLLECTION_INFO.ORDER_MAX)
        self.reihenfolge_spin_box.setMinimum(Global.MEDIADOCKINFOFILES.COLLECTION_INFO.ORDER_MIN)
        layout_2_2_1.addWidget(self.reihenfolge_spin_box)

        layout_2_2_1.addWidget(QtGui.QLabel(u'Höhere Zahlen werden weiter oben angezeigt'), 1)

        # Name der Sammlung
        self.sammlungs_line_edit = QtGui.QLineEdit()
        self.sammlungs_completer = MtWidgets.FileCompleter(self.application,
                                                           Global.MEDIADOCKINFOFILES.FILES.COLLECTIONS)
        self.sammlungs_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.sammlungs_line_edit.setCompleter(self.sammlungs_completer)
        layout_2_2.addWidget(self.sammlungs_line_edit)
        linecache.checkcache(Global.MEDIADOCKINFOFILES.FILES.COLLECTIONS)
        first_line = linecache.getline(Global.MEDIADOCKINFOFILES.FILES.COLLECTIONS, 1).decode('utf-8').strip('\n\r')
        self.sammlungs_line_edit.setText(first_line)
        # Gemeinde
        self.gemeinde_line_edit = QtGui.QLineEdit()
        self.gemeinde_completer = MtWidgets.FileCompleter(self.application, Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        self.gemeinde_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.gemeinde_line_edit.setCompleter(self.gemeinde_completer)
        layout_2_2.addWidget(self.gemeinde_line_edit)
        linecache.checkcache(Global.MEDIADOCKINFOFILES.FILES.CHURCHES)
        first_line = linecache.getline(Global.MEDIADOCKINFOFILES.FILES.CHURCHES, 1).decode('utf-8').strip('\n\r')
        self.gemeinde_line_edit.setText(first_line)
        # Kommentar
        self.kommentar_line_edit = QtGui.QLineEdit()
        layout_2_2.addWidget(self.kommentar_line_edit)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_0 = QtGui.QWidget()
        layout.addWidget(widget_0)
        layout_0 = QtGui.QHBoxLayout()
        layout_0.setContentsMargins(0, 0, 0, 0)
        widget_0.setLayout(layout_0)

        speichere_datei_button = QtGui.QPushButton(u'Datei speichern')
        speichere_datei_button.setToolTip(u'Sammlung speichern')
        # noinspection PyUnresolvedReferences
        speichere_datei_button.clicked.connect(self.save_file)
        layout_0.addWidget(speichere_datei_button)

        oeffne_datei_button = QtGui.QPushButton(u'Datei öffnen')
        oeffne_datei_button.setToolTip(u'Sammlung öffnen')
        # noinspection PyUnresolvedReferences
        oeffne_datei_button.clicked.connect(self.load_file)
        layout_0.addWidget(oeffne_datei_button)

        layout_0.addWidget(QtGui.QWidget(), 1)

        layout.addWidget(QtGui.QWidget(), 1)

    def use_start_date_check_box_state_changed(self, value):
        if not value:
            self.use_end_date_check_box.setChecked(False)
            self.start_calendar_widget.setDisabled(True)
        else:
            self.start_calendar_widget.setEnabled(True)

    def use_end_date_check_box_state_changed(self, value):
        if not value:
            self.end_calendar_widget.setDisabled(True)
        else:
            self.use_start_date_check_box.setChecked(True)
            self.end_calendar_widget.setEnabled(True)

    def __show_today_start(self):
        now = datetime.datetime.now()
        self.start_calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))

    def __show_today_end(self):
        now = datetime.datetime.now()
        self.end_calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))

    @staticmethod
    def __get_date_from_etree(root, tag_name):
        tag = root.find(tag_name)
        try:
            return datetime.datetime.strptime(tag.text, '%d.%m.%Y')
        except (ValueError, AttributeError):
            return None

    def load_file(self):
        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'collection_info.xml')

        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window, u'Sammlung öffnen',
                                                      default_path,
                                                      u'XML-Dateien (*.xml)')
        file_name = file_name[0]
        if not file_name:
            return
        if not os.path.isfile(file_name):
            self.application.logger.log(u'Datei konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            self.__save_path(file_name)
            try:
                tree = ElementTree.parse(file_name)
            except IOError:
                self.application.logger.log(u'Datei konnte nicht geöffnet werden', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            except ElementTree.ParseError:
                self.application.logger.log(u'Datei ist keine gültige XML-Datei', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            root = tree.getroot()

            version_tag = root.find('version')
            try:
                version = int(version_tag.text)
            except (ValueError, AttributeError):
                version = 0

            now = datetime.datetime.now()
            start_date = self.__get_date_from_etree(root, 'startdate')
            end_date = self.__get_date_from_etree(root, 'enddate')

            if start_date:
                self.use_start_date_check_box.setChecked(True)
                self.start_calendar_widget.setEnabled(True)
                self.start_calendar_widget.setSelectedDate(QtCore.QDate(start_date.year,
                                                                        start_date.month,
                                                                        start_date.day))
            else:
                self.use_start_date_check_box.setChecked(False)
                self.start_calendar_widget.setEnabled(False)
                self.start_calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))

            if end_date:
                self.end_calendar_widget.setSelectedDate(QtCore.QDate(end_date.year, end_date.month, end_date.day))
            else:
                self.end_calendar_widget.setSelectedDate(QtCore.QDate(now.year, now.month, now.day))

            if start_date and end_date:
                self.use_end_date_check_box.setChecked(True)
                self.end_calendar_widget.setEnabled(True)
            else:
                self.use_end_date_check_box.setChecked(False)
                self.end_calendar_widget.setEnabled(False)

            order_tag = root.find('order')
            try:
                order = int(order_tag.text)
            except (ValueError, AttributeError):
                self.reihenfolge_spin_box.setValue(0)
                self.application.logger.log(u'Reihenfolge konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)
            else:
                self.reihenfolge_spin_box.setValue(order)

            name_tag = root.find('name')
            if name_tag is not None:
                if name_tag.text is not None:
                    self.sammlungs_line_edit.setText(name_tag.text.strip())
                else:
                    self.sammlungs_line_edit.setText(u'')
            else:
                self.sammlungs_line_edit.setText(u'')
                self.application.logger.log(u'Name konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

            church_tag = root.find('church')
            if church_tag is not None:
                if church_tag.text is not None:
                    self.gemeinde_line_edit.setText(church_tag.text.strip())
                else:
                    self.gemeinde_line_edit.setText(u'')
            else:
                self.gemeinde_line_edit.setText(u'')
                self.application.logger.log(u'Gemeinde konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

            info_tag = root.find('info')
            if info_tag is not None:
                if info_tag.text is not None:
                    self.kommentar_line_edit.setText(info_tag.text.strip())
                else:
                    self.kommentar_line_edit.setText(u'')
            else:
                self.kommentar_line_edit.setText(u'')
                self.application.logger.log(u'Kommentar konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

    def save_file(self):
        order = self.reihenfolge_spin_box.value()

        start_date = self.start_calendar_widget.selectedDate()
        end_date = self.end_calendar_widget.selectedDate()
        if self.use_start_date_check_box.isChecked() and self.use_end_date_check_box.isChecked() and \
           end_date < start_date:
            self.application.logger.log(u'Enddatum liegt vor dem Startdatum.', __name__,
                                        MediaToolsLogger.SEVERITY_WARNING, True, True)
            return

        if order == 0:
            question = u'Anzeigereihenfolge ist auf Standardwert "0". Ist das richtig?'
            message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                            u'Frage',
                                            question,
                                            QtGui.QMessageBox.NoButton,
                                            self.application.main_window)
            message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
            message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)

            response = message_box.exec_()
            if response != 0:  # not yes
                return

        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'collection_info.xml')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Sammlung speichern',
                                                      default_path,
                                                      u'XML-Dateien (*.xml)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if os.path.basename(file_name) != u'collection_info.xml':
            self.application.logger.log(u'Dateiname muss "collection_info.xml" sein', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        elif not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)

            data = ElementTree.Element('data')

            version = '0'
            version_tag = ElementTree.SubElement(data, 'version')
            version_tag.text = version

            name_tag = ElementTree.SubElement(data, 'name')
            name = self.sammlungs_line_edit .text()
            name_tag.text = name
            self.sammlungs_completer.add_item(name)

            # if start date selected...
            if self.use_start_date_check_box.isChecked():
                start_date_str = '%02i.%02i.%04i' % (start_date.day(), start_date.month(), start_date.year())
                start_date_tag = ElementTree.SubElement(data, 'startdate')
                start_date_tag.text = start_date_str

                if self.use_end_date_check_box.isChecked():
                    end_date_str = '%02i.%02i.%04i' % (end_date.day(), end_date.month(), end_date.year())
                    end_date_tag = ElementTree.SubElement(data, 'enddate')
                    end_date_tag.text = end_date_str

            church_tag = ElementTree.SubElement(data, 'church')
            church = self.gemeinde_line_edit.text()
            church_tag.text = church
            self.gemeinde_completer.add_item(church)

            info_tag = ElementTree.SubElement(data, 'info')
            info = self.kommentar_line_edit.text()
            info_tag.text = info

            order_tag = ElementTree.SubElement(data, 'order')
            order_tag.text = unicode(order)

            automatic_tag = ElementTree.SubElement(data, 'automatic')
            automatic_tag.text = u'False'

            indent_xml(data)
            xml = ElementTree.ElementTree(data)
            xml.write(file_name, xml_declaration=True, encoding='UTF-8')

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.MEDIADOCKINFOFILES.COLLECTION_INFO.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.MEDIADOCKINFOFILES.COLLECTION_INFO.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


class CategoryInfoWidget(QtGui.QWidget):

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(8, 8, 8, 8)
        self.setLayout(layout)

        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        widget_2_1 = QtGui.QWidget()
        layout_2.addWidget(widget_2_1)
        layout_2_1 = QtGui.QVBoxLayout()
        layout_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_1.setLayout(layout_2_1)

        label = QtGui.QLabel(u'Anzeigereihenfolge:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Name:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)

        widget_2_2 = QtGui.QWidget()
        layout_2.addWidget(widget_2_2)
        layout_2_2 = QtGui.QVBoxLayout()
        layout_2_2.setContentsMargins(0, 0, 0, 0)
        widget_2_2.setLayout(layout_2_2)

        widget_2_2_1 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_1)
        layout_2_2_1 = QtGui.QHBoxLayout()
        layout_2_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_2_1.setLayout(layout_2_2_1)

        self.reihenfolge_spin_box = QtGui.QSpinBox()
        self.reihenfolge_spin_box.setMaximum(Global.MEDIADOCKINFOFILES.CATEGORY_INFO.ORDER_MAX)
        self.reihenfolge_spin_box.setMinimum(Global.MEDIADOCKINFOFILES.CATEGORY_INFO.ORDER_MIN)
        layout_2_2_1.addWidget(self.reihenfolge_spin_box)

        layout_2_2_1.addWidget(QtGui.QLabel(u'Höhere Zahlen werden weiter oben angezeigt'), 1)

        self.name_completer = MtWidgets.FileCompleter(self.application, Global.MEDIADOCKINFOFILES.FILES.CATEGORIES)
        self.name_completer.set_suggestion_policy(MtWidgets.SUGGESTION_POLICY.BEGIN)
        self.name_line_edit = QtGui.QLineEdit()
        self.name_line_edit.setCompleter(self.name_completer)
        layout_2_2.addWidget(self.name_line_edit)
        linecache.checkcache(Global.MEDIADOCKINFOFILES.FILES.CATEGORIES)
        first_line = linecache.getline(Global.MEDIADOCKINFOFILES.FILES.CATEGORIES, 1).decode('utf-8').strip('\n\r')
        self.name_line_edit.setText(first_line)

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_0 = QtGui.QWidget()
        layout.addWidget(widget_0)
        layout_0 = QtGui.QHBoxLayout()
        layout_0.setContentsMargins(0, 0, 0, 0)
        widget_0.setLayout(layout_0)

        speichere_datei_button = QtGui.QPushButton(u'Datei speichern')
        speichere_datei_button.setToolTip(u'Kategorie speichern')
        # noinspection PyUnresolvedReferences
        speichere_datei_button.clicked.connect(self.save_file)
        layout_0.addWidget(speichere_datei_button)

        oeffne_datei_button = QtGui.QPushButton(u'Datei öffnen')
        oeffne_datei_button.setToolTip(u'Kategorie öffnen')
        # noinspection PyUnresolvedReferences
        oeffne_datei_button.clicked.connect(self.load_file)
        layout_0.addWidget(oeffne_datei_button)

        layout_0.addWidget(QtGui.QWidget(), 1)

        layout.addWidget(QtGui.QWidget(), 1)

    def load_file(self):
        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'category_info.xml')

        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window, u'Kategorie öffnen',
                                                      default_path,
                                                      u'XML-Dateien (*.xml)')
        file_name = file_name[0]
        if not file_name:
            return
        if not os.path.isfile(file_name):
            self.application.logger.log(u'Datei konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            self.__save_path(file_name)
            try:
                tree = ElementTree.parse(file_name)
            except IOError:
                self.application.logger.log(u'Datei konnte nicht geöffnet werden', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            except ElementTree.ParseError:
                self.application.logger.log(u'Datei ist keine gültige XML-Datei', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            root = tree.getroot()

            name_tag = root.find('name')
            if name_tag is not None:
                if name_tag.text is not None:
                    self.name_line_edit.setText(name_tag.text.strip())
                else:
                    self.name_line_edit.setText(u'')
            else:
                self.name_line_edit.setText(u'')
                self.application.logger.log(u'Kategorie konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

            order_tag = root.find('order')
            try:
                order = int(order_tag.text)
            except ValueError:
                self.reihenfolge_spin_box.setValue(0)
                self.application.logger.log(u'Reihenfolge konnte nicht aus Datei gelesen werden', __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)
            else:
                self.reihenfolge_spin_box.setValue(order)

    def save_file(self):

        order = self.reihenfolge_spin_box.value()
        if order == 0:
            question = u'Anzeigereihenfolge ist auf Standardwert "0". Ist das richtig?'
            message_box = QtGui.QMessageBox(QtGui.QMessageBox.Question,
                                            u'Frage',
                                            question,
                                            QtGui.QMessageBox.NoButton,
                                            self.application.main_window)
            message_box.addButton(u"Ja", QtGui.QMessageBox.YesRole)
            message_box.addButton(u"Nein", QtGui.QMessageBox.NoRole)

            response = message_box.exec_()
            if response != 0:  # not yes
                return

        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'category_info.xml')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Kategorie speichern',
                                                      default_path,
                                                      u'XML-Dateien (*.xml)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if os.path.basename(file_name) != u'category_info.xml':
            self.application.logger.log(u'Dateiname muss "category_info.xml" sein', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        elif not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)

            data = ElementTree.Element('data')

            version = '0'
            version_tag = ElementTree.SubElement(data, 'version')
            version_tag.text = version

            name_tag = ElementTree.SubElement(data, 'name')
            name = self.name_line_edit.text()
            name_tag.text = name

            self.name_completer.add_item(name)

            order_tag = ElementTree.SubElement(data, 'order')
            order_tag.text = unicode(order)

            automatic_tag = ElementTree.SubElement(data, 'automatic')
            automatic_tag.text = u'False'

            indent_xml(data)
            xml = ElementTree.ElementTree(data)
            xml.write(file_name, xml_declaration=True, encoding='UTF-8')

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.MEDIADOCKINFOFILES.CATEGORY_INFO.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.MEDIADOCKINFOFILES.CATEGORY_INFO.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path
