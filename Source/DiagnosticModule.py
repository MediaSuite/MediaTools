# -*- coding: utf-8 -*-

import sys
import traceback
import threading

import Logger


class DiagnosticModule(object):
    __instance = None

    @staticmethod 
    def get_instance(log_file_dir):
        """ Static access method. """
        if DiagnosticModule.__instance is None:
            DiagnosticModule(log_file_dir)
        return DiagnosticModule.__instance
    
    def __init__(self, log_file_dir):
        """ Virtually private constructor. """
        if DiagnosticModule.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            DiagnosticModule.__instance = self
        self.__logger = Logger.Logger(log_file_dir, 'DiagnosticModule', 366)
        sys.excepthook = self.handle_exception
        self.__logger.cleanup()
        
    def handle_exception(self, type_, value, traceback_):
        message = 'Exception "{}" of type "{}" occurred in thread "{}" !|! '.format(
            str(value), str(type_), threading.current_thread().name
        )
        message += '!|! traceback: ' + ' | '.join(traceback.format_tb(traceback_))
        self.__logger.log(message, __name__, Logger.SEVERITY_ERROR)


def execute_with_exception_handling(func_):
    def wrapper(*args, **kwargs):
        try:
            return func_(*args, **kwargs)
        except:
            sys.excepthook(*sys.exc_info())
            sys.__excepthook__(*sys.exc_info())
    return wrapper
