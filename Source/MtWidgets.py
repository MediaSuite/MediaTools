# coding=utf-8

import os
import re
import codecs
import json
from PySide import QtGui
from PySide import QtCore

import Dateinamen
import Global
import MediaToolsLogger

illegal_chars = ['"', "'", '-', '_', ',', ';', ':', '.', '(', ')', '/', '{', '[', ']', '}', '<', '>', '\n', '\t', '\a',
                 '\f', '\\', '\r', '\b', u'’']


def all_words_in_order(a, b):
    a = a.lower()
    for x in illegal_chars:
        a = a.replace(x, '')
    b = b.lower()
    for x in illegal_chars:
        b = b.replace(x, '')
    return b in a


def start_with_words(a, b):
    a = a.lower()
    for x in illegal_chars:
        a = a.replace(x, '')
    b = b.lower()
    for x in illegal_chars:
        b = b.replace(x, '')
    return a.startswith(b)


class SUGGESTION_POLICY(object):
    BEGIN = 0
    IN = 1
    NO = 2


SUGGESTION_POLICIES = [0, 1, 2]


class CompleterBase(QtGui.QCompleter):
    # https://stackoverflow.com/questions/5129211/qcompleter-custom-completion-rules

    def __init__(self, *args, **kwargs):
        super(CompleterBase, self).__init__(*args, **kwargs)
        self.source_model = QtGui.QStandardItemModel()
        self.__policy = SUGGESTION_POLICY.BEGIN

    def setModel(self, model):
        self.source_model = model
        super(CompleterBase, self).setModel(self.source_model)

    def update_model(self):
        local_completion_prefix = self.local_completion_prefix
        policy = self.__policy

        class InnerProxyModel(QtGui.QSortFilterProxyModel):
            def filterAcceptsRow(self, source_row, source_parent):
                index = self.sourceModel().index(source_row, 0, source_parent)
                model_row = self.sourceModel().data(index)
                if policy == SUGGESTION_POLICY.BEGIN:
                    return start_with_words(model_row, local_completion_prefix)
                elif policy == SUGGESTION_POLICY.IN:
                    return all_words_in_order(model_row, local_completion_prefix)
                return False

        proxy_model = InnerProxyModel()
        proxy_model.setSourceModel(self.source_model)
        super(CompleterBase, self).setModel(proxy_model)

    def set_suggestion_policy(self, policy):
        self.__policy = policy


class ListCompleter(CompleterBase):

    def __init__(self, application, data, policy=SUGGESTION_POLICY.BEGIN):
        super(ListCompleter, self).__init__()
        self.application = application
        self.__policy = policy

        self.setCompletionMode(QtGui.QCompleter.PopupCompletion)
        self.local_completion_prefix = ""
        self.source_model = QtGui.QStandardItemModel()
        if u"" not in data:
            data = [u""] + data
        for item in data:
            standard_item = QtGui.QStandardItem(item)
            self.source_model.appendRow(standard_item)
        self.setModel(self.source_model)

    def splitPath(self, path):
        self.local_completion_prefix = path
        self.update_model()
        return ""

    def has_item(self, item):
        return bool(self.source_model.findItems(item))


class FileCompleter(CompleterBase):

    def __init__(self, application, file_path, policy=SUGGESTION_POLICY.BEGIN):
        super(FileCompleter, self).__init__()
        self.application = application
        self.file_path = file_path

        if not os.path.exists(file_path):
            dir_path = os.path.dirname(file_path)
            try:
                os.makedirs(dir_path)
            except Exception as e:
                pass  # todo message
            try:
                completer_file = open(file_path, 'a')
                completer_file.write('\n')
                completer_file.close()
            except Exception as e:
                pass  # todo message

        self.__policy = policy

        self.modification_time = 0
        self.setCompletionMode(QtGui.QCompleter.PopupCompletion)
        self.local_completion_prefix = ""
        self.load_source_model()

    def load_source_model(self):
        try:
            read_file = codecs.open(self.file_path, 'r', 'utf-8')
            content = read_file.readlines()
            read_file.close()
        except Exception as e:
            print e
            # todo
            return
        else:
            self.modification_time = os.path.getmtime(self.file_path)
            data = list()
            for line in content:
                temp = re.sub(Global.WHITESPACES, u'', line)
                if temp and temp not in data:
                    data.append(temp)

            self.source_model = QtGui.QStandardItemModel()
            if u"" not in data:
                data = [u""] + data
            for item in data:
                standard_item = QtGui.QStandardItem(item)
                self.source_model.appendRow(standard_item)
            self.setModel(self.source_model)

    def check_reload_source_model(self):
        if self.file_path and os.path.isfile(self.file_path):
            return os.path.getmtime(self.file_path) != self.modification_time
        else:
            return False

    def splitPath(self, path):
        if self.check_reload_source_model():
            self.load_source_model()
        self.local_completion_prefix = path
        self.update_model()
        return ""

    def add_item(self, item):
        if item and not self.source_model.findItems(item):
            try:
                write_file = codecs.open(self.file_path, 'a', 'utf-8')
            except Exception as e:
                print e
                # todo
            else:
                write_file.write(os.linesep + item)
                write_file.close()
        self.load_source_model()


class MultiCompleter(CompleterBase):

    def __init__(self,
                 application,
                 file_path,
                 policy=SUGGESTION_POLICY.BEGIN,
                 separators=(u';', u' und', u'&', u',', u'+')):
        super(MultiCompleter, self).__init__()
        self.application = application
        self.file_path = file_path

        if not os.path.exists(file_path):
            dir_path = os.path.dirname(file_path)
            try:
                os.makedirs(dir_path)
            except Exception as e:
                pass  # todo message
            try:
                completer_file = open(file_path, 'a')
                completer_file.write('\n')
                completer_file.close()
            except Exception as e:
                pass  # todo message

        self.__policy = policy
        self.separators = separators

        self.modification_time = 0
        self.file_items = []
        self.setCompletionMode(QtGui.QCompleter.PopupCompletion)
        self.local_completion_prefix = ""
        self.prefix = ""
        self.load_file()

    def load_file(self):
        try:
            read_file = codecs.open(self.file_path, 'r', 'utf-8')
            content = read_file.readlines()
            read_file.close()
        except Exception as e:
            print e
            # todo
            self.file_items = []
        else:
            self.modification_time = os.path.getmtime(self.file_path)
            data = list()
            for line in content:
                temp = re.sub(Global.WHITESPACES, u'', line)
                if temp and temp not in data:
                    data.append(temp)
            if u"" not in data:
                data = [u""] + data
            self.file_items = data

    def check_reload_file(self):
        if self.file_path and os.path.isfile(self.file_path):
            return os.path.getmtime(self.file_path) != self.modification_time
        else:
            return False

    def update_model(self):
        local_completion_prefix = self.local_completion_prefix
        policy = self.__policy

        if len(local_completion_prefix) != 0:
            prefix = self.prefix[:len(local_completion_prefix) * -1]
        else:
            prefix = self.prefix

        self.source_model = QtGui.QStandardItemModel()
        for item in self.file_items:
            if prefix:
                standard_item = QtGui.QStandardItem(prefix + item)
            else:
                standard_item = QtGui.QStandardItem(item)
            self.source_model.appendRow(standard_item)
        super(CompleterBase, self).setModel(self.source_model)

        separators = self.separators

        class InnerProxyModel(QtGui.QSortFilterProxyModel):
            def filterAcceptsRow(self, source_row, source_parent):
                index = self.sourceModel().index(source_row, 0, source_parent)
                model_row = self.sourceModel().data(index)
                b = model_row
                for separator in separators:
                    b = b.split(separator)[-1]
                b = b.lstrip(u''.join(Global.DATEINAMEN.MODEL.DELIMITERS))
                if policy == SUGGESTION_POLICY.BEGIN:
                    return start_with_words(b, local_completion_prefix)
                elif policy == SUGGESTION_POLICY.IN:
                    return all_words_in_order(b, local_completion_prefix)
                return False

        proxy_model = InnerProxyModel()
        proxy_model.setSourceModel(self.source_model)
        super(CompleterBase, self).setModel(proxy_model)

    def splitPath(self, path):
        a = self.__get_end_for_completion(path, self.separators)
        if self.check_reload_file():
            self.load_file()
        self.local_completion_prefix = a
        self.prefix = path
        self.update_model()
        return ""

    @staticmethod
    def __get_end_for_completion(text, separators):
        a = text
        for separator in separators:
            a = a.split(separator)[-1]
        a = a.lstrip(u''.join(Global.DATEINAMEN.MODEL.DELIMITERS))
        return a

    def add_items(self, items):
        item_list = [items]
        for separator in self.separators:
            new_item_list = list()
            for item in item_list:
                for split_item in item.split(separator):
                    new_item_list.append(split_item)
            item_list = new_item_list

        for x in range(len(item_list)):
            item_list[x] = item_list[x].strip(u''.join(Global.DATEINAMEN.MODEL.DELIMITERS))

        for item in item_list:
            if item and item not in self.file_items:
                try:
                    write_file = codecs.open(self.file_path, 'a', 'utf-8')
                except Exception as e:
                    print e
                    # todo
                else:
                    write_file.write(os.linesep + item)
                    write_file.close()
        self.load_file()

    def set_suggestion_policy(self, policy):
        self.__policy = policy


class CompleterLineEdit(QtGui.QLineEdit):

    def keyPressEvent(self, event):
        selected = self.completer().popup().selectedIndexes()
        is_visible = self.completer().popup().isVisible()
        if (event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter) and selected and is_visible:
            self.setText(selected[0].data())
            self.completer().popup().close()
        else:
            super(CompleterLineEdit, self).keyPressEvent(event)


class ExportedNamesWidget(QtGui.QWidget):

    def __init__(self, application, show_order_buttons=False):
        super(self.__class__, self).__init__()

        self.application = application
        self.model = application.main_window.dateinamen_model

        self.show_order_buttons = show_order_buttons

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        self.list_widget = QtGui.QListWidget()

        button_select_all = QtGui.QPushButton(u'Alles auswählen')
        button_select_all.setToolTip(u'Alles auswählen')
        # noinspection PyUnresolvedReferences
        button_select_all.clicked.connect(self.list_widget.selectAll)
        layout_1.addWidget(button_select_all)
        button_clear_selection = QtGui.QPushButton(u'Auswahl aufheben')
        button_clear_selection.setToolTip(u'Auswahl aufheben')
        # noinspection PyUnresolvedReferences
        button_clear_selection.clicked.connect(self.list_widget.clearSelection)
        layout_1.addWidget(button_clear_selection)
        button_toggle_selection = QtGui.QPushButton(u'Auswahl umkehren')
        button_toggle_selection.setToolTip(u'Auswahl umkehren')
        # noinspection PyUnresolvedReferences
        button_toggle_selection.clicked.connect(self.toggle_selection)
        layout_1.addWidget(button_toggle_selection)
        layout_1.addStretch()

        self.button_save_dateinamen = QtGui.QPushButton(u'Dateinamen speichern')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-save-3-240.png'))
        self.button_save_dateinamen.setIcon(icon)
        self.button_save_dateinamen.setToolTip(u'Dateinamen speichern')
        # noinspection PyUnresolvedReferences
        self.button_save_dateinamen.clicked.connect(self.save_dateinamen)
        layout_1.addWidget(self.button_save_dateinamen)

        self.button_load_dateinamen = QtGui.QPushButton(u'Dateinamen öffnen')
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-disk-15-240.png'))
        self.button_load_dateinamen.setIcon(icon)
        self.button_load_dateinamen.setToolTip(u'Dateinamen öffnen')
        # noinspection PyUnresolvedReferences
        self.button_load_dateinamen.clicked.connect(self.load_dateinamen)
        layout_1.addWidget(self.button_load_dateinamen)

        self.list_widget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)
        layout.addWidget(self.list_widget, 1)

        self.load_names()

        self.model.copied_changed.connect(self.load_names)

    def load_names(self):
        self.list_widget.clear()
        counter = 0
        for dateiname in self.model.copied_names:
            item = QtGui.QListWidgetItem()
            widget = QtGui.QWidget()
            layout = QtGui.QHBoxLayout()
            layout.setContentsMargins(0, 0, 0, 0)
            widget.setLayout(layout)

            button_copy = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-copy-7-240.png'))
            button_copy.setIcon(icon)
            button_copy.setToolTip(u'Name in Zwischenablage kopieren')
            # noinspection PyUnresolvedReferences
            button_copy.clicked.connect(self.handle_button_copy_clicked)
            button_copy.setFixedWidth(24)
            layout.addWidget(button_copy)

            button_up = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-3-240-up.png'))
            button_up.setIcon(icon)
            button_up.setToolTip(u'Name nach oben')
            # noinspection PyUnresolvedReferences
            button_up.clicked.connect(self.handle_button_up_clicked)
            button_up.setFixedWidth(24)
            if counter == 0:
                button_up.setDisabled(True)
            # button_up.setFixedWidth(5)
            if self.show_order_buttons:
                layout.addWidget(button_up)

            button_down = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-arrow-3-240-down.png'))
            button_down.setIcon(icon)
            button_down.setToolTip(u'Name nach unten')
            # noinspection PyUnresolvedReferences
            button_down.clicked.connect(self.handle_button_down_clicked)
            if counter == len(self.model.copied_names) - 1:
                button_down.setDisabled(True)
            button_down.setFixedWidth(24)
            if self.show_order_buttons:
                layout.addWidget(button_down)

            label_dateiname = QtGui.QLabel(dateiname.copy_name)
            layout.addWidget(label_dateiname)
            layout.addStretch()

            button_delete = QtGui.QPushButton()
            icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-x-mark-4-240.png'))
            button_delete.setIcon(icon)
            button_delete.setToolTip(u'Name löschen')
            # noinspection PyUnresolvedReferences
            button_delete.clicked.connect(self.handle_button_delete_clicked)
            button_delete.setFixedWidth(24)
            layout.addWidget(button_delete)

            widget_size = widget.sizeHint()
            item_size = QtCore.QSize(widget_size.width() + 16, widget_size.height())
            item.setSizeHint(item_size)

            self.list_widget.insertItem(counter, item)
            self.list_widget.setItemWidget(item, widget)

            counter += 1

    def toggle_selection(self):
        for index in range(self.list_widget.count()):
            item = self.list_widget.item(index)
            if item.isSelected():
                item.setSelected(False)
            else:
                item.setSelected(True)

    def handle_button_copy_clicked(self):
        button = self.sender()
        index = self.list_widget.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.copy_name_again(index.row())

    def handle_button_up_clicked(self):
        button = self.sender()
        index = self.list_widget.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.move_copied_name_up(index.row())

    def handle_button_down_clicked(self):
        button = self.sender()
        index = self.list_widget.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.move_copied_name_down(index.row())

    def handle_button_delete_clicked(self):
        button = self.sender()
        index = self.list_widget.indexAt(button.parentWidget().pos())
        if index.isValid():
            self.model.remove_copied_name_at_index(index.row())

    def get_selected_item_indexes(self):
        indexes = [i.row() for i in self.list_widget.selectedIndexes()]
        return indexes

    def load_dateinamen(self):  # todo
        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.expanduser(u'~')

        file_name = QtGui.QFileDialog.getOpenFileName(self.application.main_window, u'Dateinamen öffnen',
                                                      default_path,
                                                      u'JSON (*.json)')
        file_name = file_name[0]
        if not file_name:
            return
        if not os.path.isfile(file_name):
            self.application.logger.log(u'Datei konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
            return
        else:
            self.__save_path(file_name)
            try:
                read_file = open(file_name, 'r')
            except IOError:
                self.application.logger.log(u'Datei konnte nicht geöffnet werden', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            try:
                json_data = json.load(read_file)
            except Exception as e:
                self.application.logger.log(u'Datei konnte nicht ausgelesen werden'.format(str(e)), __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            else:
                read_file.close()
            try:
                dateinamen_dicts = list(json_data)
            except Exception:
                self.application.logger.log(u'Unerwartetes Dateiformat', __name__,
                                            MediaToolsLogger.SEVERITY_ERROR, True, True)
                return
            errors = 0
            for dateiname in dateinamen_dicts:
                try:
                    self.model.add_to_copied_names(Dateinamen.Dateiname(self.model, dateiname))
                except Exception as e:
                    errors += 1
                    self.application.logger.log(u'Failed to deserialize Dateiname: {0}'.format(str(e)), __name__,
                                                MediaToolsLogger.SEVERITY_ERROR, False, False)
            if errors:
                self.application.logger.log(u'{0} von {1} Dateinamen konnten nicht geladen '
                                            u'werden'.format(errors, len(dateinamen_dicts)), __name__,
                                            MediaToolsLogger.SEVERITY_WARNING, True, True)

    def save_dateinamen(self):
        if not self.model.copied_names:
            self.application.logger.log(u'Keine Dateinamen im Speicher.', __name__,
                                        MediaToolsLogger.SEVERITY_INFO, True, True)
            return

        default_path = self.__load_path()
        if not os.path.isfile(default_path):
            default_path = os.path.join(os.path.expanduser(u'~'), u'Dateinamen.json')

        file_name = QtGui.QFileDialog.getSaveFileName(self.application.main_window, u'Dateinamen speichern',
                                                      default_path,
                                                      u'JSON (*.json)')

        file_name = unicode(file_name[0])

        if not file_name:
            return

        if not os.path.isdir(os.path.dirname(file_name)):
            self.application.logger.log(u'Ordner konnte nicht gefunden werden', __name__,
                                        MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            self.__save_path(file_name)

            try:
                write_file = open(file_name, 'w')
            except IOError as e:
                self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
            else:
                dateinamen = [i.get_dict() for i in self.model.copied_names]
                try:
                    json.dump(dateinamen, write_file)
                except Exception as e:
                    self.application.logger.log(u'Fehler beim Speichern der Datei "{0}": {1}'.format(file_name, str(e)),
                                                __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
                write_file.close()

    @staticmethod
    def __save_path(path):
        try:
            codecs.open(Global.DATEINAMEN.FILES.DIALOG_PATH, 'w', 'utf-8').write(path)
        except IOError:
            pass

    @staticmethod
    def __load_path():
        try:
            path = codecs.open(Global.DATEINAMEN.FILES.DIALOG_PATH, 'r', 'utf-8').read()
        except IOError:
            return u''
        else:
            return path


class TypeDefinitionDialog(QtGui.QDialog):

    def __init__(self, parent, type_definition):
        super(self.__class__, self).__init__(parent, QtCore.Qt.WindowTitleHint)

        self.ok = False

        self.setWindowTitle(u'Typdefinition bearbeiten')

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        widget_1 = QtGui.QWidget()
        layout.addWidget(widget_1)
        layout_1 = QtGui.QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        widget_1.setLayout(layout_1)

        layout_1.addWidget(QtGui.QLabel(u'<b>Allgemein</b>'))

        widget_2 = QtGui.QWidget()
        layout.addWidget(widget_2)
        layout_2 = QtGui.QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        widget_2.setLayout(layout_2)

        widget_2_1 = QtGui.QWidget()
        layout_2.addWidget(widget_2_1)
        layout_2_1 = QtGui.QVBoxLayout()
        layout_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_1.setLayout(layout_2_1)

        label = QtGui.QLabel(u'Anzeigename:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)
        label = QtGui.QLabel(u'Abkürzung:')
        label.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        layout_2_1.addWidget(label)

        widget_2_2 = QtGui.QWidget()
        layout_2.addWidget(widget_2_2)
        layout_2_2 = QtGui.QVBoxLayout()
        layout_2_2.setContentsMargins(0, 0, 0, 0)
        widget_2_2.setLayout(layout_2_2)

        widget_2_2_1 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_1)
        layout_2_2_1 = QtGui.QHBoxLayout()
        layout_2_2_1.setContentsMargins(0, 0, 0, 0)
        widget_2_2_1.setLayout(layout_2_2_1)

        self.display_name_line_edit = QtGui.QLineEdit()
        layout_2_2_1.addWidget(self.display_name_line_edit)

        widget_2_2_2 = QtGui.QWidget()
        layout_2_2.addWidget(widget_2_2_2)
        layout_2_2_2 = QtGui.QHBoxLayout()
        layout_2_2_2.setContentsMargins(0, 0, 0, 0)
        widget_2_2_2.setLayout(layout_2_2_2)

        self.abbreviation_line_edit = QtGui.QLineEdit()
        layout_2_2_2.addWidget(self.abbreviation_line_edit)

        widget_3 = QtGui.QWidget()
        layout.addWidget(widget_3)
        layout_3 = QtGui.QHBoxLayout()
        layout_3.setContentsMargins(0, 0, 0, 0)
        widget_3.setLayout(layout_3)

        layout_3.addWidget(QtGui.QLabel(u'<b>ID3-Tags</b>'))

        widget_4 = QtGui.QWidget()
        layout.addWidget(widget_4)
        layout_4 = QtGui.QHBoxLayout()
        layout_4.setContentsMargins(0, 0, 0, 0)
        widget_4.setLayout(layout_4)

        layout_4.addWidget(QtGui.QLabel(u'Titel:'))

        self.id3_tag_title_line_edit = QtGui.QLineEdit()
        layout_4.addWidget(self.id3_tag_title_line_edit)

        widget_5 = QtGui.QWidget()
        layout.addWidget(widget_5)
        layout_5 = QtGui.QHBoxLayout()
        layout_5.setContentsMargins(0, 0, 0, 0)
        widget_5.setLayout(layout_5)

        self.id3_tag_overwrite_title_radio_button_true = QtGui.QRadioButton(u'Titel überschreiben, wenn anderer Titel '
                                                                            u'angegeben wird')
        layout_5.addWidget(self.id3_tag_overwrite_title_radio_button_true)
        self.id3_tag_overwrite_title_radio_button_false = QtGui.QRadioButton(u'Anderen Titel anhängen')
        layout_5.addWidget(self.id3_tag_overwrite_title_radio_button_false)
        layout_5.addStretch()

        widget_6 = QtGui.QWidget()
        layout.addWidget(widget_6)
        layout_6 = QtGui.QHBoxLayout()
        layout_6.setContentsMargins(0, 0, 0, 0)
        widget_6.setLayout(layout_6)

        layout_6.addWidget(QtGui.QLabel(u'Interpret:'))

        self.id3_tag_interpret_line_edit = QtGui.QLineEdit()
        layout_6.addWidget(self.id3_tag_interpret_line_edit)

        widget_7 = QtGui.QWidget()
        layout.addWidget(widget_7)
        layout_7 = QtGui.QHBoxLayout()
        layout_7.setContentsMargins(0, 0, 0, 0)
        widget_7.setLayout(layout_7)

        self.id3_tag_overwrite_interpret_radio_button_true = QtGui.QRadioButton(u'Interpret überschreiben, wenn '
                                                                                u'anderer Interpret angegeben wird')
        layout_7.addWidget(self.id3_tag_overwrite_interpret_radio_button_true)
        self.id3_tag_overwrite_interpret_radio_button_false = QtGui.QRadioButton(u'Anderen Interpreten anhängen')
        layout_7.addWidget(self.id3_tag_overwrite_interpret_radio_button_false)
        layout_7.addStretch()

        widget_8 = QtGui.QWidget()
        layout.addWidget(widget_8)
        layout_8 = QtGui.QHBoxLayout()
        layout_8.setContentsMargins(0, 0, 0, 0)
        widget_8.setLayout(layout_8)

        layout_8.addWidget(QtGui.QLabel(u'Genre:'))

        self.id3_tag_genre_line_edit = QtGui.QLineEdit()
        layout_8.addWidget(self.id3_tag_genre_line_edit)

        widget_9 = QtGui.QWidget()
        layout.addWidget(widget_9)
        layout_9 = QtGui.QHBoxLayout()
        layout_9.setContentsMargins(0, 0, 0, 0)
        widget_9.setLayout(layout_9)

        layout_9.addWidget(QtGui.QLabel(u'<b>Vorauswahl Attribute</b>'))

        widget_10 = QtGui.QWidget()
        layout.addWidget(widget_10)
        layout_10 = QtGui.QHBoxLayout()
        layout_10.setContentsMargins(0, 0, 0, 0)
        widget_10.setLayout(layout_10)

        layout_10.addWidget(QtGui.QLabel(u'Vortragender:'))
        self.interpret_radio_button_true = QtGui.QRadioButton(u'Auswählen')
        layout_10.addWidget(self.interpret_radio_button_true)
        self.interpret_radio_button_false = QtGui.QRadioButton(u'Nicht auswählen')
        layout_10.addWidget(self.interpret_radio_button_false)
        layout_10.addStretch()

        widget_11 = QtGui.QWidget()
        layout.addWidget(widget_11)
        layout_11 = QtGui.QHBoxLayout()
        layout_11.setContentsMargins(0, 0, 0, 0)
        widget_11.setLayout(layout_11)

        layout_11.addWidget(QtGui.QLabel(u'Bibelstelle:'))
        self.scripture_radio_button_true = QtGui.QRadioButton(u'Auswählen')
        layout_11.addWidget(self.scripture_radio_button_true)
        self.scripture_radio_button_false = QtGui.QRadioButton(u'Nicht auswählen')
        layout_11.addWidget(self.scripture_radio_button_false)
        layout_11.addStretch()

        widget_12 = QtGui.QWidget()
        layout.addWidget(widget_12)
        layout_12 = QtGui.QHBoxLayout()
        layout_12.setContentsMargins(0, 0, 0, 0)
        widget_12.setLayout(layout_12)

        layout_12.addWidget(QtGui.QLabel(u'Titel:'))
        self.title_radio_button_true = QtGui.QRadioButton(u'Auswählen')
        layout_12.addWidget(self.title_radio_button_true)
        self.title_radio_button_false = QtGui.QRadioButton(u'Nicht auswählen')
        layout_12.addWidget(self.title_radio_button_false)
        layout_12.addStretch()

        widget_13 = QtGui.QWidget()
        layout.addWidget(widget_13)
        layout_13 = QtGui.QHBoxLayout()
        layout_13.setContentsMargins(0, 0, 0, 0)
        widget_13.setLayout(layout_13)

        layout_13.addWidget(QtGui.QLabel(u'Sonst:'))
        self.other_radio_button_true = QtGui.QRadioButton(u'Auswählen')
        layout_13.addWidget(self.other_radio_button_true)
        self.other_radio_button_false = QtGui.QRadioButton(u'nicht auswählen')
        layout_13.addWidget(self.other_radio_button_false)
        layout_13.addStretch()

        layout.addStretch()

        line = QtGui.QFrame()
        line.setFrameShape(QtGui.QFrame.HLine)
        line.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(line)

        widget_14 = QtGui.QWidget()
        layout.addWidget(widget_14)
        layout_14 = QtGui.QHBoxLayout()
        layout_14.setContentsMargins(0, 0, 0, 0)
        widget_14.setLayout(layout_14)

        layout_14.addStretch()

        self.ok_button = QtGui.QPushButton(u'OK')
        # noinspection PyUnresolvedReferences
        self.ok_button.clicked.connect(self.on_click_ok)
        layout_14.addWidget(self.ok_button)

        self.abort_button = QtGui.QPushButton(u'Abbrechen')
        # noinspection PyUnresolvedReferences
        self.abort_button.clicked.connect(self.on_click_abort)
        layout_14.addWidget(self.abort_button)

        self.load_type_definition(type_definition)

    def load_type_definition(self, type_definition):
        self.display_name_line_edit.setText(type_definition.display_name)
        self.abbreviation_line_edit.setText(type_definition.abbreviation)
        self.id3_tag_title_line_edit.setText(type_definition.id3_tag_title)
        if type_definition.id3_tag_overwrite_title:
            self.id3_tag_overwrite_title_radio_button_true.setChecked(True)
            self.id3_tag_overwrite_title_radio_button_false.setChecked(False)
        else:
            self.id3_tag_overwrite_title_radio_button_true.setChecked(False)
            self.id3_tag_overwrite_title_radio_button_false.setChecked(True)
        self.id3_tag_interpret_line_edit.setText(type_definition.id3_tag_interpret)
        if type_definition.id3_tag_overwrite_interpret:
            self.id3_tag_overwrite_interpret_radio_button_true.setChecked(True)
            self.id3_tag_overwrite_interpret_radio_button_false.setChecked(False)
        else:
            self.id3_tag_overwrite_interpret_radio_button_true.setChecked(False)
            self.id3_tag_overwrite_interpret_radio_button_false.setChecked(True)
        self.id3_tag_genre_line_edit.setText(type_definition.id3_tag_genre)
        if type_definition.name_radio_button:
            self.interpret_radio_button_true.setChecked(True)
            self.interpret_radio_button_false.setChecked(False)
        else:
            self.interpret_radio_button_true.setChecked(False)
            self.interpret_radio_button_false.setChecked(True)
        if type_definition.stelle_radio_button:
            self.scripture_radio_button_true.setChecked(True)
            self.scripture_radio_button_false.setChecked(False)
        else:
            self.scripture_radio_button_true.setChecked(False)
            self.scripture_radio_button_false.setChecked(True)
        if type_definition.titel_radio_button:
            self.title_radio_button_true.setChecked(True)
            self.title_radio_button_false.setChecked(False)
        else:
            self.title_radio_button_true.setChecked(False)
            self.title_radio_button_false.setChecked(True)
        if type_definition.sonst_radio_button:
            self.other_radio_button_true.setChecked(True)
            self.other_radio_button_false.setChecked(False)
        else:
            self.other_radio_button_true.setChecked(False)
            self.other_radio_button_false.setChecked(True)

    def get_type_definition(self):
        type_definition = Dateinamen.TypeDefinition()
        type_definition.display_name = self.display_name_line_edit.text().strip()
        type_definition.abbreviation = self.abbreviation_line_edit.text().strip()
        type_definition.id3_tag_title = self.id3_tag_title_line_edit.text().strip()
        type_definition.id3_tag_overwrite_title = self.id3_tag_overwrite_interpret_radio_button_true.isChecked()
        type_definition.id3_tag_genre = self.id3_tag_genre_line_edit.text().strip()
        type_definition.id3_tag_interpret = self.id3_tag_interpret_line_edit.text().strip()
        type_definition.id3_tag_overwrite_interpret = self.id3_tag_overwrite_interpret_radio_button_true.isChecked()
        type_definition.name_radio_button = self.interpret_radio_button_true.isChecked()
        type_definition.stelle_radio_button = self.scripture_radio_button_true.isChecked()
        type_definition.titel_radio_button = self.title_radio_button_true.isChecked()
        type_definition.sonst_radio_button = self.other_radio_button_true.isChecked()
        return type_definition

    def on_click_ok(self):
        self.ok = True
        self.close()

    def on_click_abort(self):
        self.ok = False
        self.close()


class QuitDialog(QtGui.QDialog):

    def __init__(self, parent):
        super(self.__class__, self).__init__(parent, QtCore.Qt.WindowTitleHint)

        self.ok = False

        self.setWindowTitle(u"Echt jetzt?")

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(11, 8, 2, 2)
        self.setLayout(layout)

        top_layout = QtGui.QHBoxLayout()
        top_layout.setContentsMargins(0, 0, 0, 0)
        widget = QtGui.QWidget()
        widget.setLayout(top_layout)
        layout.addWidget(widget)

        image_label = QtGui.QLabel(self)
        icon = QtGui.QPixmap(os.path.join(Global.ICON_DIR, u'iconmonstr-help-4-240.png'))
        icon = icon.scaled(32, 32, QtCore.Qt.KeepAspectRatio)
        image_label.setPixmap(icon)
        top_layout.addWidget(image_label)

        label = QtGui.QLabel(u"Soll das Programm wirklich beendet werden?")
        label_font = label.font()
        label_font.setPointSize(9)
        label.setFont(label_font)
        label.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        label.setContentsMargins(12, 0, 9, 0)
        top_layout.addWidget(label)

        top_layout.addStretch()

        layout.addStretch()

        self.show_again_checkbox = QtGui.QCheckBox(u"Nicht mehr nachfragen")
        self.show_again_checkbox.setChecked(not parent.general_settings_model.ask_quit)
        layout.addWidget(self.show_again_checkbox)

        buttons_layout = QtGui.QHBoxLayout()
        widget = QtGui.QWidget()
        widget.setLayout(buttons_layout)
        layout.addWidget(widget)

        buttons_layout.addStretch()

        self.yes_button = QtGui.QPushButton(u"Ja")
        # noinspection PyUnresolvedReferences
        self.yes_button.clicked.connect(self.on_click_yes)
        buttons_layout.addWidget(self.yes_button)

        self.no_button = QtGui.QPushButton(u"Nein")
        # noinspection PyUnresolvedReferences
        self.no_button.clicked.connect(self.on_click_no)
        buttons_layout.addWidget(self.no_button)

    def on_click_yes(self):
        self.ok = True
        self.close()

    def on_click_no(self):
        self.ok = False
        self.close()

    @property
    def ask_quit(self):
        return not self.show_again_checkbox.isChecked()
