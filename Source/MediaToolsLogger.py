# -*- coding: utf-8 -*-

from datetime import datetime
import os

from PySide import QtGui
from PySide import QtCore

import Logger

# Severity der Log-Meldungen
SEVERITY_DICT = Logger.SEVERITY_DEBUG

SEVERITY_DEBUG = Logger.SEVERITY_DEBUG
SEVERITY_INFO = Logger.SEVERITY_INFO
SEVERITY_WARNING = Logger.SEVERITY_WARNING
SEVERITY_ERROR = Logger.SEVERITY_WARNING

TIME_INCREMENT = 1.0


class MediaToolsLogger(QtGui.QWidget, Logger.Logger):
    """Logger für MediaTools"""

    show_message = QtCore.Signal(unicode, int)
    show_status_bar = QtCore.Signal(unicode)

    def __init__(self, application, log_dir):
        Logger.Logger.__init__(self, log_dir, 'MediaTools', 31)
        QtGui.QWidget.__init__(self)
        self.application = application
        self.show_message.connect(self.__show_message)
        self.show_status_bar.connect(self.__show_status_bar)

    def log(self, message, module_name, severity=SEVERITY_INFO, status_bar=False, dialog=False):
        super(self.__class__, self).log(message, module_name, severity)
        if status_bar:
            self.show_status_bar.emit(message.replace('\n', '').replace('\r', ''))
        if dialog:
            self.show_message.emit(message, severity)

    @property
    def log_file_path(self):
        now = datetime.now()
        return os.path.join(self._Logger__dir, '{} {:04d}-{:02d}-{:02d}.log'.format(self._Logger__log_file_prefix,
                                                                                    now.year,
                                                                                    now.month,
                                                                                    now.day
                                                                                    )
                            )

    @QtCore.Slot(unicode, int)
    def __show_message(self, message, severity):
        if severity == SEVERITY_ERROR:
            QtGui.QMessageBox.critical(self.application.main_window, 'Fehler', message)
        elif severity == SEVERITY_WARNING:
            QtGui.QMessageBox.warning(self.application.main_window, 'Warnung', message)
        if severity == SEVERITY_INFO:
            QtGui.QMessageBox.information(self.application.main_window, 'Information', message)

    @QtCore.Slot(unicode)
    def __show_status_bar(self, message):
        if hasattr(self.application, "main_window"):
            self.application.main_window.status_bar.setText(message)
