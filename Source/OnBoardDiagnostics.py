
from datetime import datetime
import tarfile

import Global


class OnBoardDiagnostics(object):

    def __init__(self, application):
        self.__application = application

    @staticmethod
    def create_filename():
        now = datetime.now()
        zip_name = 'MediaTools_Diagnose_{:04d}-{:02d}-{:02d}_{:02d}-{:02d}-{:02d}.mt_obd'.format(
            now.year, now.month, now.day, now.hour, now.minute, now.second)
        return zip_name

    def write_diagnostic_file(self, file_path):
        with tarfile.open(file_path, "w:gz") as target_package:
            target_package.add(Global.MEDIA_TOOLS_APP_DATA_PATH, 'AppData')
