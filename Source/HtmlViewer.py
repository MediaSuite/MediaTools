# coding=utf-8

import os
import subprocess
import webbrowser
from PySide import QtGui
from PySide import QtWebKit
from PySide import QtCore

import Global


class MediaToolsWebView(QtWebKit.QWebView):

    def contextMenuEvent(self, event):
        pass


class HtmlViewer(QtGui.QWidget):
    set_html = QtCore.Signal(unicode)
    __load_started_signal = QtCore.Signal()
    __load_finished_signal = QtCore.Signal()

    def __init__(self, application, page):
        super(self.__class__, self).__init__()

        self.application = application

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(layout)

        self.web_view = MediaToolsWebView()
        layout.addWidget(self.web_view)

        self.web_view.load(os.path.join(Global.HTML_VIEWER.DOCUMENT_ROOT, page))
        self.web_view.page().mainFrame().addToJavaScriptWindowObject('server', self)

    @QtCore.Slot()
    def show_dateinamen(self):
        self.application.main_window.show_dateinamen()

    @QtCore.Slot()
    def show_dateinamen_settings(self):
        self.application.main_window.show_dateinamen_settings()

    @QtCore.Slot()
    def show_playlists(self):
        self.application.main_window.show_playlist_dateinamen()

    @QtCore.Slot()
    def show_create_dirs(self):
        self.application.main_window.show_create_dirs()

    @QtCore.Slot()
    def show_record_info(self):
        self.application.main_window.show_record_info()

    @QtCore.Slot()
    def show_collection_info(self):
        self.application.main_window.show_collection_info()

    @QtCore.Slot()
    def show_category_info(self):
        self.application.main_window.show_category_info()

    @QtCore.Slot()
    def show_mp3tag(self):
        self.application.main_window.show_mp3tag()

    @QtCore.Slot()
    def open_help(self):
        self.application.main_window.open_help()

    @QtCore.Slot()
    def show_churches_file(self):
        self.application.main_window.open_churches_file()

    @QtCore.Slot()
    def open_events_file(self):
        self.application.main_window.open_events_file()

    @QtCore.Slot()
    def open_playlists_mediatypes(self):
        self.application.main_window.open_playlists_mediatypes()

    @QtCore.Slot(str)
    def open_url(self, url):
        webbrowser.open_new_tab(url)

    @QtCore.Slot()
    def show_dir_licenses(self):
        subprocess.Popen(r'explorer /root,"{0}"'.format(Global.LICENSES_DIR))
