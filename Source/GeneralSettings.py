# coding=utf-8

import codecs
import json
import os

from PySide import QtGui

import Global
import MediaToolsLogger


class GeneralSettingsWidget(QtGui.QWidget):
    """
    Allgemeine Einstellungen
    """

    def __init__(self, application):
        super(self.__class__, self).__init__()
        self.application = application

        layout = QtGui.QVBoxLayout()
        layout.setContentsMargins(5, 5, 5, 5)
        self.setLayout(layout)

        self.ask_quit_check_button = QtGui.QCheckBox(u'Beim Beenden nachfragen')
        layout.addWidget(self.ask_quit_check_button)
        # noinspection PyUnresolvedReferences
        self.ask_quit_check_button.stateChanged.connect(self.ask_quit_check_button_changed)
        self.ask_quit_check_button.setChecked(self.application.main_window.general_settings_model.ask_quit)

        layout.addStretch()

    def ask_quit_check_button_changed(self, value):
        self.application.main_window.general_settings_model.ask_quit = self.ask_quit_check_button.isChecked()


class GeneralSettingsModel(object):

    def __init__(self, application):
        self.application = application
        if os.path.exists(Global.GENERALSETTINGS.PATH):
            self.__change_time = os.path.getmtime(Global.GENERALSETTINGS.PATH)
        else:
            self.__change_time = 0
        self.__loaded = False
        self.__settings = Global.GENERALSETTINGS.DEFAULT_SETTINGS.copy()
        self.__load_settings()

    def __load_settings(self):
        """Einstellungen aus Datei laden."""
        write = False
        checked_settings = dict()
        if not os.path.exists(Global.GENERALSETTINGS.PATH):
            self.__write_settings()
        try:
            settings_file = codecs.open(Global.GENERALSETTINGS.PATH, 'r', 'utf-8')
        except IOError:
            self.__loaded = False
            self.application.logger.log(u"Fehler beim Lesen der Einstellungsdatei für allgemeine Einstellungen.",
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            unchecked_settings = Global.GENERALSETTINGS.DEFAULT_SETTINGS.copy()
            try:
                unchecked_settings = json.loads(settings_file.read())
            except ValueError:
                self.application.logger.log(u"Fehler beim Lesen der Einstellungsdatei für allgemeine Einstellungen.",
                                            __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
                unchecked_settings = self.__settings.copy()
                write = True
            else:
                settings_file.close()
            finally:
                if not isinstance(unchecked_settings, dict):
                    unchecked_settings = dict()
                    write = True

                # ask_quit
                if 'ask_quit' in unchecked_settings:
                    checked_value = self.__check_ask_quit(unchecked_settings['ask_quit'])
                    if checked_value != unchecked_settings['ask_quit']:
                        write = True
                    checked_settings['ask_quit'] = checked_value
                else:
                    checked_settings['ask_quit'] = Global.GENERALSETTINGS.DEFAULT_SETTINGS['ask_quit']
                    write = True

                # show_window_on_top
                if 'show_window_on_top' in unchecked_settings:
                    checked_value = self.__check_show_window_on_top(unchecked_settings['show_window_on_top'])
                    if checked_value != unchecked_settings['show_window_on_top']:
                        write = True
                    checked_settings['show_window_on_top'] = checked_value
                else:
                    checked_settings['show_window_on_top'] = \
                        Global.GENERALSETTINGS.DEFAULT_SETTINGS['show_window_on_top']
                    write = True

                self.__settings = checked_settings
                self.__loaded = True

        if write:
            self.__write_settings()

    def __write_settings(self):
        """Einstellungen in Datei schreiben."""
        try:
            settings_file = codecs.open(Global.GENERALSETTINGS.PATH, 'w', 'utf-8')
        except IOError:
            self.application.logger.log(u"Fehler beim Schreiben der Einstellungsdatei für allgemeine Einstellungen.",
                                        __name__, MediaToolsLogger.SEVERITY_ERROR, True, True)
        else:
            settings_file.write(json.dumps(self.__settings))
            settings_file.close()
            self.__change_time = os.path.getmtime(Global.GENERALSETTINGS.PATH)

    def __check_reload_settings(self):
        """Prüft ob die Einstellungsdatei sich geändert hat und neu geladen werden muss."""
        if not os.path.exists(Global.GENERALSETTINGS.PATH):
            return True
        return not self.__loaded or self.__change_time != os.path.getmtime(Global.GENERALSETTINGS.PATH)

    def load_default_settings(self):
        """Setzt alles auf Defaulteinstellungen zurück."""
        self.__settings = Global.GENERALSETTINGS.DEFAULT_SETTINGS.copy()
        self.__write_settings()

    @property
    def ask_quit(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['ask_quit']

    @ask_quit.setter
    def ask_quit(self, value):
        checked_value = self.__check_ask_quit(value)
        if checked_value == self.__settings['ask_quit']:
            return
        else:
            self.__settings['ask_quit'] = checked_value
            self.__write_settings()

    @staticmethod
    def __check_ask_quit(value):
        return bool(value)

    @property
    def show_window_on_top(self):
        if self.__check_reload_settings():
            self.__load_settings()
        return self.__settings['show_window_on_top']

    @show_window_on_top.setter
    def show_window_on_top(self, value):
        checked_value = self.__check_show_window_on_top(value)
        if checked_value == self.__settings['show_window_on_top']:
            return
        else:
            self.__settings['show_window_on_top'] = checked_value
            self.__write_settings()

    @staticmethod
    def __check_show_window_on_top(value):
        return bool(value)
