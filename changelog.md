## [1.0.4] - 2022-06-09
### Geändert
- Option zur Darstellung des Fensters im Vordergrund hinzugefügt

## [1.0.3] - 2021-04-12
### Geändert
- Beenden-Dialog ausschaltbar gemacht

## [1.0.2] - 2020-02-16
### Geändert
- Bei ID3 Tags muss das Ereignis und die Gemeinde angegeben werden

## [1.0.1] - 2020-01-04
### Hinzugefügt
- Diagnosemodul
### Behoben
- Bug im Logger

## [1.0.0] - 2018-06-16
Initiale Version
