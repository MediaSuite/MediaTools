# Definition des TypeDefinitionDict

### Abstract
Ein TypeDefinitionDict ist ein JSON-serialisierbares Dictionary um
eigene Typen in MediaTools zu definieren. Die Zeichen <>{} werden für die
Anzeige escaped.

### Attribute
|Name| Typ |Bedeutung|
|-----|-----|----------|
|display_name|unicode|Name der im Dropdown angezeigt wird|
|abbreviation|unicode|Abkürzung die im Dateinamen verwendet wird. Für Dateinamen unzulässige Zeichen werden entfernt.|
|id3_tag_title|unicode|Titel im ID3-Tag|
|id3_tag_overwrite_title|boolean|Bestimmt ob der Titel im ID3-Tag überschrieben wird. Wenn wahr und ein anderer Titel vorhanden ist wird der andere Titel. Wenn wahr und kein anderer Titel vorhanden ist wird id3_tag_title verwendet. Wenn unwahr wird eine ein anderer Titel, wenn vorhanden, an id3_tag_title angehängt.  todo sauber formulieren|
|id3_tag_genre|unicode|Genre im ID3-Tag. Wenn nicht angegeben ist das Genre "Religiös"|
|id3_tag_interpret|unicode|Name es Interpreten im ID3-Tag|
|id3_tag_overwrite_interpret|boolean|Bestimmt ob der Interpret im ID3-Tag überschrieben wird. Wenn wahr und ein anderer Interpret vorhanden ist wird der andere Interpret verwendet. Wenn wahr und kein anderer Interpret vorhanden ist wird id3_tag_interpret verwendet. Wenn unwahr wird eine ein anderer Interpret, wenn vorhanden, an id3_tag_interpret angehängt. todo sauber formulieren|
|name_radio_button|boolean|Gibt an ob das Feld für den Vortragenden angewählt wird, wenn dieser Typ ausgewählt wird.|
|stelle_radio_button|boolean|Gibt an ob das Feld für die Bibelstelle angewählt wird, wenn dieser Typ ausgewählt wird.|
|titel_radio_button|boolean|Gibt an ob das Feld für den Titel angewählt wird, wenn dieser Typ ausgewählt wird.|
|sonst_radio_button|boolean|Gibt an ob das Feld für Sonst angewählt wird, wenn dieser Typ ausgewählt wird.|

Beispiel:
```
{
    'display_name': u'Chor',
    'abbreviation': u'ChL',
    'id3_tag_title': u'',
    'id3_tag_overwrite_title': True,
    'id3_tag_genre': u'Chorlied',
    'id3_tag_interpret': u'Chor',
    'id3_tag_overwrite_interpret': False,
    'name_radio_button': False,
    'stelle_radio_button': False,
    'titel_radio_button': True,
    'sonst_radio_button': False
}
```
