# Definition des DateinamenDict

### Abstract
Ein DateinamenDict ist ein JSON-serialisierbares Dictionary um
Dateinamen serialisieren zu können.

### Attribute
|Name| Typ |Bedeutung|
|-----|-----|----------|
|counter|integer|Zählerstand|
|general_song|unicode oder None|Inhalt des Eingabefeldes "Allg.Lied" wenn Allg.Lied angewählt sonst None|
|type_other_text|unicode oder None|Inhalt des Eingabefeldes "Sonst" im Bereich Typen, wenn "Sonst" angewählt sonst None|
|name|unicode oder None|Inhalt des Feldes "Vortragender", wenn angewählt.|
|scripture_book|unicode oder None|Inhalt des Feldes "Bibelbuch", wenn angewählt sonst None|
|scripture_text|unicode oder None|Inhalt des Feldes "Kapitel und Versangabe", wenn angewählt, sonst None|
|title|unicode oder None|Inhalt des Feldes "Titel", wenn angewählt sonst None|
|other|unicode oder None|Inhalt des Feldes "Sonst", wenn angewählt sonst None|
|type_definition|TypeDefinitionDict oder None|Siehe TypeDefinitionDict.md|

Beispiel:
```
tbd todo
```
